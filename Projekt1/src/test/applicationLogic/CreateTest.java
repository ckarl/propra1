package test.applicationLogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import applicationLogic.Create;
import dataInterfaces.Addressy;
import dataStructures.Customer;
import dataStructures.Person;
import dataStructures.Tutor;

public class CreateTest {

	// Address
	String street = "Hauptstraße";
	String houseNr = "1337";
	String zip = "57072";
	String city = "Siegen";
	
	// Person
	String id = "1";
	String lastname = "Koch";
	String firstname = "Karl";
	String phoneNumber = "012345678987654";	
	
	// Tutor
	String bankData = "DEXX XXXX XXXX XXXX XXXX XX";
	String email = "test@example.com";

	@Test
	public void test_creation_of_address() {
		Addressy address = Create.address(street, houseNr, zip, city);
		assertNotNull(address);
		assertEquals("Hauptstraße 1337 57072 Siegen", address.toString());
	}
	
	@Test
	public void test_creation_of_person() {
		Addressy address = Create.address(street, houseNr, zip, city);
		Person person = Create.person(lastname, firstname, address, phoneNumber);
		assertNotNull(person);
		assertEquals(lastname, person.getLastname());
		assertEquals(firstname, person.getFirstname());
		assertEquals(address, person.getAddress());
	}
	
	@Test
	public void test_creation_of_tutor() {
		Addressy address = Create.address(street, houseNr, zip, city);
		Person person = Create.person(lastname, firstname, address, phoneNumber);
		String subjects = "Alle Hauptfächer, nur Grundschule";
		Tutor tutor = Create.tutor(person, bankData, email, subjects);
		assertNotNull(tutor);
		assertEquals(person, tutor.getPerson());
		assertEquals(bankData, tutor.getBankData());
		assertEquals(email, tutor.getEmail());
	}

	@Test
	public void test_creation_of_customer() {
		Addressy address = Create.address(street, houseNr, zip, city);
		Person person = Create.person(lastname, firstname, address, phoneNumber);
		String subjects = "Alle Hauptfächer, nur Grundschule";
		Tutor tutor = Create.tutor(person, bankData, email, subjects);
		Addressy invoiceAddress = Create.address("Rechnungsstraße", "12", "34242", "Daaden");
		Customer customer = Create.customer(person, tutor, invoiceAddress);
		assertNotNull(customer);
		assertEquals(person, customer.getPerson());
		assertEquals(tutor, customer.getTutor());
	}
	
}
