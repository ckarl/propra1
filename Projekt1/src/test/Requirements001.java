package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import applicationLogic.BrainGain;
import dataStructures.Customer;
import dataStructures.StateCustomer;
import dataStructures.Subject;
import dataStructures.Tutor;

public class Requirements001 {
	
	@Test
	public void customer_unique_invoice_number() {
		Customer customer = Requirements001Helper.createCustomer();
				
		BrainGain brainGain = new BrainGain();
		brainGain.setJJJJ("2021");
		brainGain.setMM("05");
		
		assertEquals("202105-MM", brainGain.getUniqueInvoiceNumberOf(customer));
	}
	
	@Test
	public void customer_has_address_and_phonenumber_and_invoiceaddress() {
		Customer customer = Requirements001Helper.createCustomer();

		assertEquals("Musterstraße 5 55555 Musterstadt", customer.getPerson().getAddress().toString());
		assertEquals("017xxxxxxxxx", customer.getPerson().getPhoneNumber().toString());
		assertEquals("Markt 13 57271 Hilchenbach", customer.getInvoiceAddress().toString());
	}
	
	@Test
	public void if_customer_is_state_customer_then_approved_subject_and_max_hour_and_current_hour() {
		StateCustomer stateCustomer = Requirements001Helper.createStateCustomer();
		
		assertTrue(stateCustomer instanceof StateCustomer);
		List<Subject> approvedSubjects = stateCustomer.getApprovedSubjects();
		for(Subject subject : approvedSubjects) {
			assertEquals(25, subject.getMaxHour()); 
			assertEquals(0, subject.getCurrentHout());
		}

	}
	
	@Test
	public void customer_has_tutor() {
		Customer customer = Requirements001Helper.createCustomer();
		
		assertEquals("Tutor Theodor 0123456789 Tutorstraße 23 57074 Kaan-Marienborn DE01 1234 1234 1234 1234 00 tutor@example.com", customer.getTutor().toString());
	}
	
	@Test
	public void customer_test_example_data () {
		Customer customer = Requirements001Helper.createCustomer();
		BrainGain brainGain = new BrainGain();
		brainGain.setJJJJ("2021");
		brainGain.setMM("05");
		
		assertEquals("Kundenname: Max Mustermann\n"
				+ "Tutor: Theodor Tutor\n"
				+ "Fächer: \n"
				+ "Rechnungsnummer: 202105-MM\n"
				+ "Rechnungsadresse: Stadt Hilchenbach - Sozialamt Frau Willert, Markt 13, 57271 Hilchenbach\n"
				+ "Kontakt: 017xxxxxxxxx\n"
				+ "Addresse: Musterstraße 5 55555 Musterstadt\n"
				+ "", brainGain.stringOfDatasetOf(customer));
	}
	
	@Test
	public void state_customer_test_example_data () {
		Customer customer = Requirements001Helper.createStateCustomer();
		BrainGain brainGain = new BrainGain();
		brainGain.setJJJJ("2021");
		brainGain.setMM("05");
		
		assertEquals("Kundenname: Max Mustermann\n"
				+ "Tutor: Theodor Tutor\n"
				+ "Fächer: Deutsch (25), English (25), \n"
				+ "Rechnungsnummer: 202105-MM\n"
				+ "Rechnungsadresse: Stadt Hilchenbach - Sozialamt Frau Willert, Markt 13, 57271 Hilchenbach\n"
				+ "Kontakt: 017xxxxxxxxx\n"
				+ "Addresse: Musterstraße 5 55555 Musterstadt\n"
				+ "", brainGain.stringOfDatasetOf(customer));
	}
	
	@Test
	public void tutor_has_id() {
		Tutor tutor = Requirements001Helper.createTutor();
		BrainGain brainGain = new BrainGain();
		
		assertEquals("THTU", brainGain.getIdOf(tutor));
	}
	
	@Test
	public void tutor_test_example_data() {
		Tutor tutor = Requirements001Helper.createTutorExampleData();
		BrainGain brainGain = new BrainGain();
		
		assertEquals("Tutor: Mustermann Max\n"
				+ "Identifizierung: MUMA\n"
				+ "Fächer/Präferenzen: Alle Hauptfächer, nur Grundschule\n"
				+ "Addresse: Musterstraße 5 55555 Musterstadt\n"
				+ "Kontakt: 017xxxxxxxxx\n"
				+ "Kontodaten Lohn: DEXX XXXX XXXX XXXX XXXX XX\n"
				+ "E-Mail-Adresse (für Lohnabrechnung): max@mustermann.de\n"
				+ "", brainGain.stringOfDatasetOf(tutor));
	}
	
	@Test
	public void braingain_allow_tutor_to_enter_hours() {
		fail("This is Projekt Teil 2. Do GUI first!");

		Tutor tutor = Requirements001Helper.createTutorExampleData();
		BrainGain brainGain = new BrainGain();
		//brainGain.allowTutorToAddHours(tutor, subject, customer, hours)
	}
	
	@Test
	public void braingain_count_current_hour_of_state_customer() {
		fail("This is Projekt Teil 2. Do GUI first!");
	}
	
	@Test
	public void braingain_allow_tutor_to_export_invoice_for_each_customer() {
		fail("This is Projekt Teil 2. Do GUI first!");
	}
	
	@Test
	public void test_invoice_contains_invoice_number_of_customer_and_date_and_tutor() {
		fail("This is Projekt Teil 2. Do GUI first!");
	}
	
	@Test
	public void test_invoice_contains_invoice_positions_with_position_number_and_description_and_date_and_single_price_and_amount_hours_and_sum() {
		fail("This is Projekt Teil 2. Do GUI first!");
	}
	
	@Test
	public void braingain_allow_each_tutor_to_print_own_hours_worked() {
		fail("This is Projekt Teil 2. Do GUI first!");
	}
	
	@Test
	public void test_tutor_view_contains_position_and_tutor_name_and_month_and_taugh_customer_and_given_hours_and_fee() {
		fail("This is Projekt Teil 2. Do GUI first!");
	}

}
