package test;

import java.util.ArrayList;
import java.util.List;

import applicationLogic.Create;
import dataInterfaces.Addressy;
import dataStructures.Customer;
import dataStructures.Person;
import dataStructures.StateCustomer;
import dataStructures.Subject;
import dataStructures.Tutor;

public class Requirements001Helper {

	public static Person createCustomerPerson() {
		String customerLastname = "Mustermann";
		String customerFirstname = "Max";
		Addressy customerAddress = Create.address("Musterstraße", "5", "55555", "Musterstadt");
		String customerPhoneNumber = "017xxxxxxxxx";
		Person customerPerson = Create.person(customerLastname, customerFirstname, customerAddress, customerPhoneNumber);
		return customerPerson;
	}

	public static Tutor createTutor() {
		String tutorLastname = "Tutor";
		String tutorFirstname = "Theodor";
		Addressy tutorAddress = Create.address("Tutorstraße", "23", "57074", "Kaan-Marienborn");
		String tutorPhoneNumber = "0123456789";
		Person tutorPerson = Create.person(tutorLastname, tutorFirstname, tutorAddress, tutorPhoneNumber);
		String bankData = "DE01 1234 1234 1234 1234 00";
		String email = "tutor@example.com";
		String subject = "Alle Hauptfächer, nur Grundschule";
		Tutor tutor = Create.tutor(tutorPerson, bankData, email, subject);
		return tutor;
	}

	public static Tutor createTutorExampleData() {
		String tutorLastname = "Max";
		String tutorFirstname = "Mustermann";
		Addressy tutorAddress = Create.address("Musterstraße", "5", "55555", "Musterstadt");
		String tutorPhoneNumber = "017xxxxxxxxx";
		Person tutorPerson = Create.person(tutorLastname, tutorFirstname, tutorAddress, tutorPhoneNumber);
		String bankData = "DEXX XXXX XXXX XXXX XXXX XX";
		String email = "max@mustermann.de";
		String tutorSubjects = "Alle Hauptfächer, nur Grundschule";
		Tutor tutor = Create.tutor(tutorPerson, bankData, email, tutorSubjects);
		return tutor;
	}
	
	public static Customer createCustomer() {
		Person customerPerson = createCustomerPerson();
		Tutor tutor = Requirements001Helper.createTutor();
		Addressy invoiceAddress = Create.invoiceAddress("Stadt Hilchenbach", "Sozialamt Frau Willert", "Markt", "13", "57271", "Hilchenbach");
		Customer customer = Create.customer(customerPerson, tutor, invoiceAddress);
		return customer;
	}

	public static StateCustomer createStateCustomer() {
		Customer customer = Requirements001Helper.createCustomer();
		List<Subject> approvedSubjects = Requirements001Helper.createApprovedSubjects();
		StateCustomer stateCustomer = Create.stateCustomer(customer, approvedSubjects);
		return stateCustomer;
	}

	private static List<Subject> createApprovedSubjects() {
		List<Subject> approvedSubjects = new ArrayList<>();
		approvedSubjects.add(Create.subject("Deutsch", 25, 0));
		approvedSubjects.add(Create.subject("English", 25, 0));
		return approvedSubjects;
	}

}
