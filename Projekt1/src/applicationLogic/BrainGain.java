package applicationLogic;

import java.util.List;

import dataInterfaces.Addressy;
import dataStructures.Customer;
import dataStructures.Person;
import dataStructures.StateCustomer;
import dataStructures.Subject;
import dataStructures.Tutor;

public class BrainGain {

	private String jjjj;
	private String mm;
	
	public void setJJJJ(String jjjj) {
		this.jjjj = jjjj;
	}

	public void setMM(String mm) {
		this.mm = mm;
	}

	public String getUniqueInvoiceNumberOf(Customer customer) {
		return jjjj + mm + "-" + getInitialsOf(customer);
	}

	private String getInitialsOf(Customer customer) {
		return String.valueOf(customer.getPerson().getFirstname().charAt(0)) + 
				String.valueOf(customer.getPerson().getLastname().charAt(0));
	}

	public String stringOfDatasetOf(Customer customer) {
		StringBuilder builder = new StringBuilder();
		builder.append(keyValue("Kundenname", firstAndLastNameOf(customer)));
		builder.append(keyValue("Tutor", firstAndLastNameOf(customer.getTutor())));
		builder.append(keyValue("Fächer", subjectsOf(customer)));
		builder.append(keyValue("Rechnungsnummer", getUniqueInvoiceNumberOf(customer)));
		builder.append(keyValue("Rechnungsadresse", getInvoiceAddressOf(customer)));
		builder.append(keyValue("Kontakt", customer.getPerson().getPhoneNumber()));
		builder.append(keyValue("Addresse", getAddressOf(customer)));
		return builder.toString();
	}

	private String getAddressOf(Customer customer) {
		return customer.getPerson().getAddress().toString();
	}

	private String getInvoiceAddressOf(Customer customer) {
		Addressy address = customer.getInvoiceAddress();
		if (address == null) {
			address = customer.getPerson().getAddress();
		}
		return address.toInvoiceAddressString();
	}

	private String subjectsOf(Customer customer) {
		StringBuilder builder = new StringBuilder("");
		if (customer instanceof StateCustomer) {
			StateCustomer stateCustomer = (StateCustomer) customer;
			List<Subject> subjects = stateCustomer.getApprovedSubjects();
			for (Subject subject : subjects) {
				builder.append(subject.getName() + " (" + subject.getMaxHour() + "), ");
			}
		}
		return builder.toString();
	}

	private Object keyValue(String key, String value) {
		return key + ": " + value + "\n";
	}

	private String firstAndLastNameOf(Customer customer) {
		return firstAndLastNameOf(customer.getPerson());
	}
	
	private String firstAndLastNameOf(Tutor tutor) {
		return firstAndLastNameOf(tutor.getPerson());
	}

	private String firstAndLastNameOf(Person person) {
		return person.getFirstname() + " " + person.getLastname();
	}

	public String getIdOf(Tutor tutor) {
		return (String.valueOf(tutor.getPerson().getFirstname().substring(0,2)) + 
				String.valueOf(tutor.getPerson().getLastname().substring(0,2))).toUpperCase();

	}

	public String stringOfDatasetOf(Tutor tutor) {
		StringBuilder builder = new StringBuilder();
		builder.append(keyValue("Tutor", firstAndLastNameOf(tutor)));
		builder.append(keyValue("Identifizierung", getIdOf(tutor)));
		builder.append(keyValue("Fächer/Präferenzen", tutor.getSubjects()));
		builder.append(keyValue("Addresse", getAddressOf(tutor)));
		builder.append(keyValue("Kontakt", tutor.getPerson().getPhoneNumber()));
		builder.append(keyValue("Kontodaten Lohn", tutor.getBankData()));
		builder.append(keyValue("E-Mail-Adresse (für Lohnabrechnung)", tutor.getEmail().toString()));
		return builder.toString();
	}

	private String getAddressOf(Tutor tutor) {
		return tutor.getPerson().getAddress().toString();
	}

}
