package applicationLogic;

import java.util.List;

import dataInterfaces.Addressy;
import dataStructures.AddressFirstnameLastnameStreetHouseNrZipCity;
import dataStructures.AddressStreetHouseNrZipCity;
import dataStructures.Customer;
import dataStructures.Person;
import dataStructures.StateCustomer;
import dataStructures.Subject;
import dataStructures.Tutor;

public class Create {
	
	public static AddressStreetHouseNrZipCity address(String street, String houseNr, String zip, String city) {
		AddressStreetHouseNrZipCity address = new AddressStreetHouseNrZipCity();
		address.setStreet(street);
		address.setHouseNr(houseNr);
		address.setZip(zip);
		address.setCity(city);
		return address;
	}

	public static Person person(String lastname, String firstname, Addressy address, String phoneNumber) {
		Person person = new Person();
		person.setLastname(lastname);
		person.setFirstname(firstname);
		person.setAddress(address);
		person.setPhoneNumber(phoneNumber);
		return person;
	}
	
	public static Tutor tutor(Person person, String bankData, String email, String subjects) {
		Tutor tutor = new Tutor();
		tutor.setPerson(person);
		tutor.setBankData(bankData);
		tutor.setEmail(email);
		tutor.setSubjects(subjects);
		return tutor;
	}
	
	public static Customer customer(Person person, Tutor tutor, Addressy invoiceAddress) {
		Customer customer = new Customer();
		customer.setPerson(person);
		customer.setTutor(tutor);
		customer.setInvoiceAddress(invoiceAddress);
		return customer;
	}
	
	public static StateCustomer stateCustomer(Customer customer, List<Subject> approvedSubject) {
		StateCustomer stateCustomer = new StateCustomer();
		stateCustomer.setPerson(customer.getPerson());
		stateCustomer.setTutor(customer.getTutor());
		stateCustomer.setInvoiceAddress(customer.getInvoiceAddress());
		stateCustomer.setApprovedSubjects(approvedSubject);
		return stateCustomer;
	}

	public static Addressy invoiceAddress(String firstname, String lastname, String street, String houseNr, String zip, String city) {
		AddressFirstnameLastnameStreetHouseNrZipCity invoiceAddress = new AddressFirstnameLastnameStreetHouseNrZipCity();
		invoiceAddress.setFirstname(firstname);
		invoiceAddress.setLastname(lastname);
		invoiceAddress.setStreet(street);
		invoiceAddress.setHouseNr(houseNr);
		invoiceAddress.setZip(zip);
		invoiceAddress.setCity(city);
		return invoiceAddress;
	}

	public static Subject subject(String name, int maxHour, int currentHour) {
		Subject subject = new Subject();
		subject.setName(name);
		subject.setMaxHour(maxHour);
		subject.setCurrentHour(currentHour);
		return subject;
	}

}
