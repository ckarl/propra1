package dataInterfaces;

import java.util.List;

import dataStructures.Subject;

public interface HasApprovedSubjects {
	
	void setApprovedSubjects(List<Subject> approvedSubjects);
	
	List<Subject> getApprovedSubjects();

}
