package dataInterfaces;

public interface HasEmail {
	
	void setEmail(String Email);
	
	String getEmail();

}
