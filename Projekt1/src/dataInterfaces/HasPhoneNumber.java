package dataInterfaces;

public interface HasPhoneNumber {
	
	String getPhoneNumber();
	
	void setPhoneNumber(String phoneNumber);

}
