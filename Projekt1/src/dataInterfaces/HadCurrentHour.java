package dataInterfaces;

public interface HadCurrentHour {

	void setCurrentHour(Number currentHour);
	
	Number getCurrentHour();
}
