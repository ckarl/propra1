package dataInterfaces;

public interface HasInvoiceAddress {
	
	Addressy getInvoiceAddress();
	
	void setInvoiceAddress(Addressy address);

}
