package dataInterfaces;

public interface HasAddressy {
		
	void setAddress(Addressy address);
	
	Addressy getAddress();

}
