package dataInterfaces;

import dataStructures.Person;

public interface IsPerson {
	
	Person getPerson();
	
	void setPerson(Person person);

}
