package dataInterfaces;

import dataStructures.Customer;

public interface isCustomer {

	Customer getCustomer();
	
	void setCustomer(Customer customer);
}
