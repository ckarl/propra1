package dataInterfaces;

public interface HasApprovedSubject {
	
	void setApprovedSubject(String approvedSubject);
	
	String getApprovedSubject();

}
