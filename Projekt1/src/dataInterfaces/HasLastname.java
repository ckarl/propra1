package dataInterfaces;

public interface HasLastname {
	
	void setLastname(String lastname);
	
	String getLastname();

}
