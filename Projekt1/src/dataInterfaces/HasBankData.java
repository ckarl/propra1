package dataInterfaces;

public interface HasBankData {

	void setBankData(String BankData);
	
	String getBankData();
}
