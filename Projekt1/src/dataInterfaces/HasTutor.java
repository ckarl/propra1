package dataInterfaces;

import dataStructures.Tutor;

public interface HasTutor {
	
	public Tutor getTutor();
	
	public void setTutor(Tutor tutor);

}
