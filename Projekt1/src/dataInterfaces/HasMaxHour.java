package dataInterfaces;

public interface HasMaxHour {
	
	void setMaxHour(Number maxHour);
	
	Number getMaxHour();

}
