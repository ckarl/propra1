package dataInterfaces;

public interface HasFirstname {
	
	String getFirstname();
	
	void setFirstname(String firstname);

}
