package dataStructures;

public class AddressStreetHouseNrZipCity extends Address {

	private String street;
	private String houseNr;
	private String zip;
	private String city;
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getHouseNr() {
		return houseNr;
	}
	
	public void setHouseNr(String houseNr) {
		this.houseNr = houseNr;
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String toString() {
		return street + " " + houseNr + " " + zip + " " + city;
	}

	@Override
	public String toInvoiceAddressString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getStreet() + " " + getHouseNr() + ", ");
		builder.append(getZip() + " " + getCity());
		return builder.toString();
	}
}
