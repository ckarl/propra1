package dataStructures;

import dataInterfaces.Addressy;
import dataInterfaces.HasInvoiceAddress;
import dataInterfaces.HasTutor;
import dataInterfaces.IsPerson;

public class Customer implements IsPerson, HasTutor, HasInvoiceAddress {
	
	private Tutor tutor;
	private Person person;
	private Addressy invoiceAddress;
	
	@Override
	public String toString() {
		return this.person.toString() + " " + tutor;
	}

	@Override
	public Person getPerson() {
		return this.person;
	}

	@Override
	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public Tutor getTutor() {
		return this.tutor;
	}

	@Override
	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

	@Override
	public Addressy getInvoiceAddress() {
		return this.invoiceAddress;
	}

	@Override
	public void setInvoiceAddress(Addressy address) {
		this.invoiceAddress = address;
	}

}
