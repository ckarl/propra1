package dataStructures;



public class FeeItem {
	
	private int fee_PositionNr;
	private Customer customer;
	private double hour_Tutor;
	private double hourly_Wage;
	private double total_Wage;
	private double salary;
	
	
	public FeeItem(int fee_PositionNr, Customer customer, double hour_Tutor, double hourly_Wage, double total_Wage, double salary) {
		
		this.fee_PositionNr = fee_PositionNr;
		this.customer = customer;
		this.hour_Tutor = hour_Tutor;
		this.hourly_Wage = hourly_Wage;
		this.total_Wage = total_Wage;
		this.salary = salary;
	}
	
	
	
	
	public int getFee_PositionNr() {
		return fee_PositionNr;
	}
	
	public void setFee_PositionNr(int fee_PositionNr) {
		this.fee_PositionNr = fee_PositionNr;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public double getHour_Tutor() {
		return hour_Tutor;
	}
	
	public void setHour_Tutor(double hour_Tutor) {
		this.hour_Tutor = hour_Tutor;
	}
	
	public double getHourly_Wage() {
		return hourly_Wage;
	}
	
	public void setHourly_Wage(double hourly_Wage) {
		this.hourly_Wage = hourly_Wage;
	}
	
	public double getTotal_Wage() {
		return total_Wage;
	}
	
	public void setTotal_Wage(double total_Wage) {
		this.total_Wage = total_Wage;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	
	
	public String toString() {
		return " " + fee_PositionNr + " " + customer + " " + hour_Tutor + " " + hourly_Wage + " " + total_Wage + " " + salary;
		}

	

}
