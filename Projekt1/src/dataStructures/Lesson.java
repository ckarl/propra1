package dataStructures;

import java.util.Date;

public class Lesson {
	
	private String lesson_Name;
	private String lesson_Level;
	private double lesson_Price;
	private double duration;
	private Date lesson_Time;
	
	public Lesson(String lesson_Name, String lesson_Level, double lesson_Price, double duration, Date lesson_Time) {
		this.lesson_Name = lesson_Name;
		this.lesson_Level = lesson_Level;
		this.lesson_Price = lesson_Price;
		this.duration = duration;
		this.lesson_Time = lesson_Time;
	}
	
	public String getLesson_Name() {
		return lesson_Name;
	}
	
	public void setLesson_Name(String lesson_Name) {
		this.lesson_Name = lesson_Name;
	}
	
	public String getLesson_Level() {
		return lesson_Level;
	}
	
	public void setLesson_Level(String lesson_Level) {
		this.lesson_Level = lesson_Level;
	}
	
	public double getLesson_Price() {
		return lesson_Price;
	}
	
	public void setLesson_Price(double lesson_Price) {
		this.lesson_Price = lesson_Price;
	}
	
	public double getDuration() {
		return duration;
	}
	
	public void setDuration(double duration) {
		this.duration = duration;
	}
	
	public Date getLesson_Time() {
		return lesson_Time;
	}
	
	public void setLesson_Time(Date lesson_Time) {
		this.lesson_Time = lesson_Time;
	}
	
	
	
	public String toString() {
		return " " + lesson_Name + " " + lesson_Level + " " + lesson_Price + " " + duration + " " + lesson_Time; 
	}
	
	

	

}
