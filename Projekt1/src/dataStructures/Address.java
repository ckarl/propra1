package dataStructures;

import dataInterfaces.Addressy;

public class Address implements Addressy {

	@Override
	public String toInvoiceAddressString() {
		return "Class Address should not be instantiated";
	}
	
}
