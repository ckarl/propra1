package dataStructures;

import java.util.ArrayList;
import java.util.List;

import dataInterfaces.HasApprovedSubjects;

public class StateCustomer extends Customer implements HasApprovedSubjects {
	
	private Customer customer;
	private List<Subject> approvedSubjects = new ArrayList<>();

	@Override
	public String toString() {
		return customer.toString() + " " + approvedSubjects.toString();
	}

	@Override
	public void setApprovedSubjects(List<Subject> approvedSubjects) {
		this.approvedSubjects = approvedSubjects;
	}

	@Override
	public List<Subject> getApprovedSubjects() {
		return this.approvedSubjects;
	}
	
}
