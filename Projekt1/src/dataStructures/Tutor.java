package dataStructures;

import dataInterfaces.HasBankData;
import dataInterfaces.HasEmail;
import dataInterfaces.IsPerson;

public class Tutor implements IsPerson, HasEmail, HasBankData, HasSubjects {

	private Person person;
	private String bankData;
	private String email;
	private String subjects;

	@Override
	public String getBankData() {
		return bankData;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public Person getPerson() {
		return this.person;
	}

	@Override
	public void setBankData(String bankData) {
		this.bankData = bankData;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return person.toString() + " " + bankData + " " + email;
	}

	@Override
	public String getSubjects() {
		return this.subjects;
	}

	@Override
	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

}
