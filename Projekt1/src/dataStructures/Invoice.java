package dataStructures;

public class Invoice {
	
	private int invoiceNr;
	private InvoiceLine invoiceLine;
	private Customer customer;
	
	
	public Invoice(int invoiceNr, InvoiceLine invoiceLine, Customer customer) {
		this.invoiceNr = invoiceNr;
		this.invoiceLine = invoiceLine;
		this.customer = customer;
	}
	
	public int getInvoiceNr() {
		return invoiceNr;
	}
	
	public void setInvoiceNr(int invoiceNr) {
		this.invoiceNr = invoiceNr;
	}
	
	public InvoiceLine getInvoiceLine() {
		return invoiceLine;
	}
	
	public void setInvoiceLine(InvoiceLine invoiceLine) {
		this.invoiceLine = invoiceLine;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	public String toString() {
		return " " + invoiceNr + " " + invoiceLine + " " + customer;
	}

}
