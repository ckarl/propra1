package dataStructures;

import java.util.Date;

public class FeeNotice {
	
	private Date month;
	private FeeItem feeItem;
	
	
	public FeeNotice(Date month, FeeItem feeItem) {
		
		this.month = month;
		this.feeItem = feeItem;
	}
	
	
	public Date getMonth(){
		return month;
	}
	
	public void setMonth(Date month) {
		this.month = month;
	}
	
	public FeeItem getFeeItem() {
		return feeItem;
	}
	
	public void setFeeItem(FeeItem feeItem) {
		this.feeItem = feeItem;
	}
	
	
	
	public String toString() {
		return " " + month + " " + feeItem;
	}

	

}
