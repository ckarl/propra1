package dataStructures;

import dataInterfaces.Addressy;
import dataInterfaces.HasAddressy;

public class Institute implements HasAddressy{
	
	private String institute_Name;
	private String recipient;
	private Addressy address;
	
	public Institute(String institute_Name, String recipient, Address address) {
		this.institute_Name = institute_Name;
		this.recipient = recipient;
		this.address = address;		
	}
	
	public String getInstitute_Name() {
		return institute_Name;
	}
	
	public void setInstitute_Name(String institute_Name) {
		this.institute_Name = institute_Name;
	}
		
	@Override
	public String toString() {
		return institute_Name + " " + recipient + " " + address.toString();
	}

	@Override
	public void setAddress(Addressy address) {
		this.address = address;
	}

	@Override
	public Addressy getAddress() {
		return this.address;
	}

}
