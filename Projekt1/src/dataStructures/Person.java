package dataStructures;

import dataInterfaces.Addressy;
import dataInterfaces.HasAddressy;
import dataInterfaces.HasFirstname;
import dataInterfaces.HasLastname;
import dataInterfaces.HasPhoneNumber;

public class Person implements HasLastname, HasFirstname, HasPhoneNumber, HasAddressy {
	
	private String lastname;
	private String firstname;
	private String phoneNumber;
	private Addressy address;	
		
	@Override
	public String toString() {
		return lastname + " " + firstname + " " + phoneNumber + " " + address;
	}

	@Override
	public void setAddress(Addressy address) {
		this.address = address;
	}

	@Override
	public Addressy getAddress() {
		return this.address;
	}

	@Override
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	@Override
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String getFirstname() {
		return this.firstname;
	}

	@Override
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Override
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String getLastname() {
		return this.lastname;
	}

}
