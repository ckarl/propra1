package dataStructures;

import dataInterfaces.HasFirstname;
import dataInterfaces.HasLastname;

public class AddressFirstnameLastnameStreetHouseNrZipCity extends AddressStreetHouseNrZipCity implements HasFirstname, HasLastname {

	private String firstname;
	private String lastname;

	@Override
	public String getFirstname() {
		return this.firstname;
	}

	@Override
	public String getLastname() {
		return this.lastname;
	}

	@Override
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Override
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toInvoiceAddressString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getFirstname()+ " - " + getLastname() + ", ");
		builder.append(getStreet() + " " + getHouseNr() + ", ");
		builder.append(getZip() + " " + getCity());
		return builder.toString();
	}

}
