package dataStructures;

public interface HasSubjects {

	String getSubjects();
	
	void setSubjects(String subjects);
}
