package dataStructures;

import dataInterfaces.HasCurrentHour;
import dataInterfaces.HasMaxHour;
import dataInterfaces.HasName;

public class Subject  implements HasName, HasMaxHour, HasCurrentHour {

	private Number currentHour;
	private Number maxHour;
	private String name;

	@Override
	public void setCurrentHour(Number currentHour) {
		this.currentHour = currentHour;
	}

	@Override
	public Number getCurrentHout() {
		return this.currentHour;
	}

	@Override
	public void setMaxHour(Number maxHour) {
		this.maxHour = maxHour;
	}

	@Override
	public Number getMaxHour() {
		return this.maxHour;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}
