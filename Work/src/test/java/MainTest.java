package test.java;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import main.java.Main;

public class MainTest {
	
	@Test
	public void testEcho() {
		String expected = "message";
		String actual = Main.echo(expected);
		assertEquals(expected, actual);
	}

}
