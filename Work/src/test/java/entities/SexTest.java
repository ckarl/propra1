package test.java.entities;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import main.java.entities.Sex;

public class SexTest {
	
	@Test
	public void test_male() {
		Sex male = Sex.MALE;
		assertEquals("male", male.getIdentifier());
	}

	@Test
	public void test_female() {
		Sex male = Sex.FEMALE;
		assertEquals("female", male.getIdentifier());
	}
	
	@Test
	public void test_equality() {
		Sex firstFemale = Sex.FEMALE;
		Sex secondFemale = Sex.FEMALE;
		assertEquals(firstFemale, secondFemale);
	}

}
