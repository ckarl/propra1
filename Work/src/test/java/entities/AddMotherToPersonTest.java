package test.java.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;

public class AddMotherToPersonTest {
	
	private static final String personNameString = "person";
	private static final String motherNameString = "mother";
	
	private Person person;
	private Person mother;
	
	@BeforeEach
	public void beforeEach() {
		Name personeName = new Name(personNameString);
		Sex personSex = Sex.FEMALE;
		this.person = new Person(personeName, personSex);
		Name motherName = new Name(motherNameString);
		Sex motherSex = Sex.FEMALE;
		this.mother = new Person(motherName, motherSex);
	}

	@Test
	public void given_person_when_set_mother_then_gotten_father_expected() {
		person.setMother(mother);
		Person actualMother = person.getMother();
		assertEquals(mother, actualMother);
	}
	
	@Test
	public void given_person_when_set_mother_then_gotten_children_expected() {
		person.setMother(mother);
		List<Person> children = mother.getChildren();
		assertTrue(children.contains(person));
	}
}