package test.java.entities;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;

import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;

public class PersonTest {

	@Test
	public void test_creation_of_a_person() {
		Name name = new Name("aName");
		Sex sex = Sex.FEMALE;
		Person person = new Person(name, sex);
		assertNotNull(person);
	}

	@Test
	public void given_person_when_toString_then_nothing() {
		Name name = new Name("aName");
		Sex sex = Sex.FEMALE;
		Person person = new Person(name, sex);
		System.out.println(person);
	}
}
