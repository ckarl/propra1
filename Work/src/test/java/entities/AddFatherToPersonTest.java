package test.java.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;

public class AddFatherToPersonTest {
	
	private static final String personNameString = "person";
	private static final String fatherNameString = "father";
	
	private Person person;
	private Person father;
	
	@BeforeEach
	public void beforeEach() {
		Name personeName = new Name(personNameString);
		Sex personSex = Sex.FEMALE;
		this.person = new Person(personeName, personSex);
		Name fatherName = new Name(fatherNameString);
		Sex fatherSex = Sex.MALE;
		this.father = new Person(fatherName, fatherSex);
	}

	@Test
	public void given_person_when_set_father_then_gotten_father_expected() {
		person.setFather(father);
		Person actualFather = person.getFather();
		assertEquals(father, actualFather);
	}
	
	@Test
	public void given_person_when_set_father_then_gotten_children_expected() {
		person.setFather(father);
		List<Person> children = father.getChildren();
		assertTrue(children.contains(person));
	}
}

