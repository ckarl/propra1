package test.java.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;

public class AddChildToPersonTest {
	
	private static final String motherNameString = "mother";
	private static final String childNameString = "child";
	private static final String fatherNameString = "father";
	
	private Person mother;
	private Person child;
	private Person father;
	
	@BeforeEach
	public void beforeEach() {
		Name personName = new Name(motherNameString);
		Sex personSex = Sex.FEMALE;
		mother = new Person(personName, personSex);
		Name childName = new Name(childNameString);
		Sex childSex = Sex.FEMALE;
		child = new Person(childName, childSex);
		Name fatherName = new Name(fatherNameString);
		Sex fatherSex = Sex.MALE; 
		father = new Person(fatherName, fatherSex);
	}
	
	@Test
	public void given_person_when_adding_child_then_gotten_children_expected() {
		mother.addChild(child);
		List<Person> children = mother.getChildren();
		assertTrue(children.contains(child));
	}
	
	@Test
	public void given_person_when_adding_child_then_gotten_mother_expected() {
		mother.addChild(child);
		Person actualMother = child.getMother();
		Person actualFather = child.getFather();
		assertEquals(mother, actualMother);
		assertEquals(null, actualFather);
	}

	@Test
	public void given_person_when_adding_child_then_gotten_father_expected() {
		father.addChild(child);
		Person actualMother = child.getMother();
		Person actualFather = child.getFather();
		assertEquals(null, actualMother);
		assertEquals(father, actualFather);
	}
}
