package test.java.stuff;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;
import main.java.stuff.From;

public class FromTest {
	
	private static final String personNameString = "Person";
	
	@Test
	public void given_empty_list_when_toJson_then_equals_expected() {
		List<Person> persons = new ArrayList<>();
		From from = new From(persons);
		String s = from.toJson();
		assertEquals("[]", s);
	}
	
	@Test
	public void given_one_persons_when_toJson_then_equals_expected() {
		Name personName = new Name(personNameString);
		Sex personSex = Sex.FEMALE;
		Person person = new Person(personName, personSex);
		List<Person> persons = new ArrayList<>();
		persons.add(person);
		From from = new From(persons);
		String s = from.toJson();
		assertEquals("[{\"children\":[],\"sex\":{\"identifier\":\"female\"},\"name\":{\"firstName\":\"Person\",\"lastName\":\"\"}}]", s);
	}

}
