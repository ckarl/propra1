package test.java.stuff;

import static org.junit.Assert.assertTrue;

import java.util.Scanner;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserInputTest {

	private Scanner scanner;
	
	@BeforeEach
	public void beforeEach() {
		scanner = new Scanner(System.in);
	}
	
	@AfterEach
	public void afterEach() {
		scanner.close();
	}
	
	@Test
	public void test_dummy() {
		assertTrue(true);
	}
	
// 	@Test
	public void test_user_input_integer() {
		System.out.print("Enter first number- ");  
		int a = scanner.nextInt();  
		System.out.print("Enter second number- ");  
		int b = scanner.nextInt();  
		System.out.print("Enter third number- ");  
		int c = scanner.nextInt();  
		int d = a + b + c;  
		System.out.println("Total= " +d);  
	}

}
