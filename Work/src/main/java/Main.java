package main.java;

import java.util.ArrayList;

import main.java.entities.Context;
import main.java.entities.Person;
import main.java.ui.Menu;
import main.java.ui.cmd.Cmd000Welcome;
import main.java.ui.swing.Swing000Welcome;

/**
 * This class is the starting point of the program. 
 * As such it is the only place where a main method is to be found
 * 
 * @author user
 *
 */

public class Main {
	
	private static final boolean USE_GRAPHICAL_USER_INTERFACE = true;
	
	public static void main(String[] args) {
		Context context = new Context();
		context.setPersons(new ArrayList<Person>());
		Menu.setContext(context);
		Menu welcome;
		if (Main.USE_GRAPHICAL_USER_INTERFACE) {
			welcome = new Swing000Welcome();
		} else {
			welcome = new Cmd000Welcome();
		}
		
		try {
			
		
		welcome.execute();
		}
		catch(NullPointerException e) {
			System.out.println("NullPointerException thrown!");
		}
		
    }

	/**
 	 * This is a dummy method stub for setting up
	 * MainTest.java.
	 * 
	 * @param message Message to be returned
	 * @return message passed
	 */
	public static String echo (String message) {
		return message;
	}
}