package main.java.stuff;

import java.util.List;

import org.json.JSONArray;

import main.java.entities.Person;

public class From {

	private List<Person> persons;

	public From(List<Person> persons) {
		this.persons = persons;
	}

	public String toJson() {
		JSONArray jsonArray = new JSONArray(persons);
		return jsonArray.toString();
	}

}
