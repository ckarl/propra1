package main.java.entities;

public class Sex {
	
	public static final Sex FEMALE = new Sex("female");
	public static final Sex MALE = new Sex("male");

	private String identifier;
	
	public Sex(String identifier) {
		this.identifier = identifier;
	}
	
	public String getIdentifier() {
		return this.identifier;
	}
	
	@Override
	public int hashCode() {
		return this.identifier.hashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		boolean isEqual = false;
		if (other instanceof Sex) {
			isEqual = (this.hashCode() == other.hashCode());
		}
		return isEqual;
	}
	
	@Override
	public String toString() {
		return this.identifier;
	}
}
