package main.java.entities;

import java.util.ArrayList;
import java.util.List;

public class Context {

	private ArrayList<Person> persons;

	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
	}

	public boolean addPerson(Person personToBeAdded) {
		return this.persons.add(personToBeAdded);
	}

	public List<Person> getPersons() {
		return this.persons;
	}

}
