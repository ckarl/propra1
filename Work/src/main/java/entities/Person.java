package main.java.entities;

import java.util.ArrayList;
import java.util.List;

public class Person {
	
	private final Name name;
	private final Sex sex;
	private final ArrayList<Person> children;
	
	private Person father;
	private Person mother;
	
	public Person(Name name, Sex sex) {
		this.name = name;
		this.sex = sex;
		this.children = new ArrayList<Person>();
	}
	
	public Name getName() {
		return this.name;
	}
		
	/**
	 * This method returns the contents of Person for debugging only. 
	 * Do not rely on it for other displays of Person instances, write
	 * methods for that. The output of this method will change in future
	 * in order to accommodate debugging.
	 */
	@Override
	public String toString() {
		return this.name.toString() + ", " + this.sex.toString() + ", children: " + children.size();
	}

	public boolean setFather(Person father) {
		boolean fatherIsSet = this.setFatherInternally(father);
		boolean childIsAdded = father.addChildInternally(this);
		return fatherIsSet && childIsAdded;
	}
	
	private boolean setFatherInternally(Person father) {
		boolean fatherIsSet = false;
		if (father.sex.equals(Sex.MALE)) {
			this.father = father;
			fatherIsSet = true;
		}
		return fatherIsSet;
	}

	private boolean addChildInternally(Person child) {
		return this.children.add(child);
	}

	public Person getFather() {
		return this.father;
	}

	public List<Person> getChildren() {
		return this.children;
	}

	public boolean setMother(Person mother) {
		boolean motherIsSet = this.setMotherInternally(mother);
		boolean childIsAdded = mother.addChildInternally(this);
		return motherIsSet && childIsAdded;		
	}

	private boolean setMotherInternally(Person mother) {
		boolean motherIsSet = false;
		if (mother.sex.equals(Sex.FEMALE)) {
			this.mother = mother;
			motherIsSet = true;
		}
		return motherIsSet;
	}

	public Person getMother() {
		return this.mother;
	}

	public boolean addChild(Person child) {
		boolean childIsAdded = this.addChildInternally(child);
		boolean parentIsSet = false;
		if (childIsAdded) {
			parentIsSet = this.setParentOf(child);
		}
		return childIsAdded && parentIsSet;		
	}

	private boolean setParentOf(Person child) {
		boolean parentIsSet = false;
		if(this.sex.equals(Sex.FEMALE)) {
			parentIsSet = child.setMotherInternally(this);
		} else if (this.sex.equals(Sex.MALE)) {
			parentIsSet = child.setFatherInternally(this);
		} else {
			throw new RuntimeException("Parent sex unexpected: " + this.toString());
		}
		return parentIsSet;
	}

	public Sex getSex() {
		return this.sex;
	}

}
