package main.java.entities;

public class Name {

	private final String firstName;
	private final String lastName;

	public Name(String firstName) {
		this.firstName = firstName;
		this.lastName = "";
	}
	
	public Name(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	@Override
	public String toString() {
		return this.firstName + " " + this.lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

}
