package main.java.ui.cmd;

import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;
import main.java.ui.Menu;

public class Cmd010AddPerson extends Menu{

	@Override
	public Object execute() {
		Name name =(Name) new Cmd011AddPersonName().execute();
		Sex sex = (Sex) new Cmd012AddPersonSex().execute();
		Person personToBeAdded = new Person(name, sex);
		print(personToBeAdded.toString());
		return addPerson(personToBeAdded);
	}

}
