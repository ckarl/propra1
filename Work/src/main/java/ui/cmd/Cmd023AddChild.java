package main.java.ui.cmd;

import java.util.List;
import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd023AddChild extends Menu{
	
	@Override
	public Object execute() {
		boolean isChildAdded = false;
		List<Person> persons = createLocalCopyOfPersons();
		Person child = (Person) new Cmd025SelectChild(persons).execute();
		Person parent = (Person) new Cmd024SelectPerson(persons).execute();		
		isChildAdded = parent.addChild(child);
		return isChildAdded;
	}			
}