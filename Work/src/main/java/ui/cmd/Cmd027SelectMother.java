package main.java.ui.cmd;

import java.util.List;

import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd027SelectMother extends Menu{

	private List<Person> persons;

	public Cmd027SelectMother(List<Person> persons) {
		this.persons = persons;
	}

	@Override
	public Object execute() {
		List<Person> females = collectFemalesFrom(persons);
		Person mother = selectPersonFromList(females);
		restartIfNull(mother);
		print("Selected mother: " + mother);
		return mother;
	}

}
