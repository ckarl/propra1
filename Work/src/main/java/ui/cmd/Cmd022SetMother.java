package main.java.ui.cmd;

import java.util.List;
import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd022SetMother extends Menu{

	// TODO Have an entry to jump back // abort
	
	@Override
	public Object execute() {
		boolean isMotherSet = false;
		List<Person> persons = createLocalCopyOfPersons();
		Person child = (Person) new Cmd025SelectChild(persons).execute();
		Person mother = (Person) new Cmd027SelectMother(persons).execute();				
		isMotherSet = child.setMother(mother);
		return isMotherSet;
	}
			
}