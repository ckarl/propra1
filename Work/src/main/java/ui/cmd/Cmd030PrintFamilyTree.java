package main.java.ui.cmd;

import java.util.ArrayList;

import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd030PrintFamilyTree extends Menu {
	
	@Override
	public Object execute() {
		ArrayList<Person> persons = createLocalCopyOfPersons();				
		Person generation0 = (Person) new Cmd025SelectChild(persons).execute();
		return this.printFamilyTree(generation0);
	}

	private boolean printFamilyTree(Person generation0) {
		StringBuilder builder = new StringBuilder();
		preOrderFamilyTree(builder, generation0, 0);
		return print(builder.toString());
	}

	private void preOrderFamilyTree(StringBuilder builder, Person person, int indent) {
		if (person == null ) {
			return;
		}
		builder.append(this.indentAndPerson(indent, person));
		indent++;
		preOrderFamilyTree(builder, person.getFather(), indent);
		preOrderFamilyTree(builder, person.getMother(), indent);
	}

	private String indentAndPerson(int indent, Person person) {
		return this.indent(indent) + person.getName().getFirstName();
	}

	private String indent(int indent) {
		String indentStr = "";
		for (int i = 0; i < indent; i++) {
			indentStr = indentStr + " ";
		}
		return indentStr;
	}	

}
