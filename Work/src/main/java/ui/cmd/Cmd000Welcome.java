package main.java.ui.cmd;

import main.java.ui.Menu;

public class Cmd000Welcome extends Menu {
	
	@Override
	public Object execute() {
		print("1: Exit");
		print("2: Add a person");
		print("3: Set father");
		print("4: Set mother");
		print("5: Add child");
		print("6: Print family tree");
		print("Else: Restart to WelcomeMenue.");
		int response = getResponseAsIntOrMinusOne();
		switch (response) {
		case 1:
			new Cmd001Exit().execute();
			break;
		case 2: 
			new Cmd010AddPerson().execute();
			break;
		case 3:
			new Cmd021SetFather().execute();
			break;
		case 4:
			new Cmd022SetMother().execute();
			break;
		case 5:
			new Cmd023AddChild().execute();
			break;
		case 6:
			new Cmd030PrintFamilyTree().execute();
			break;
		default:
			break;
		}
		return new Cmd000Welcome().execute();
	}

}
