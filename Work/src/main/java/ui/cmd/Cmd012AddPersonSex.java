package main.java.ui.cmd;

import main.java.entities.Sex;
import main.java.ui.Menu;

public class Cmd012AddPersonSex extends Menu{

	@Override
	public Object execute() {
	    Sex sex = null;
		print("Enter Sex:");
		print("1: male");
	    print("2: female");
	    int sexInt = getResponseAsIntOrMinusOne();
		switch (sexInt) {
		case 1:
			sex = Sex.MALE;
			break;
		case 2:
			sex = Sex.FEMALE;
			break;
		default:
			print(sexInt+ " invalid. Restart.");
			new Cmd012AddPersonSex().execute();
			break;
		}
		return sex;
	}

}
