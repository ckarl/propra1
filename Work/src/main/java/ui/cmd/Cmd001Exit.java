package main.java.ui.cmd;

import main.java.ui.Menu;

public class Cmd001Exit extends Menu {

	@Override
	public Object execute() {
		print("1: Yes, exit.");
		print("Else: Restart to WelcomeMenue.");
		int response = getResponseAsIntOrMinusOne();
		switch (response) {
		case 1:
			print("Exiting. Have a nice day.");
			System.exit(0);
			break;
		default:
			break;
		}
		return new Cmd000Welcome().execute();
	}

}
