package main.java.ui.cmd;

import java.util.List;

import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd025SelectChild extends Menu{

	private List<Person> persons;

	public Cmd025SelectChild(List<Person> persons) {
		this.persons = persons;
	}

	@Override
	public Object execute() {
		Person child = this.selectPersonFromList(persons);
		restartIfNull(child);
		print("Selected child: " + child);
		return child;
	}

}
