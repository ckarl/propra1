package main.java.ui.cmd;

import java.util.List;

import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd026SelectFather extends Menu {

	private List<Person> persons;

	public Cmd026SelectFather(List<Person> persons) {
		this.persons = persons;
	}

	@Override
	public Object execute() {
		List<Person> males = collectMalesFrom(persons);
		Person father = selectPersonFromList(males);
		restartIfNull(father);
		print("Selected father: " + father);
		return father;
	}

}
