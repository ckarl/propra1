package main.java.ui.cmd;

import java.util.List;

import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd021SetFather extends Menu{
	
	@Override
	public Object execute() {
		List<Person> persons = createLocalCopyOfPersons();
		// TODO Passing to Constructor breaks current Model
		Person child = (Person) new Cmd025SelectChild(persons).execute();
		Person father = (Person) new Cmd026SelectFather(persons).execute();		
		return child.setFather(father);
	}
			
}