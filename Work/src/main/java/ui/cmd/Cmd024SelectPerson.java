package main.java.ui.cmd;

import java.util.List;

import main.java.entities.Person;
import main.java.ui.Menu;

public class Cmd024SelectPerson extends Menu {

	private List<Person> persons;

	public Cmd024SelectPerson(List<Person> persons) {
		this.persons = persons;
	}

	@Override
	public Object execute() {
		Person parent = selectPersonFromList(persons);
		restartIfNull(parent);
		print("Selected parent: " + parent);
		return parent;
	}
}
