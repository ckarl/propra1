package main.java.ui.cmd;

import main.java.entities.Name;
import main.java.ui.Menu;

public class Cmd011AddPersonName extends Menu {

	@Override
	public Object execute() {
		Name name = null;
		print("Enter name:");
		String response = this.getResponseAsString();
		name = new Name(response);
		return name;
	}
	
}
