package main.java.ui.swing;
 
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;

import javax.swing.*;

import main.java.ui.Menu;
 
public class Swing000Welcome extends Menu implements CanGetComponent {

	@Override
	public Object execute() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	Swing000Welcome menu = new Swing000Welcome();
                menu.createAndShowGUI();
            }
        });
        return null;
	}
	
	public void createAndShowGUI() { 


		/*
		 * Load and save family tree
		 * TODO
		 */
		JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        //menu.getAccessibleContext().setAccessibleDescription("Load and save");
        menuBar.add(menu);        
        JMenu menuQuit = new JMenu("Quit");        
        menuBar.add(menuQuit);
        
        /*
         * Add Person
         */
        JButton addPersonButton = new Swing010AddPerson().getComponent();
        
        //Create and set up the window.
        JFrame frame = new JFrame("FamilyTree");
		frame.setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(menuBar);
        frame.getContentPane().add(addPersonButton);
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

	public JButton getComponent() {
		// TODO Auto-generated method stub
		return null;
	}
 
}

