package main.java.ui.swing;

import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;
import main.java.ui.cmd.Cmd011AddPersonName;
import main.java.ui.cmd.Cmd012AddPersonSex;

public class Swing010AddPerson extends Swing000Welcome {

	@Override
	public JButton getComponent() {
		JButton addPersonButton = new JButton("Add Person");
        addPersonButton.addActionListener(new ActionListener() {
        	   @Override
        	   public void actionPerformed(ActionEvent e) {
        		   JPanel addPersonPanel = new JPanel(new GridLayout(0, 1));
        		   
        		   JLabel nameLabel = new JLabel("Name:");
        		   JTextField nameField = new JTextField();
        		   JLabel sexLabel = new JLabel("Sex");
        		   JRadioButton sexButtonMale = new JRadioButton("Male");
        		   sexButtonMale.setSelected(true);
        		   JRadioButton sexButtonFemale = new JRadioButton("Female");
        		   JButton addPersonOK = new JButton("Ok");
        		   addPersonOK.addActionListener(new ActionListener() {
        			   @Override
        			   public void actionPerformed(ActionEvent e) {
        				   Name name =(Name) new Cmd011AddPersonName().execute();
        				   Sex sex = (Sex) new Cmd012AddPersonSex().execute();
        				   Person personToBeAdded = new Person(name, sex);
        				   print(personToBeAdded.toString());
        				   addPerson(personToBeAdded);
        				   addPersonPanel.setVisible(false);
        				   JComponent comp = (JComponent) e.getSource();
        				   Window win = SwingUtilities.getWindowAncestor(comp);
        				   win.dispose();
        			   }
        		   });
        		   addPersonPanel.add(nameLabel);
        		   addPersonPanel.add(nameField);
        		   addPersonPanel.add(new JSeparator(SwingConstants.VERTICAL));
        		   addPersonPanel.add(sexLabel);
        		   addPersonPanel.add(sexButtonMale);
        		   addPersonPanel.add(sexButtonFemale);
        		   addPersonPanel.add(addPersonOK);
        		   
        		   JFrame addPersonFrame = new JFrame("Add Person");
        		   addPersonFrame.getContentPane().add(addPersonPanel);
        		   addPersonFrame.pack();
        		   addPersonFrame.setLocationRelativeTo( null );
        		   addPersonFrame.setVisible( true );
        	   }
        	});
		return addPersonButton;
	}

}
