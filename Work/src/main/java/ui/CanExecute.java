package main.java.ui;

public interface CanExecute {
	
	Object execute();

}
