package main.java.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import main.java.entities.Context;
import main.java.entities.Name;
import main.java.entities.Person;
import main.java.entities.Sex;

public abstract class Menu implements CanExecute{

	private static Context context;
	public static Scanner in;

	public Menu() {
		print("--------------");
		print(this.getClass().getSimpleName());
		print("--------------");
	}
	
	public static void setContext(Context context) {
		Menu.context = context;
		Menu.in = new Scanner(System.in);
	}

	protected boolean print(String message) {
		System.out.println(message);
		return true;
	}
	
	protected int getResponseAsIntOrMinusOne() {
		int response = -1;
		try {
			String responseAsString = getResponseAsString();
			response = Integer.parseInt(responseAsString);
		} catch (Exception e) {
			print(e.getClass().toString() + ". Setting response to: " + response);
		}
		return response;
	}
	
	protected String getResponseAsString() {
		String response = "not set";
		response = in.next();
		return response;
	}
	
	@Override
	public abstract Object execute();

	protected ArrayList<Person> createLocalCopyOfPersons() {
		return new ArrayList<Person>(Menu.context.getPersons());
	}
	
	public List<Person> collectFemalesFrom(List<Person> persons) {
		// TODO all the sex stuff should be handles in Sex class, in case it changes.
		return persons.stream()
	    .filter(person -> person.getSex() == Sex.FEMALE).collect(Collectors.toList());
	}
	
	public List<Person> collectMalesFrom(List<Person> persons) {
		return persons.stream()
			    .filter(person -> person.getSex() == Sex.MALE).collect(Collectors.toList());
	}
	
	public Person selectPersonFromList(List<Person> persons) {
		Person person = null;
		printIndexedPersons(persons);
		int index = this.getResponseAsIntOrMinusOne();
		try {			
			person = persons.get(index);
			persons.remove(index);
		} catch (Exception e) {
			print("No person found at " + index + ". Returning null.");
		}
		return person;
	}
	
	private void printIndexedPersons(List<Person> persons) {
		int i = 0;
		for (Person person : persons) {
			printIndexAndPerson(i, person);
			i++;
		}
	}
	
	protected void printIndexAndPerson(int i, Person person) {
		print(i + ": " + this.nameOf(person));				
	}
	
	private String nameOf(Person person) {
		Name personName = person.getName();
		String firstName = personName.getFirstName();
		String lastName = personName.getLastName();
		return firstName + " " + lastName;
	}
	
	public void restartIfNull(Person person) {
		if(null == person) {
			this.execute();
		}
	}

	public boolean addPerson(Person personToBeAdded) {
		return context.addPerson(personToBeAdded);
	}

}
