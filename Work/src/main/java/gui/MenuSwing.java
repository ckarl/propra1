package main.java.gui;
 
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;

import main.java.ui.Menu;        
 
public class MenuSwing extends Menu {

	@Override
	public Object execute() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	MenuSwing menu = new MenuSwing();
                menu.createAndShowGUI();
            }
        });
        return null;
	}
	
	public void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Familytree");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Add the ubiquitous "Hello World" label.
        JLabel label = new JLabel("Hello World!");
        frame.getContentPane().add(label);
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
        
        setLayout(new BorderLayout());
        JLabel titleLabel = new JLabel("Menu: Welcome");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setBorder(BorderFactory.createEmptyBorder(20,0,20,0));
        
        // Main Panel
        
       JPanel mainPanel = new JPanel();
       
       JPanel p1 = new JPanel();
       JPanel p2 = new JPanel();
       JPanel p3 = new JPanel();
       JPanel p4 = new JPanel();
       JPanel p5 = new JPanel();
       
       mainPanel.setLayout(new GridLayout(2,1));
       mainPanel.add(p1);
       mainPanel.add(p2);
       mainPanel.add(p3);
       mainPanel.add(p4);
       mainPanel.add(p5);
       
       JLabel l1 = new JLabel("Add a Person");
       JTextField tf1 = new JTextField(12);
       p1.add(l1);
       p1.add(tf1);
       
       JLabel l2 = new JLabel("Set father");
       JTextField tf2 = new JTextField(12);
       p2.add(l2);
       p2.add(tf2);
       
       JLabel l3 = new JLabel("Set mother");
       JTextField tf3 = new JTextField(12);
       p3.add(l3);
       p3.add(tf3);
       
       JLabel l4 = new JLabel("Add child");
       JTextField tf4 = new JTextField(12);
       p4.add(l4);
       p4.add(tf4);
       
       JLabel l5 = new JLabel("Print family tree");
       JTextField tf5 = new JTextField(12);
       p5.add(l5);
       p5.add(tf5);
       
       //Buttons
       JPanel buttonPanel = new JPanel();
       JButton okButton = new JButton("OK");
       JButton exitButton = new JButton("Exit");
       JButton restartButton = new JButton("Restart to WelcomeMenue");
       buttonPanel.add(okButton);
       buttonPanel.add(exitButton);
       buttonPanel.add(restartButton);
       
       //Top-Level Layout
       add(titleLabel, BorderLayout.NORTH);
       add(mainPanel, BorderLayout.CENTER);
       add(buttonPanel, BorderLayout.SOUTH);
       frame.pack();
    }

	private void add(JPanel mainPanel, String center) {
		// TODO Auto-generated method stub
		
	}

	private void add(JLabel titleLabel, String north) {
		// TODO Auto-generated method stub
		
	}
	private void setLayout(BorderLayout borderLayout) {
		// TODO Auto-generated method stub
		
	}
 
}

