// Nicht fertige Codes!!!
package generate;

import java.io.FileOutputStream;



import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



public class Generate_PDF {

	public static void main(String[] args) {
		
		try {
			
		//Create File Name
		String file_name = "C:\\generate_pdf\\rechnung.pdf";
		
		//Create Document object
		Document document = new Document();
		
		//Get pdfwriter instance
		PdfWriter.getInstance(document, new FileOutputStream(file_name));
		
		//Open the document
		document.open();
		
		
		
		// Add a content
		Paragraph para = new Paragraph("Das ist eine Rechnung vom Braingain");
		document.add(para);
		
		
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		
		
		//Add Pdf table objects
		PdfPTable table = new PdfPTable(6); //zwischen den Klammern: Anzahl von Spalten
		
		//Create Cell object
		PdfPCell c1 = new PdfPCell(new Phrase("Pos"));
		table.addCell(c1);
		
		c1 = new PdfPCell(new Phrase("Leistung"));
		table.addCell(c1);
		
		c1 = new PdfPCell(new Phrase("Datum"));
		table.addCell(c1);
		
		c1 = new PdfPCell(new Phrase("Einzelpreis"));
		table.addCell(c1);
		
		c1 = new PdfPCell(new Phrase("Anzahl Stunde"));
		table.addCell(c1);
		
		c1 = new PdfPCell(new Phrase("Gesamtpreis"));
		table.addCell(c1);
		table.setHeaderRows(1);
		/*
		table.addCell("1.0");
		table.addCell("1.1");
		table.addCell("1.2");
		table.addCell("2.1");
		table.addCell("2.2");
		table.addCell("2.3");
		table.addCell("3.1");
		table.addCell("3.2");
		table.addCell("3.3");
		table.addCell("4.1");
		table.addCell("4.2");
		table.addCell("4.3");
		*/
		
		document.add(table);
		
		//Add image in pdf
		document.add(Image.getInstance("C:\\generate_pdf\\logo.png"));
		
		
		
		//Close the document
		document.close();
		
		
		System.out.println("finished");
		
	    }  catch (Exception e) {
	    	System.err.println(e);
		
	    }

	}

}
