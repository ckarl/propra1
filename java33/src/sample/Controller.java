package sample;

import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Models.Invoice;


public class Controller implements Initializable
{
    @FXML
    private TextField txtKundeFirstname;

    @FXML
    private TextField txtKundeLastname;

    @FXML
    private ComboBox cboKundeGender;

    @FXML
    private DatePicker dtKundeBorn;

    @FXML
    private TextField txtKundeStreet;

    @FXML
    private TextField txtKundeStreetNr;

    @FXML
    private TextField txtKundeCity;

    @FXML
    private TextField txtKundeZip;

    @FXML
    private TextField txtKundeMail;

    @FXML
    private TextField txtKundeTel;

    @FXML
    private TextField txtKundeIBAN;

    @FXML
    private TextField txtKundeStunden;

    @FXML
    private TableView tableViewKunden;

    @FXML
    private RadioButton rdYes;

    @FXML
    private RadioButton rdNo;

    @FXML
    private TextField txtTutorFirstname;
    @FXML
    private TextField txtTutorLastname;
    @FXML
    private TextField txtTutorStreet;
    @FXML
    private TextField  txtTutorCity;
    @FXML
    private TextField    txtTutorMail;
    @FXML
    private TextField  txtTutorStreetNr;
    @FXML
    private TextField   txtTutorZip;
    @FXML
    private  TextField txtTutorTel;
    @FXML
    private  TextField txtTutorNiveau;
    @FXML
    private  TextField txtTutorStundenLohn ;
    @FXML
    private  TextField txtTutorIBAN ;
    @FXML
    private ComboBox cboTutorFach ;
    @FXML
    private ComboBox cboSchulNiveau ;
    @FXML
    private ComboBox  cboTutorGender;
    @FXML
    private TableView tableViewTutor ;
    @FXML
    private DatePicker  dtTutorBorn;

    @FXML
    private TextField txtTutorPassword;
    
    //Update: 02.06.2021
    @FXML
    private ComboBox cboKundeRechnung;
    @FXML
    private ComboBox cboMonatRechnung;
    @FXML
    private ComboBox cboJahrRechnung;
    
    //Update 02.06.2021: Alles f�r das bearbeiten der Preise
    
    @FXML
    private RadioButton rdNeuesFach;
    @FXML
    private RadioButton rdUpdatePreis;
    @FXML
    private ComboBox cboUpdateFach;
    @FXML
    private ComboBox cboUpdateNiveau; //hie� vorher cboUpdatePreis oder so. Evnetuell Fehlerhaft
    @FXML
    private TextField txtNameNeuesFach;
    @FXML
    private TextField txtNiveauNeuesFach;
    @FXML
    private TextField txtPreisNeuesFach;
    @FXML
    private TextField txtPreisUpdate;
    
  //Update 05.06.2021: 
    @FXML
    private TableView tableViewUnterrichtsfach;
    
    @FXML
	private BarChart<?, ?> salaryChart; // Update 6.6.21 f�r die Statistik
	@FXML
	private CategoryAxis x;
	@FXML
	private NumberAxis y;
	
	//Update 08.06.21: extra PDF f�r das Amt
	
	@FXML
	private TextField txtAdressat;
	@FXML
	private TextField txtStrasseHausnr;
	@FXML
	private TextField txtPlzOrt;
	@FXML
	private TextArea txtAreaFreitext;
	@FXML
	private RadioButton rdStaatlichYes;
	
    public void ButtonSaveClick(ActionEvent action) throws SQLException
    {
        if (helperFunctions.StringIsEmpty(txtKundeFirstname.getText()) || helperFunctions.StringIsEmpty(txtKundeLastname.getText()) || helperFunctions.StringIsEmpty(txtKundeStreet.getText())
           || helperFunctions.StringIsEmpty(txtKundeStreetNr.getText()) || helperFunctions.StringIsEmpty(txtKundeCity.getText()) || helperFunctions.StringIsEmpty(txtKundeZip.getText())
           || helperFunctions.StringIsEmpty(txtKundeMail.getText()) || helperFunctions.StringIsEmpty(txtKundeTel.getText()) || helperFunctions.StringIsEmpty(txtKundeIBAN.getText())
           || cboKundeGender.getValue() == null || dtKundeBorn.getValue() == null || (rdYes.isSelected() && helperFunctions.StringIsEmpty(txtKundeStunden.getText())))
           	
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte füllen Sie zuerst alle Felder aus!", ButtonType.OK);
            alert.showAndWait();
            return;
        }

        long PLZ = -1;

        try
        {
            PLZ = Long.parseLong(txtKundeZip.getText());
        } catch (Exception e) {}

        if (PLZ == -1)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Die Postleitzahl muss numerisch sein!", ButtonType.OK);
            alert.showAndWait();
            return;
        }
        
        int addressID;
        
        if(Database_Helper.doesAdressExist(Integer.parseInt(txtKundeZip.getText()), txtKundeCity.getText(), txtKundeStreet.getText(), txtKundeStreetNr.getText()) == true) {
        	
        	addressID = Database_Helper.getAddressID(Integer.parseInt(txtKundeZip.getText()), txtKundeCity.getText(), txtKundeStreet.getText(), txtKundeStreetNr.getText());
        }
        else {
        	Database_Helper.addAdresse(Integer.parseInt(txtKundeZip.getText()), txtKundeCity.getText(), txtKundeStreet.getText(), txtKundeStreetNr.getText());
        	addressID = Database_Helper.getAddressID(Integer.parseInt(txtKundeZip.getText()), txtKundeCity.getText(), txtKundeStreet.getText(), txtKundeStreetNr.getText());
        }
        
        
        //int addressID = Database_Helper.getAddressID(Integer.parseInt(txtKundeZip.getText()), txtKundeCity.getText(), txtKundeStreet.getText(), txtKundeStreetNr.getText());

        //if (addressID == -1)
          //  addressID = Database_Helper.addAdresse(Integer.parseInt(txtKundeZip.getText()), txtKundeCity.getText(), txtKundeStreet.getText(), txtKundeStreetNr.getText());

        int stundenAnz = 0;

        if (rdYes.isSelected())
        {
            try
            {
                stundenAnz = Integer.parseInt(txtKundeStunden.getText());
            } catch (Exception e)
            {
                stundenAnz = -1;
            }

            if (stundenAnz <= 0)
            {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Die Stunden darf nur positive Zahl sein!", ButtonType.OK);
                alert.showAndWait();
                return;
            }
            else if (stundenAnz > 35)
            {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Die Stunden darf maximal 35 sein!", ButtonType.OK);
                alert.showAndWait();
                return;
            }
        }

        String actionResult = Database_Helper.AddKunde(txtKundeFirstname.getText(), txtKundeLastname.getText(), txtKundeMail.getText(),
                txtKundeTel.getText(), txtKundeIBAN.getText(), addressID, 0, stundenAnz);

        Alert alert = new Alert(Alert.AlertType.INFORMATION, actionResult, ButtonType.OK);
        alert.showAndWait();

       //Update 04.06.21: ausgeklammert: refreshData();
        refreshDataKunde();
    }

    public void ButtonDeleteClick(ActionEvent action) throws SQLException
    {
        if (tableViewKunden.getItems() == null || tableViewKunden.getItems().size() == 0)
            return;

        ObservableList<String> selectedItem = (ObservableList<String>) tableViewKunden.getSelectionModel().getSelectedItem();

        if (selectedItem == null)
            return;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Möchten Sie den/die Kunden/in wirklich löschen?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() != ButtonType.YES)
            return;

        int KundeID = Integer.parseInt(selectedItem.get(0));

        Database_Helper.DeleteKunde(KundeID);

        alert = new Alert(Alert.AlertType.INFORMATION, "Kunde wurde erfolgreich gelöscht.", ButtonType.OK);
        alert.showAndWait();
       // Update 04.06.21: auskommentiert: refreshData();
        refreshDataKunde();
        //tableViewKunden.getItems().remove(selectedItem);
    }

    public void RadioButtonChanged(ActionEvent action) throws SQLException
    {
        if (rdYes.isSelected())
        {
            txtKundeStunden.setDisable(false);
            txtKundeStunden.setEditable(true);
        }
        else
            txtKundeStunden.setDisable(true);
    }
    public void RadioButtonChangedPreis(ActionEvent action) throws SQLException
    {
   
        if (rdNeuesFach.isSelected())
        {
        	
        	txtNameNeuesFach.setDisable(false); //l�uft
            txtNameNeuesFach.setEditable(true); //l�uft
            txtNiveauNeuesFach.setDisable(false); //l�uft
            txtNiveauNeuesFach.setEditable(true); //l�uft
            txtPreisNeuesFach.setDisable(false); //l�uft
            txtPreisNeuesFach.setEditable(true); //l�uft
            
            txtPreisUpdate.setDisable(true);
           
        		
    
            
        }
        else if (rdUpdatePreis.isSelected()) {
        	txtNameNeuesFach.setDisable(true); //l�uft
        	txtNiveauNeuesFach.setDisable(true); //l�uft
        	txtPreisNeuesFach.setDisable(true); //l�uft
        
        	txtPreisUpdate.setDisable(false);
        	txtPreisUpdate.setEditable(true);
        }
        	
	
    }
    
    //Update 08.06: 
   
    
    public void RadioButtonStaatlichYes(ActionEvent action) throws SQLException {
    	if(rdStaatlichYes.isSelected()) {
    		
    		txtAdressat.setDisable(false); //l�uft
            txtAdressat.setEditable(true); //l�uft
            txtStrasseHausnr.setDisable(false);
            txtStrasseHausnr.setEditable(true);
            txtPlzOrt.setDisable(false); //l�uft
            txtPlzOrt.setEditable(true); //l�uft
            txtAreaFreitext.setDisable(false); //l�uft
            txtAreaFreitext.setEditable(true); //l�uft
    		
    	}
    	else if (!rdStaatlichYes.isSelected()) {
    		txtAdressat.setDisable(true); //l�uft
            txtAdressat.setEditable(false); //l�uft
            txtStrasseHausnr.setDisable(true);
            txtStrasseHausnr.setEditable(false);
            txtPlzOrt.setDisable(true); //l�uft
            txtPlzOrt.setEditable(false); //l�uft
            txtAreaFreitext.setDisable(true); //l�uft
            txtAreaFreitext.setEditable(false); //l�uft
    		
    	}
    }

    public void ButtonSaveClickTutor(ActionEvent action) throws SQLException
    {
        if (helperFunctions.StringIsEmpty(txtTutorFirstname.getText())
                || helperFunctions.StringIsEmpty(txtTutorLastname.getText())
                || helperFunctions.StringIsEmpty(txtTutorStreet.getText())
                || helperFunctions.StringIsEmpty(txtTutorStreetNr.getText())
                || helperFunctions.StringIsEmpty(txtTutorCity.getText())
                || helperFunctions.StringIsEmpty(txtTutorZip.getText())
                || helperFunctions.StringIsEmpty(txtTutorIBAN.getText())
                || helperFunctions.StringIsEmpty(txtTutorMail.getText())
                || helperFunctions.StringIsEmpty(txtTutorTel.getText())
                || helperFunctions.StringIsEmpty(txtTutorPassword.getText())
                || cboTutorGender.getValue() == null
                || dtTutorBorn.getValue() == null )
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte füllen Sie zuerst alle Felder aus!", ButtonType.OK);
            alert.showAndWait();
            return;
        }

        long PLZ = -1;

        try
        {
            PLZ = Long.parseLong(txtTutorZip.getText());
        } catch (NumberFormatException e) {}

        if (PLZ == -1)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Die Postleitzahl muss numerisch sein!", ButtonType.OK);
            alert.showAndWait();
            return;
        }
        
        //Update 06.06: Bug Reperatur
        int addressID;
        
        if(Database_Helper.doesAdressExist(Integer.parseInt(txtTutorZip.getText()), txtTutorCity.getText(), txtTutorStreet.getText(), txtTutorStreetNr.getText()) == true) {
        	
        	addressID = Database_Helper.getAddressID(Integer.parseInt(txtTutorZip.getText()), txtTutorCity.getText(), txtTutorStreet.getText(), txtTutorStreetNr.getText());
        }
        else {
        	Database_Helper.addAdresse(Integer.parseInt(txtTutorZip.getText()), txtTutorCity.getText(), txtTutorStreet.getText(), txtTutorStreetNr.getText());
        	addressID = Database_Helper.getAddressID(Integer.parseInt(txtTutorZip.getText()), txtTutorCity.getText(), txtTutorStreet.getText(), txtTutorStreetNr.getText());
        }
       
        /*
        int addressID = Database_Helper.getAddressID(Integer.parseInt(txtTutorZip.getText()), txtTutorCity.getText(), txtTutorStreet.getText(), txtTutorStreetNr.getText());

        if (addressID == -1)
            addressID = Database_Helper.addAdresse(Integer.parseInt(txtTutorZip.getText()), txtTutorCity.getText(), txtTutorStreet.getText(), txtTutorStreetNr.getText());
         */
        int stundenAnz = 0;


        try
        {
            stundenAnz = Integer.parseInt(txtTutorStundenLohn.getText());
        } catch (Exception e)
        {
            stundenAnz = -1;
        }

        if (stundenAnz == -1)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Die Stunden darf nur eine Nummer sein!", ButtonType.OK);
            alert.showAndWait();
            return;
        }

        //(String firstname, String lastname, String email, String tel, String bankaccount,String paymentperhoure, int adresse_id, String password) throws SQLException
        //Update 08.06.21: String actionResult ...
        //String actionResult = Database_Helper.AddTutor(txtTutorFirstname.getText(), txtTutorLastname.getText(),
          //      txtTutorMail.getText(), txtTutorTel.getText(),txtTutorIBAN.getText(), Integer.parseInt(txtTutorStundenLohn.getText()),
            //    addressID, txtTutorPassword.getText());
        
        Database_Helper.AddTutor(txtTutorFirstname.getText(), txtTutorLastname.getText(),
                txtTutorMail.getText(), txtTutorTel.getText(),txtTutorIBAN.getText(), Integer.parseInt(txtTutorStundenLohn.getText()),
                  addressID, txtTutorPassword.getText());

        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Einf�gen erfolgreich", ButtonType.OK);
        alert.showAndWait();

        refreshDataTutor();
    }


    public void ButtonDeleteClickTutor(ActionEvent action) throws SQLException
    {
        if (tableViewTutor.getItems() == null || tableViewTutor.getItems().size() == 0)
            return;

        ObservableList<String> selectedItem = (ObservableList<String>) tableViewTutor.getSelectionModel().getSelectedItem();

        if (selectedItem == null)
            return;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Möchten Sie den/die Tutor/in wirklich löschen?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() != ButtonType.YES)
            return;

        int TutorID = Integer.parseInt(selectedItem.get(0));

        Database_Helper.DeleteTutor(TutorID);

        alert = new Alert(Alert.AlertType.INFORMATION, "Tutor wurde erfolgreich gelöscht.", ButtonType.OK);
        alert.showAndWait();
        refreshDataTutor();
        //tableViewTutor.getItems().remove(selectedItem);
    }
    
    //Update 02.06.2021
    public void buttonSavePreis(ActionEvent action) throws SQLException {
    	
    	if (!rdNeuesFach.isSelected() && !rdUpdatePreis.isSelected() )
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Fehlerhafte Eingabe. Bitte eine der Optionen w�hlen.", ButtonType.OK);
            alert.showAndWait();
            return;
        }
    	
    	if(rdNeuesFach.isSelected()) {
    		
    		if (helperFunctions.StringIsEmpty(txtNameNeuesFach.getText())
            || helperFunctions.StringIsEmpty(txtNiveauNeuesFach.getText())
            || helperFunctions.StringIsEmpty(txtPreisNeuesFach.getText())
            )
    {
        Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte f�llen Sie zuerst alle Felder aus!", ButtonType.OK);
        alert.showAndWait();
        return;
    }
    		try {
    			String fach_name = txtNameNeuesFach.getText();
        		String fach_niveau = txtNiveauNeuesFach.getText();
        		String fach_preis_String = txtPreisNeuesFach.getText();
        	
        		double fach_preis = Double.parseDouble(fach_preis_String);
        		Database_Helper.addNeuesFach(fach_name, fach_niveau, fach_preis);
        		Alert alertDone = new Alert(Alert.AlertType.INFORMATION, "Das neue Fach wurde erfolgreich angelegt.", ButtonType.OK);
                alertDone.showAndWait();
                return;
                
        		
    			
    		}
                 catch (NumberFormatException exception) {
                
                System.out.println(exception);
                Alert alert = new Alert(Alert.AlertType.WARNING, "Fehlerhafte Eingabe. Bitte passen Sie den Preis an.", ButtonType.OK);
                alert.showAndWait();
                return;
  
                 } 
    	}
    	else if(rdUpdatePreis.isSelected()) {
    		
    		String fach_name = cboUpdateFach.getValue().toString();
    		String fach_niveau = cboUpdateNiveau.getValue().toString();
    		String fach_preis_String = txtPreisUpdate.getText();
    		double fach_preis = Double.parseDouble(fach_preis_String);
    		Database_Helper.updateFachPreis(fach_name, fach_niveau, fach_preis);
    		Alert alertDone = new Alert(Alert.AlertType.INFORMATION, "Der Preis wurde aktualisiert.", ButtonType.OK);
            alertDone.showAndWait();
            return;
    		
    	}
    	
    	else {
    	
    	Alert alertDefault = new Alert(Alert.AlertType.WARNING, "�berpr�fen sie Ihre Eingabe!", ButtonType.OK);
        alertDefault.showAndWait();
        return;
        }
    }

    private void refreshDataTutor()
    {
        tableViewTutor.getColumns().clear();
        tableViewTutor.getItems().clear();
        
        //Update 04.06.2021: andere Methode genutzt wegen optimierter Tabelle
        //hier ist dann ein Fehler aufgetreten. Deswegen die alte Methode
        var tutoren = Database_Helper.buildDataFromTable("tutor");

        for (int i = 0; i < tutoren.columnList.size(); i++)
            tableViewTutor.getColumns().addAll(tutoren.columnList.get(i));

        tableViewTutor.setItems(tutoren.data);
    }
    
    //Update: 02.06.2021
    public void ButtonGeneratePDF(ActionEvent action) throws SQLException {
    	
    	//Update 08.06.2021: erste Seite hinzugef�gt
    	if (rdStaatlichYes.isSelected() && txtAdressat.getText()== null || txtStrasseHausnr.getText()==null 
    			|| txtPlzOrt.getText()==null
    			|| txtAreaFreitext.getText()==null) {
    		Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte f�llen Sie zuerst alle Felder aus!", ButtonType.OK);
            alert.showAndWait();
            return;
    	} 
    	if (rdStaatlichYes.isSelected()) {
    		String[] splitParts = cboKundeRechnung.getValue().toString().split(" ");
            String vorname = splitParts[0];
            String nachname = splitParts[1];
            int kundeID = Database_Helper.getKundeID(vorname, nachname);
            
            int monat = (int) cboMonatRechnung.getValue();
            int jahr = (int) cboJahrRechnung.getValue();
            String adressat = txtAdressat.getText();
            String strasseundhausnr = txtStrasseHausnr.getText();
            String plzort = txtPlzOrt.getText();
            String freitext = txtAreaFreitext.getText();
            
            Database_Helper.addInvoiceForCustomer(kundeID, monat, jahr);
        	
        	Invoice invoice = new Invoice();
        	invoice.createPDFforStaatlich(adressat, strasseundhausnr, plzort, freitext, kundeID, monat, jahr);
        	System.out.println("PDF Created");
        	
        	//Update 02.06.2021
        	Alert alert = new Alert(Alert.AlertType.INFORMATION, "Die Rechnung wurde erfolgreich erstellt", ButtonType.OK);
            alert.showAndWait();
    		
    	}
    	
    	//Update 03.06.2021: Fehlermeldung eingef�gt
    	
    	if (cboKundeRechnung.getValue()== null || cboMonatRechnung.getValue()==null 
    			|| cboJahrRechnung.getValue()==null) {
    		Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte f�llen Sie zuerst alle Felder aus!", ButtonType.OK);
            alert.showAndWait();
            return;
    	}
    	
    	if(!rdStaatlichYes.isSelected()) {
    	String[] splitParts = cboKundeRechnung.getValue().toString().split(" ");
        String vorname = splitParts[0];
        String nachname = splitParts[1];
        int kundeID = Database_Helper.getKundeID(vorname, nachname);
        
        int monat = (int) cboMonatRechnung.getValue();
        int jahr = (int) cboJahrRechnung.getValue();
        
        Database_Helper.addInvoiceForCustomer(kundeID, monat, jahr);
    	
    	Invoice invoice = new Invoice();
    	invoice.createPDF(kundeID, monat, jahr);
    	System.out.println("PDF Created");
    	
    	//Update 02.06.2021
    	Alert alert = new Alert(Alert.AlertType.INFORMATION, "Die Rechnung wurde erfolgreich erstellt", ButtonType.OK);
        alert.showAndWait();
    	}
    }
    
    //Update 04.06.2021: hier liegt ein Fehler vor. Eventuell weil vorher die Connection und das rs nicht geclosedd wurde
    private void refreshData() {
        tableViewKunden.getColumns().clear();
        tableViewKunden.getItems().clear();

        var kunden = Database_Helper.buildDataFromTable("tutor");

        for (int i = 0; i < kunden.columnList.size(); i++)
            tableViewKunden.getColumns().addAll(kunden.columnList.get(i));

        tableViewKunden.setItems(kunden.data);

    }
    
    //Update 04.06.2021: Die Tabelle auf dem Startfenster soll anders aussehen
    private void refreshDataKunde() {
        tableViewKunden.getColumns().clear();
        tableViewKunden.getItems().clear();

        var kunden = Database_Helper.buildDataFromTableForKunden();

        for (int i = 0; i < kunden.columnList.size(); i++)
            tableViewKunden.getColumns().addAll(kunden.columnList.get(i));

        tableViewKunden.setItems(kunden.data);

    }
    
    //Update 06.06.2021 Cedric
    public void ButtonRefresh(ActionEvent action) {
    	refreshDataUnterrichtsfach();
    }
    
  //Update 06.06.2021: Update Cedric; Die Tabelle mit der Preisliste soll so aussehen
    private void refreshDataUnterrichtsfach() {
        tableViewUnterrichtsfach.getColumns().clear();
        tableViewUnterrichtsfach.getItems().clear();

        var unterrichtsfach = Database_Helper.buildDataFromTableForUnterrichtsfach();

        for (int i = 0; i < unterrichtsfach.columnList.size(); i++)
            tableViewUnterrichtsfach.getColumns().addAll(unterrichtsfach.columnList.get(i));

        tableViewUnterrichtsfach.setItems(unterrichtsfach.data);

    }
    
    
    public void viewerStatistik() throws SQLException { 
		salaryChart.getData().clear();
		Database_Helper db = new Database_Helper();
		
		XYChart.Series set1 = new XYChart.Series<>();
		set1.setName("Einnahmen");
		XYChart.Series set2 = new XYChart.Series<>();
		set2.setName("Ausgaben");
		XYChart.Series set3 = new XYChart.Series<>();
		set3.setName("Gewinn");
		set1.getData().add(new XYChart.Data<>("Januar", db.getIncome(0)));
		set1.getData().add(new XYChart.Data<>("Februar", db.getIncome(1)));
		set1.getData().add(new XYChart.Data<>("M�rz", db.getIncome(2)));
		set1.getData().add(new XYChart.Data<>("April", db.getIncome(3)));
		set1.getData().add(new XYChart.Data<>("Mai", db.getIncome(4)));
		set1.getData().add(new XYChart.Data<>("Juni", db.getIncome(5)));
		set1.getData().add(new XYChart.Data<>("Juli", db.getIncome(6)));
		set1.getData().add(new XYChart.Data<>("August", db.getIncome(7)));
		set1.getData().add(new XYChart.Data<>("September", db.getIncome(8)));
		set1.getData().add(new XYChart.Data<>("Oktober", db.getIncome(9)));
		set1.getData().add(new XYChart.Data<>("November", db.getIncome(10)));
		set1.getData().add(new XYChart.Data<>("Dezember", db.getIncome(11)));
		
		set2.getData().add(new XYChart.Data<>("Januar", db.getFee(0)));
		set2.getData().add(new XYChart.Data<>("Februar", db.getFee(1)));
		set2.getData().add(new XYChart.Data<>("M�rz", db.getFee(2)));
		set2.getData().add(new XYChart.Data<>("April", db.getFee(3)));
		set2.getData().add(new XYChart.Data<>("Mai", db.getFee(4)));
		set2.getData().add(new XYChart.Data<>("Juni", db.getFee(5)));
		set2.getData().add(new XYChart.Data<>("Juli", db.getFee(6)));
		set2.getData().add(new XYChart.Data<>("August", db.getFee(7)));
		set2.getData().add(new XYChart.Data<>("September", db.getFee(8)));
		set2.getData().add(new XYChart.Data<>("Oktober", db.getFee(9)));
		set2.getData().add(new XYChart.Data<>("November", db.getFee(10)));
		set2.getData().add(new XYChart.Data<>("Dezember", db.getFee(11)));
		
		set3.getData().add(new XYChart.Data<>("Januar", db.getProfit(0)));
		set3.getData().add(new XYChart.Data<>("Februar", db.getProfit(1)));
		set3.getData().add(new XYChart.Data<>("M�rz", db.getProfit(2)));
		set3.getData().add(new XYChart.Data<>("April", db.getProfit(3)));
		set3.getData().add(new XYChart.Data<>("Mai", db.getProfit(4)));
		set3.getData().add(new XYChart.Data<>("Juni", db.getProfit(5)));
		set3.getData().add(new XYChart.Data<>("Juli", db.getProfit(6)));
		set3.getData().add(new XYChart.Data<>("August", db.getProfit(7)));
		set3.getData().add(new XYChart.Data<>("September", db.getProfit(8)));
		set3.getData().add(new XYChart.Data<>("Oktober", db.getProfit(9)));
		set3.getData().add(new XYChart.Data<>("November", db.getProfit(10)));
		set3.getData().add(new XYChart.Data<>("Dezember", db.getProfit(11)));
		
		salaryChart.getData().addAll(set1,set2,set3);
		
		
	}




    public void initialize(URL url, ResourceBundle resourceBundle) 
    {
    	
   
        cboKundeGender.getItems().removeAll(cboKundeGender.getItems());
        cboKundeGender.getItems().addAll("M", "W");
        cboKundeGender.getSelectionModel().select("M");
        rdNo.setSelected(true);
        txtKundeStunden.setDisable(true);
        //Update 01.06
        cboTutorGender.getItems().removeAll(cboTutorGender.getItems());
        cboTutorGender.getItems().addAll("M", "W");
        cboTutorGender.getSelectionModel().select("M");
       //Update Ende
        
        refreshDataTutor();
       // Update 04.06.21: ausgeklammert -> refreshData();
        refreshDataKunde();
       //Update 06.06.21 Cedric 
        refreshDataUnterrichtsfach();
        //UPDATE David am 01.06 um 16:05 
        ArrayList<String> allStudents = null;
        allStudents = Database_Helper.GetAllStudents();

        for (int i = 0; i < allStudents.size(); i++)
            cboKundeRechnung.getItems().add(allStudents.get(i));
        
   
        
    	for (int i = 1; i < 13; i++) {				//alle Monate einf�gen von  1 bis 12
            cboMonatRechnung.getItems().add(i);
    	}
    	
    	ArrayList<Integer> jahre = Database_Helper.generateListOfYears();
    	for (int i = 0;i<jahre.size();i++) {
    		String dummy = jahre.get(i).toString();
    		int jahreszahl = Integer.parseInt(dummy);
    		cboJahrRechnung.getItems().add(jahreszahl);
    	}
    		
    	//Update 03.06.2021: ComboBoxen f�r F�cher ausgef�llt
    		ArrayList<String> fachNamen = Database_Helper.generateListOfFachName();
    		for(int i = 0; i<fachNamen.size();i++) {
    			cboUpdateFach.getItems().add(fachNamen.get(i).toString());
    		}
    		ArrayList<String> fachNiveau = Database_Helper.generateListOfNiveau();
    		for(int i = 0; i<fachNiveau.size();i++) {
    			cboUpdateNiveau.getItems().add(fachNiveau.get(i).toString());
    		}
    		
    		
    		
    		// Statistik initialisieren 

    		
     		XYChart.Series set1 = new XYChart.Series<>();
     		set1.setName("Einnahmen");
     		XYChart.Series set2 = new XYChart.Series<>();
     		set2.setName("Ausgaben");
     		XYChart.Series set3 = new XYChart.Series<>();
     		set3.setName("Gewinn");
     		
     		set1.getData().add(new XYChart.Data<>("Januar",0));
     		set1.getData().add(new XYChart.Data<>("Februar",0));
     		set1.getData().add(new XYChart.Data<>("M�rz", 0));
     		set1.getData().add(new XYChart.Data<>("April", 0));
     		set1.getData().add(new XYChart.Data<>("Mai", 0));
     		set1.getData().add(new XYChart.Data<>("Juni", 0));
     		set1.getData().add(new XYChart.Data<>("Juli", 0));
     		set1.getData().add(new XYChart.Data<>("August", 0));
     		set1.getData().add(new XYChart.Data<>("September", 0));
     		set1.getData().add(new XYChart.Data<>("Oktober", 0));
     		set1.getData().add(new XYChart.Data<>("November", 0));
     		set1.getData().add(new XYChart.Data<>("Dezember", 0));
     		
     		set2.getData().add(new XYChart.Data<>("Januar",0));
     		set2.getData().add(new XYChart.Data<>("Februar",0));
     		set2.getData().add(new XYChart.Data<>("M�rz", 0));
     		set2.getData().add(new XYChart.Data<>("April", 0));
     		set2.getData().add(new XYChart.Data<>("Mai", 0));
     		set2.getData().add(new XYChart.Data<>("Juni", 0));
     		set2.getData().add(new XYChart.Data<>("Juli", 0));
     		set2.getData().add(new XYChart.Data<>("August", 0));
     		set2.getData().add(new XYChart.Data<>("September", 0));
     		set2.getData().add(new XYChart.Data<>("Oktober", 0));
     		set2.getData().add(new XYChart.Data<>("November", 0));
     		set2.getData().add(new XYChart.Data<>("Dezember", 0));
     		
     		set3.getData().add(new XYChart.Data<>("Januar",0));
     		set3.getData().add(new XYChart.Data<>("Februar",0));
     		set3.getData().add(new XYChart.Data<>("M�rz", 0));
     		set3.getData().add(new XYChart.Data<>("April", 0));
     		set3.getData().add(new XYChart.Data<>("Mai", 0));
     		set3.getData().add(new XYChart.Data<>("Juni", 0));
     		set3.getData().add(new XYChart.Data<>("Juli", 0));
     		set3.getData().add(new XYChart.Data<>("August", 0));
     		set3.getData().add(new XYChart.Data<>("September", 0));
     		set3.getData().add(new XYChart.Data<>("Oktober", 0));
     		set3.getData().add(new XYChart.Data<>("November", 0));
     		set3.getData().add(new XYChart.Data<>("Dezember", 0));
     		salaryChart.getData().addAll(set1,set2,set3);
     		
     		try {
				viewerStatistik();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        
    }
    
    
 
    

}