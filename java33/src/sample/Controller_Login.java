package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.sql.SQLException;

public class Controller_Login {
    @FXML
    private TextField txtUserMail;

    @FXML
    private PasswordField pwfPasswort;	//Update 08.06

    @FXML
    private Button btnLogin;

    public void ButtonCloseProgram(ActionEvent action) throws SQLException {
        Platform.exit();
        System.exit(0);
    }

    public void ButtonTryLogin(ActionEvent action) throws SQLException, IOException {
        if (helperFunctions.StringIsEmpty(pwfPasswort.getText())
                || helperFunctions.StringIsEmpty(txtUserMail.getText())) {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte geben Sie zuerst die Mail und Passwort ein!", ButtonType.OK);
            alert.showAndWait();
            return;
        }


        Database_Helper.LoginHelperClass validationResult = Database_Helper.LoginValid(txtUserMail.getText(), pwfPasswort.getText());

        if (validationResult.LoginIsValid == false)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Es exisitiert kein User mit Ihren Eingaben! Versuchen Sie es bitte erneut.", ButtonType.OK);
            alert.showAndWait();
        }
        else
            {
                String secondWindowName = "";

                if(validationResult.AdminLogin)
                {
                    secondWindowName = "MainWindow.fxml";
                }
                else
                    {
                        secondWindowName = "TutorLogin.fxml";
                    }

                Stage stage = (Stage) btnLogin.getScene().getWindow();
                stage.close();

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(secondWindowName));
                Parent root1 = (Parent) fxmlLoader.load();
                stage = new Stage();
                //stage.initModality(Modality.APPLICATION_MODAL);
                //stage.initStyle(StageStyle.UNDECORATED);
                stage.setTitle("Programm");
                stage.setScene(new Scene(root1));
                stage.setResizable(false);
                stage.show();

        }
    }
}
