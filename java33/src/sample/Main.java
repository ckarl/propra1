package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application
{

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("LoginWindow.fxml"));
        primaryStage.setTitle("Anmeldung");
        primaryStage.setScene(new Scene(root, 410, 200));
        primaryStage.setResizable(false);
        primaryStage.show();

        /*root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        primaryStage.setTitle("Programm");
        var newScene = new Scene(root, 555, 640);
        primaryStage.setScene(newScene);
        primaryStage.setResizable(false);
        primaryStage.show();*/
    }

/*     try {
         FXMLLoader loader = new FXMLLoader(getClass().getResource(
               "PersonEditor.fxml"));
         Parent root = (Parent) loader.load();
         PersonEditorCtrl ctrl = loader.getController();
         ctrl.init(table.getSelectionModel().getSelectedItem());

         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }*/

    public static void main(String[] args)
    {
        launch(args);
    }
}
