package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Models.FeeItem;

public class Controller_TutorLogin implements Initializable
{
    @FXML
    private TextField txtStunde;

    @FXML
    private ComboBox cboFach;

    @FXML
    private ComboBox cboStudents;

    @FXML
    private DatePicker dtUnterricht;
    
    @FXML
    //Update 04.06.2021
    private TableView tableViewKundenForTutor;
    
    //Update 05.06.2021: Honorar
    
    @FXML
    private ComboBox cboMonatHonorar;
    @FXML
    private ComboBox cboJahrHonorar;
    

    public void ButtonCancel(ActionEvent action) throws SQLException {
        Platform.exit();
        System.exit(0);
    }

    public void ButtonSaveClick(ActionEvent action) throws SQLException
    {
        if (helperFunctions.StringIsEmpty(txtStunde.getText())
                || cboFach.getValue() == null
                || cboStudents.getValue() == null
                || dtUnterricht.getValue() == null   )
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte füllen Sie zuerst alle Felder aus!", ButtonType.OK);
            alert.showAndWait();
            return;
        }

        //int stunden = -1;
        double stunden = -1;

        try
        {
          //  stunden = Integer.parseInt(txtStunde.getText());
        	 stunden = Double.parseDouble(txtStunde.getText());
        } catch (Exception e)
        {
            stunden = -1;
        }

        if (stunden <= 0)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING, "Fehler bei Eingabe der Stundenzahl.", ButtonType.OK);
            alert.showAndWait();
            return;
        }

        String[] splitParts = cboStudents.getValue().toString().split(" ");
        String vorname = splitParts[0];
        String nachname = splitParts[1];

        int kundeID = Database_Helper.getKundeID(vorname, nachname);

        splitParts  = cboFach.getValue().toString().split(" ");
        String fach = splitParts[0];
        String niveau = "";

        if (splitParts.length == 2)
            niveau = splitParts[1];
        else
            {
             niveau = splitParts[1] + " " + splitParts[2];
            }

        int fachID = Database_Helper.getFachID(fach, niveau);

        Database_Helper.AddTutorUnterrichtEinheit(kundeID, Database_Helper.Login_ID, fachID, stunden, Date.valueOf(dtUnterricht.getValue()));
        //Update 05.06.2021: Aktualisierung der Stunden f�r einen Kunden
        Database_Helper.updateHours(kundeID);
        
        if(Database_Helper.getLimitFromKunde(kundeID) > 0.00) {
        	double difference = Database_Helper.getLimitFromKunde(kundeID) - Database_Helper.getStundenAktuellFromKunde(kundeID);
        	if (difference < 0) {
        		difference = difference * -1;
        		var alert = new Alert(Alert.AlertType.INFORMATION, "Achtung! Der Sch�ler hat sein Limit �berschritten. Das Limit wurde um so viele Stunden �berschritten: "+difference, ButtonType.OK);
                alert.showAndWait();
                clearInput();
        	}
        	else {
        		var alert = new Alert(Alert.AlertType.INFORMATION, "Eingabe erfolgreich. �brige Stunden bis Limit erreicht worden ist: " +difference, ButtonType.OK);
                alert.showAndWait();
                clearInput();
        	}
        }
        else {
        var alert = new Alert(Alert.AlertType.INFORMATION, "Ihre Daten wurden erfolgreich gespeichert! Der Sch�ler hat kein Stundenlimit.", ButtonType.OK);
        alert.showAndWait();
        
        clearInput();
        }
    }
    
    public void ButtonRefresh(ActionEvent action) {
    	refreshStudents();
    }
    
    //Update 05.06: Honorare in GUI
    public void ButtonCreatePDFHonorar(ActionEvent action) {
    	if (cboMonatHonorar.getValue()==null 
    			|| cboJahrHonorar.getValue()==null) {
    		Alert alert = new Alert(Alert.AlertType.WARNING, "Bitte f�llen Sie zuerst alle Felder aus!", ButtonType.OK);
            alert.showAndWait();
            return;
    	}
    	 int tutorID = Database_Helper.Login_ID;
         
         FeeItem honorar = new FeeItem();
         
    	if (cboMonatHonorar.getValue().toString().equals("Jahres�bersicht")) {
    		int jahr = (int) cboJahrHonorar.getValue();
    		 honorar.createPDFforYearFeeItem(tutorID, jahr);
    		 Alert alert = new Alert(Alert.AlertType.INFORMATION, "Jahres�bersicht wurde generiert.", ButtonType.OK);
    	        alert.showAndWait();
    	}
    	else {
    		int monat = (int) cboMonatHonorar.getValue();
            int jahr = (int) cboJahrHonorar.getValue();
    	 honorar.createPDFforFeeItem(tutorID, monat, jahr);
    	 Alert alert = new Alert(Alert.AlertType.INFORMATION, "Deine Honorar�bersicht f�r den Monat wurde erstellt.", ButtonType.OK);
         alert.showAndWait();
    	}
    	
    	
    }

    private void clearInput()
    {
        txtStunde.clear();
        cboFach.getSelectionModel().clearSelection();
        cboStudents.getSelectionModel().clearSelection();
        dtUnterricht.getEditor().clear();
    }
    
  //Update 04.06.2021: Aktualisierte Sch�ler
    private void refreshStudents() {
        tableViewKundenForTutor.getColumns().clear();
        tableViewKundenForTutor.getItems().clear();
        
        //Update 04.06.2021: Aktualisierte Sch�ler
        var students = Database_Helper.buildDataFromTableInTutor(Database_Helper.Login_ID);//Database_Helper.Login_ID

        for (int i = 0; i < students.columnList.size(); i++)
        	tableViewKundenForTutor.getColumns().addAll(students.columnList.get(i));

        tableViewKundenForTutor.setItems(students.data);

    }
    

    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        ArrayList<String> allStudents = null;
        
            allStudents = Database_Helper.GetAllStudents();
        

        for (int i = 0; i < allStudents.size(); i++)
            cboStudents.getItems().add(allStudents.get(i));

        ArrayList<String> allFächer = null;
        
         allFächer = Database_Helper.GetAllFachNames();
        

        for (int i = 0; i < allFächer.size(); i++)
            cboFach.getItems().add(allFächer.get(i));
        
        //Update 05.06: ComboBoxen aufgef�llt
        cboMonatHonorar.getItems().add("Jahres�bersicht");
        
        for (int i = 1; i < 13; i++) {				//alle Monate einf�gen von  1 bis 12
            cboMonatHonorar.getItems().add(i);
    	}
    	
        
    	ArrayList<Integer> jahre = Database_Helper.generateListOfYears();
    	for (int i = 0;i<jahre.size();i++) {
    		String dummy = jahre.get(i).toString();
    		int jahreszahl = Integer.parseInt(dummy);
    		cboJahrHonorar.getItems().add(jahreszahl);
        
        refreshStudents();

    }
}
}
