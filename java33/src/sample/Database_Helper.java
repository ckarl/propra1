package sample;

import javafx.collections.FXCollections;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Database_Helper
{
	private static String hostname = "remotemysql.com";
    private static String port = "3306";
    private static String dbname = "QAnQ6VscJZ";
    private static String user = "QAnQ6VscJZ";
    private static String password = "Tzk16k43bL";

    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";		//UPDATE: .cj. eingef�gt
    private static Connection connection = null;
   
    private static String url = "jdbc:mysql://" + hostname + ":" + port + "/" + dbname;		//UPDATE: url eingef�gt
    
    private static ObservableList<ObservableList> data;

    static
    {
        if (connection == null)
        {
            try {
                openConnection();
            } catch (SQLException var1) {
                System.err.println("SQL Fehler - " + var1.getMessage());
                var1.printStackTrace();
            } catch (ClassNotFoundException var2) {
                System.err.println("SQL Fehler - " + var2.getMessage());
                var2.printStackTrace();
            }
        }

    }

    private static void openConnection() throws SQLException, ClassNotFoundException
    {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String mySqlUrl = "jdbc:mysql://" + hostname + ":" + port + "/" + dbname;
        connection = DriverManager.getConnection(mySqlUrl, user, password);
    }

    private static int generateRandomIDForTable(String tableName)
    {
        Random rand = new Random();
        int id = -1;
        int existingCounts = -1;

        while(existingCounts != 0) {
            try(Connection connection = DriverManager.getConnection(url, user, password)) {
                id = rand.nextInt(9999);
                String query = "Select Count(*) as test From " + tableName + " Where " + tableName + "_ID = " + id;
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                rs.next();
                existingCounts = rs.getInt("test");
            } catch (SQLException var6) {
                System.err.println("SQL Fehler - " + var6.getMessage());
                var6.printStackTrace();
            }
        }

        return id;
    }

   private static boolean EntryExists(String sql)
    {
        boolean result = false;

        try(Connection connection = DriverManager.getConnection(url, user, password)) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            rs.next();
            result = rs.getInt("test") > 0;
        } catch (SQLException var4) {
            System.err.println("SQL Fehler - " + var4.getMessage());
            var4.printStackTrace();
        }

        return result;
    }
/*
    public static int addAdresse3(int postcode, String town, String street, String housenumber) throws SQLException
    {
        int resultID = generateRandomIDForTable("adresse");
        String query = "INSERT INTO adresse (adresse_id, plz, stadt, strasse, hausnr) values (?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, resultID);
        ps.setInt(2, postcode);
        ps.setString(3, town);
        ps.setString(4, street);
        ps.setString(5, housenumber);
        ps.execute();
        ps.close();
        return resultID;
    }*/
   
    /*
     * UPDATE 06.06: addAdresse ver�ndert
     */
   
   	public static void addAdresse(int postcode, String town, String street, String housenumber) {
   			
   		try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    
    		String checkSQL = "SELECT * FROM adresse WHERE adresse.plz = " + postcode + " AND adresse.stadt= '"+town+ "' AND adresse.strasse= '"+street+"' AND adresse.hausnr = '"+housenumber+"'";;
    		Statement stmt= connection.createStatement();
    		ResultSet rs = stmt.executeQuery(checkSQL);
    		
            if (!rs.next()) {
        
    		String query1 = "INSERT INTO adresse (plz, stadt, strasse, hausnr) values (?, ?, ?, ?)";
    		PreparedStatement ps = connection.prepareStatement(query1);
    		ps.setInt(1, postcode);
            ps.setString(2, town);
            ps.setString(3, street);
            ps.setString(4, housenumber);
  
             // rs.close();
              ps.execute();
              ps.close();
            }
            else {
            	System.out.println("Adresse existiert bereits");
            	
            }
            
            
      	
      		}catch (SQLException ex) {
   		    
      			System.err.println(ex.getMessage());
      			System.err.println("SQL Fehler - " + ex.getMessage());
      			ex.printStackTrace();
      			
      		}	
   	}
   	
   	//Neue Methode um Adresse zu pr�fen
   	public static boolean doesAdressExist(int postcode, String town, String street, String housenumber) {
   		boolean exist;
   		try(Connection connection = DriverManager.getConnection(url, user, password)) {
   	 	    
    		String checkSQL = "SELECT * FROM adresse WHERE adresse.plz = " + postcode + " AND adresse.stadt= '"+town+ "' AND adresse.strasse= '"+street+"' AND adresse.hausnr = '"+housenumber+"'";
    		Statement stmt= connection.createStatement();
    		ResultSet rs = stmt.executeQuery(checkSQL);
    		
            if (rs.next()) {
        
    		exist = true;
    		rs.close();
  
            return exist;
            }
            else {
            	
            	System.out.println("Adresse existiert bereits");
            	return exist=false;
            }

      		}catch (SQLException ex) {
   		    
      			System.err.println(ex.getMessage());
      			System.err.println("SQL Fehler - " + ex.getMessage());
      			ex.printStackTrace();
      			return exist=false;
      		}	
	
   	}
   
   /*
    public static int addAdresse(int postcode, String town, String street, String housenumber) throws SQLException {
    	int id=-1;
    	String query = "INSERT INTO adresse (plz, stadt, strasse, hausnr) values (?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, postcode);
        ps.setString(2, town);
        ps.setString(3, street);
        ps.setString(4, housenumber);
        //Update 05.06.2021: hier eventuell Fehlersuche
        ps.execute();
        ps.close();
       
        ResultSet rs = null;
		String query2 = "SELECT adresse_id FROM adresse where plz = ? AND stadt = ? AND strasse = ? AND hausnr = ? ";
        PreparedStatement ps2 = connection.prepareStatement(query);
        ps2.setInt(1, postcode);
        ps2.setString(2, town);
        ps2.setString(3, street);
        ps2.setString(4, housenumber);
        
        rs = ps2.executeQuery();
        if (rs.next()) {
        	id = rs.getInt("adresse_id");
        	ps.close();
        	rs.close();
        	
    }
        return id;
    }*/

    /*
     * gebe mir die Adresse anhand id
     *
     */

    public static void getAdresseByID(int id)
    {
        ResultSet rs = null;
       
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
        	 String query = "SELECT * FROM adresse where adresse_id = ? ";
             PreparedStatement ps = connection.prepareStatement(query);
			 ps.setInt(1, id);
			 rs = ps.executeQuery();
			// Iterieren über ResultSet und Ausgabe in Konsole tätigen

		        ResultSetMetaData rsmd = rs.getMetaData();
		        System.out.println("Folgender Treffer wurde gefunden: ");
		        int columnsNumber = rsmd.getColumnCount();

		        while (rs.next())
		        {
		            for (int i = 1; i <= columnsNumber; i++)
		            {
		                if (i > 1) System.out.print(",  ");
		                String columnValue = rs.getString(i);
		                System.out.print(rsmd.getColumnName(i)+ ": " +columnValue);
		            }
		            System.out.println("");
		        }

		        ps.close();
		        rs.close();
		        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        

        
    }

    /*
     * Zeig mir adresse_id anhand der Anschrift
     * UPDATE 06.06.2021: diese Methode kommt zum Einsatz beim einf�gen
     */
    public static int getAddressID(int postcode, String town, String street, String housenumber)
    {
        int result = -1;
        
       
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
        	
        	String query = "SELECT adresse_id FROM adresse where plz = ? AND stadt = ? AND strasse = ? AND hausnr = ? ";
            PreparedStatement ps = connection.prepareStatement(query);
			ps.setInt(1, postcode);
			ps.setString(2, town);
	        ps.setString(3, street);
	        ps.setString(4, housenumber);
	        ResultSet rs = ps.executeQuery();

	        if (rs.next())
	            result = rs.getInt("adresse_id");

	        return result;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return result;
		}
        
    }

    /*
     * Hier werden Kunden in die DB hinzugef�gt. UPDATE: Autoincrement statt Random
     *
     */
    
    public static String AddKunde(String firstname, String lastname, String email, String tel, String bankaccount, int adresse_id, double hours_now, double hours_limit) {
    	
   
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    		
    		String checkSQL = "SELECT Count(*) as test FROM kunde p Where p.Vorname = '" + firstname + "' AND p.Nachname = '" + lastname + "' AND p.bankverbindung = '" + bankaccount + "'"; 		//UPDATE: zus�tzlich noch Bankverbindun zum Identifizieren

            if (EntryExists(checkSQL))
                return "Person ist schon vorhanden und wird deshalb ignoriert!";
    		
    		String query = "INSERT INTO kunde (vorname, nachname, email, tel, bankverbindung, adresse_id, stunden_aktuell, stunden_limit) values (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, firstname);
            ps.setString(2, lastname);
            ps.setString(3, email);
            ps.setString(4, tel);
            ps.setString(5, bankaccount);
            ps.setInt(6, adresse_id);
            ps.setDouble(7, hours_now);
            ps.setDouble(8, hours_limit);
            //Update 05.06: hier eventuell Fehler
            ps.execute();
            ps.close();
            return "Kunde wurde erfolgreich eingef�gt!";
    	}
    	catch (SQLException ex) {
    		    
        	System.err.println(ex.getMessage());
        	System.err.println("SQL Fehler - " + ex.getMessage());
            ex.printStackTrace();
            return "Es ist ein Fehler aufgetreten!";
        }
    	
    }

    public static String AddKunde2(String firstname, String lastname, String email, String tel, String bankaccount, int adresse_id, double hours_now, double hours_limit)
    {
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
        String checkSQL = "SELECT Count(*) as test FROM kunde p Where p.Vorname = '" + firstname + "' AND p.Nachname = '" + lastname + "' AND p.bankverbindung = '" + bankaccount + "'"; 		//UPDATE: zus�tzlich noch Bankverbindun zum Identifizieren

        if (EntryExists(checkSQL))
            return "Person ist schon vorhanden und wird deshalb ignoriert!";

        String query = "INSERT INTO kunde (vorname, nachname, email, tel, bankverbindung, adresse_id, stunden_aktuell, stunden_limit) values (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(query);
       // ps.setInt(1, generateRandomIDForTable("kunde"));
        
			ps.setString(1, firstname);
		
        ps.setString(2, lastname);
        ps.setString(3, email);
        ps.setString(4, tel);
        ps.setString(5, bankaccount);
        ps.setInt(6, adresse_id);
        ps.setDouble(7, hours_now);
        ps.setDouble(8, hours_limit);

        ps.execute();
        return "Kunde wurde erfolgreich hinzugef�gt!";
    	}
        catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Fehler!";
		}
    }

    public static void DeleteKunde(int kundeID) 
    {
        
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
			String query = "DELETE FROM kunde WHERE kunde_id = " + kundeID;
	        Statement stmt;
			stmt = connection.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    
    /*
     * Methode zum einf�gen von Tutoren.
     */
    public static void AddTutor(String firstname, String lastname, String email, String tel, String bankaccount,int paymentperhoure, int adresse_id, String pw) throws SQLException
    {
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
        String checkSQL =  "SELECT Count(*) as test FROM tutor p Where  p.Stundenlohn = " + paymentperhoure +" AND p.Vorname = '" + firstname + "' AND p.Nachname = '" + lastname + "' AND p.bankverbindung = '" + bankaccount + "'" + " AND p.passwort = '" + pw + "'";

        if (EntryExists(checkSQL))
            System.out.println("Person ist schon vorhanden und wird deshalb ignoriert!");

        String query = "INSERT INTO tutor (stundenlohn,vorname, nachname, email, tel, bankverbindung, adresse_id, passwort) values (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(query);

        // ps.setInt(1, generateRandomIDForTable("kunde"));
        ps.setInt(1,paymentperhoure);
        ps.setString(2, firstname);
        ps.setString(3, lastname);
        ps.setString(4, email);
        ps.setString(5, tel);
        ps.setString(6, bankaccount);
        ps.setInt(7, adresse_id);
        ps.setString(8, pw);
        ps.execute();

        String idSQL = "SELECT t.tutor_id FROM tutor t Where t.Vorname = '" + firstname + "' AND t.Nachname = '" + lastname + "' AND t.email = '" + email + "'";

        var stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(idSQL);
        rs.next();
        int id = rs.getInt("tutor_id");

        query = "INSERT INTO user (tutor_id, passwort, email_adresse, admin) values (?, ?, ?, ?)";
        ps = connection.prepareStatement(query);

        // ps.setInt(1, generateRandomIDForTable("kunde"));
        ps.setInt(1, id);
        ps.setString(2, pw);
        ps.setString(3, email);
        ps.setInt(4, 0);
        ps.execute();
       
        System.out.println( "Tutor wurde erfolgreich hinzugef�gt!");
    	} catch(SQLException e) {
    		e.printStackTrace();
    		System.out.println( "Ein Fehler ist aufgetreten!");
    	}


    }
    
    /*
     * L�schen vom Tutor
     */

    public static void DeleteTutor(int TutorID) 
    {
        
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
			String query = "DELETE FROM user WHERE tutor_id = " + TutorID;
	        Statement stmt;
			stmt = connection.createStatement();
			stmt.executeUpdate(query);
	        query = "DELETE FROM tutor WHERE tutor_id = " + TutorID;
	        stmt.executeUpdate(query);
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    
   
    
    /*
     * UPDATE: Gib mir fach_id anhand fach_name & fach_niveau
     */
    
    public static int getFachID (String subject_name, String subject_level) {
    	
    	int id;
    	int test = -1;
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    		
    		ResultSet rs = null;
    		String query = "SELECT fach_id FROM unterrichtsfach where fach_name = ? AND fach_niveau = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, subject_name);
            ps.setString(2, subject_level);
            
            rs = ps.executeQuery();
            if (rs.next()) {
            	id = rs.getInt("fach_id");
            	ps.close();
            	rs.close();
            	return id;
            	
            }
          
            ps.close();
            rs.close();
            
    	}
    	catch (SQLException ex) {  
    		
        	System.err.println(ex.getMessage());
        	System.err.println("SQL Fehler - " + ex.getMessage());
            ex.printStackTrace();
        }	
    	return test;
    }
    
    /*
     * UPDATE: Gib mir tutor_id anhand  seiner Email
     */
    
    public static int getTutorID(String tutorEmail) 
    {
        int result = -1;
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
        String query = "SELECT tutor_id FROM user where email_adresse = '" + tutorEmail + "'";
        PreparedStatement ps;
		
		ps = connection.prepareStatement(query);
		
        ResultSet rs = ps.executeQuery();

        if (rs.next())
            result = rs.getInt("tutor_id");

        return result;
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return result;
		}
    }
    
    /*
     * UPDATE: Gib mir kunde_id anhand seinem vor/nachname
     */
    
    public static int getKundeID(String vorname, String nachName)
    {
        int result = -1;
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
        String query = "SELECT kunde_id FROM kunde where vorname = '" + vorname + "' AND nachname = '" + nachName + "'";
        PreparedStatement ps = connection.prepareStatement(query);
        ResultSet rs;
		
		rs = ps.executeQuery();
		

        if (rs.next())
            result = rs.getInt("kunde_id");

        return result;
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return result;
		}
    }
    
    
    /*
     *  Pr�fen ob es sich um Tutor oder Admin handelt
     */
    public static String LoginMail = "";
    public static int Login_ID = -1;

    public static class LoginHelperClass
    {
        public Boolean LoginIsValid = false;
        public Boolean AdminLogin = false;
    }

    
    
    /*
     *  Login Code
     */
    
    public static LoginHelperClass LoginValid(String email, String Password) 
    {
    	LoginHelperClass result = new LoginHelperClass();
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
        String query = "SELECT Count(*) as test FROM user u WHERE u.email_adresse = '" + email + "' AND u.Passwort = '" + Password + "'";
        Boolean validLogin = EntryExists(query);

        

        if (validLogin)
        {
            result.LoginIsValid = true;

            String isAdminLoginSQL = "SELECT u.Admin from user u where u.email_adresse = '" + email + "' AND u.Passwort = '" + Password + "'";

            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(isAdminLoginSQL);
            rs.next();
            result.AdminLogin = rs.getBoolean("Admin");
            LoginMail = email;
            Login_ID = getTutorID(email);
        }
        else
            {
                result.LoginIsValid = false;
                result.AdminLogin = false;
            }

        return result;
    	}
    	catch(SQLException e) {
    		e.printStackTrace();
    		return result;
    	}
    }
    

    /*
     *  Methode zur Abgabe von allen Sch�ler als Liste
     *  
     */
    public static ArrayList<String> GetAllStudents() 
    {
        ArrayList<String> result = new ArrayList<>();

        try(Connection connection = DriverManager.getConnection(url, user, password))
        {
            String SQL = "SELECT k.vorname, k.nachname FROM kunde k";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL);

            while (rs.next())
            {
                String vorname = rs.getString("vorname");
                String nachname = rs.getString("nachname");

                String fullname = vorname + " " + nachname;

                if (result.contains(fullname) == false)
                    result.add(fullname);
            }
        }
        catch (SQLException var6) {
            System.err.println("SQL Fehler - " + var6.getLocalizedMessage());
            var6.printStackTrace();
        }

        return result;
    }

    /*
     *  Methode zur Abgabe von allen F�cher_Namen als Liste
     *  
     */
    public static ArrayList<String> GetAllFachNames() 
    {
        ArrayList<String> result = new ArrayList<>();

        try(Connection connection = DriverManager.getConnection(url, user, password))
        {
            String SQL = "SELECT u.fach_name, u.fach_niveau FROM unterrichtsfach u";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL);

            while (rs.next())
            {
                String fachName = rs.getString("fach_name");
                String niveau = rs.getString("fach_niveau");
                String fullname = fachName + " " + niveau;

                if (result.contains(fullname) == false)
                    result.add(fullname);
            }
        }
        catch (SQLException var6) {
            System.err.println("SQL Fehler - " + var6.getLocalizedMessage());
            var6.printStackTrace();
        }

        return result;
    }

    /*
     *  Methode zum Einf�gen von Unterrichtseinheiten vom Tutor
     *  
     */
    
    //update 05.06.2021: aus int dauer wird ein double daue
    public static void AddTutorUnterrichtEinheit(int kunde_id, int tutor_id,
                                                 int fach_id, double dauer, Date datum)
            
    {
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
        String query = "INSERT INTO unterrichtseinheit (kunde_id, tutor_id, fach_id, dauer, datum) values (?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, kunde_id);
       
		ps.setInt(2, tutor_id);
		
        ps.setInt(3, fach_id);
        ps.setDouble(4, dauer);
        ps.setDate(5, datum);
        ps.execute();
        ps.close();
    	 } catch (SQLException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    /*
     * Gibt Alle Fach_Niveau als Liste aus
     */
    
    
    public static ArrayList<String> GetAllNiveauNames()
    {
        ArrayList<String> result = new ArrayList<>();

        try(Connection connection = DriverManager.getConnection(url, user, password))
        {
            String SQL = "SELECT u.fach_niveau FROM unterrichtsfach u";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(SQL);

            while (rs.next())
            {
                String niveau = rs.getString("fach_niveau");

                if (result.contains(niveau) == false)
                    result.add(niveau);
            }
        }
        catch (SQLException var6) {
            System.err.println("SQL Fehler - " + var6.getLocalizedMessage());
            var6.printStackTrace();
        }

        return result;
    }
  
    
    /*
     * UPDATE: Neue Methode -> Rechnung f�r Monat & Sch�ler; !VERALTET!
     * Druck nur in der Konsole
     */
    
    
    //UPDATE: Attribute vertaucht
    public static void getInvoiceData(int customer_id, int month, int year) {
    	
    	//die Methode kriegt als Argumente den Monat und das Jahr der Rechnung + die kunden_id
    	//Rechnung Mai 2021 soll dann also alle Einheiten aus April ausspucken
    	
    	if (month == 1) {
    		month = 12;					// Dezember des Vorjahres, wenn eine Januar-Rechung kommen soll
    		year = year-1;
    	}
    	else {
    		month = month-1;
    	}
 
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    	
    		String query = "SELECT unterrichtsfach.fach_name AS Fach, unterrichtsfach.fach_niveau AS Niveau, unterrichtseinheit.datum AS Datum, unterrichtsfach.fach_preis AS Einzelpreis, unterrichtseinheit.dauer AS Anzahl_h, TRUNCATE((unterrichtsfach.fach_preis * unterrichtseinheit.dauer),2) as Gesamtpreis FROM unterrichtseinheit INNER JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id WHERE MONTH(unterrichtseinheit.datum)= ? AND YEAR(unterrichtseinheit.datum) = ? AND unterrichtseinheit.kunde_id = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, month);
            ps.setInt(2, year);
            ps.setInt(3, customer_id);
            ResultSet rs = ps.executeQuery();
            
            int columns = rs.getMetaData().getColumnCount();
            for (int i = 1; i <=columns; i++)
            	System.out.print(String.format("%-15s",rs.getMetaData().getColumnLabel(i)));
            System.out.println();
            System.out.println("------------------------------------------------------------------------------------------------------------");
            
            while(rs.next()) {
            	for (int i = 1; i<= columns; i++)
            		System.out.print(String.format("%-15s", rs.getString(i)));
            	System.out.println();
            }
            
            ps.close();
            rs.close();
            
    	
    	}catch (SQLException ex) {
		    
    	System.err.println(ex.getMessage());
    	System.err.println("SQL Fehler - " + ex.getMessage());
        ex.printStackTrace();
    	} 
    }
    /*
     * UPDATE: gib mir eine ArrayList<String> f�r Rechnung
     */
    
    public static List returnInvoiceData(int customer_id, int month, int year) {
    	
    	List<String> data = new ArrayList<>();
    	
    	if (month == 1) {
    		month = 12;					// Dezember des Vorjahres, wenn eine Januar-Rechung kommen soll
    		year = year-1;
    	}
    	else {
    		month = month-1;
    	}
 
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    	
    		String query = "SELECT (@row_number:=@row_number + 1) AS Pos, unterrichtsfach.fach_name AS Fach, unterrichtsfach.fach_niveau AS Niveau, unterrichtseinheit.datum AS Datum, unterrichtsfach.fach_preis AS Einzelpreis, unterrichtseinheit.dauer AS Anzahl_h, TRUNCATE((unterrichtsfach.fach_preis * unterrichtseinheit.dauer),2) as Gesamtpreis FROM (SELECT @row_number:=0) AS temp, unterrichtseinheit INNER JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id WHERE MONTH(unterrichtseinheit.datum)=? AND YEAR(unterrichtseinheit.datum)=? AND unterrichtseinheit.kunde_id = ? ORDER BY unterrichtseinheit.datum";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, month);
            ps.setInt(2, year);
            ps.setInt(3, customer_id);
            ResultSet rs = ps.executeQuery();
            
            int columns = rs.getMetaData().getColumnCount();
            
            while(rs.next()) {
            	for (int i = 1; i<= columns; i++)
            		data.add(rs.getString(i));
            }
            
            ps.close();
            rs.close();
            return data;         
    	
    	}catch (SQLException ex) {
		    
    	System.err.println(ex.getMessage());
    	System.err.println("SQL Fehler - " + ex.getMessage());
        ex.printStackTrace();
        return data;
    	} 
    }
    	
    	public static String returnTotalPrice(int customer_id, int month, int year)
    	{
    		String price = "0.00";
    		if (month == 1) {
        		month = 12;					// Dezember des Vorjahres, wenn eine Januar-Rechung kommen soll
        		year = year-1;
        	}
        	else {
        		month = month-1;
        	}
    		
    		try(Connection connection = DriverManager.getConnection(url, user, password)) {
    	    	
    			String query = "SELECT TRUNCATE(SUM(unterrichtsfach.fach_preis * unterrichtseinheit.dauer),2) as Gesamtpreis FROM unterrichtseinheit JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id WHERE unterrichtseinheit.kunde_id = ? AND MONTH(unterrichtseinheit.datum)=? AND YEAR(unterrichtseinheit.datum)=? ";
                PreparedStatement ps = connection.prepareStatement(query);
                ps.setInt(1, customer_id);
                ps.setInt(2, month);
                ps.setInt(3, year);
                ResultSet rs = ps.executeQuery();
                
                
                while(rs.next()) {
                	price = rs.getString(1);
                }
                
                ps.close();
                rs.close();
                return price;         
        	
        	}catch (SQLException ex) {
    		    
        	System.err.println(ex.getMessage());
        	System.err.println("SQL Fehler - " + ex.getMessage());
            ex.printStackTrace(); 
            return price;
        	}
    	
    }
    	
   public static ArrayList<String> getAnschriftByKunde_ID(int kunde_id) {
	   
	   ArrayList<String> anschrift = new ArrayList<String>();
	   anschrift.add("An: ");
	   String vorname_nachname="";
       String strasse_hausnummer="";
       String plz_stadt="";
	   
	   try(Connection connection = DriverManager.getConnection(url, user, password)) {
	    	
		   //String query = "SELECT kunde.vorname, kunde.nachname, adresse.strasse, adresse.hausnr, adresse.plz, adresse.stadt from kunde join rechnung join adresse on kunde.kunde_id = rechnung.kunde_id AND rechnung.adresse_id = adresse.adresse_id WHERE kunde.kunde_id = ? ";
		  //Update 06.06. Distinct hhinzuge�fgt
		   String query1 = "SELECT DISTINCT kunde.vorname, kunde.nachname from kunde join rechnung join adresse on kunde.kunde_id = rechnung.kunde_id AND rechnung.adresse_id = adresse.adresse_id WHERE kunde.kunde_id = ? ";
		   PreparedStatement ps1 = connection.prepareStatement(query1);
           ps1.setInt(1, kunde_id);
           ResultSet rs1 = ps1.executeQuery();
           
           int columns = rs1.getMetaData().getColumnCount();
           
           while(rs1.next()) {
           	for (int i = 1; i<= columns; i++)
           		vorname_nachname += (rs1.getString(i))+ " ";
           }
           anschrift.add(vorname_nachname);
           ps1.close();
           rs1.close();
           
           //Jetzt die Stra�e und die Hausnummer
           
           //Update 06.06.2021: Distinct hinzugef�gt
           String query2 = "SELECT DISTINCT adresse.strasse, adresse.hausnr from kunde join rechnung join adresse on kunde.kunde_id = rechnung.kunde_id AND rechnung.adresse_id = adresse.adresse_id WHERE kunde.kunde_id = ? ";
           PreparedStatement ps2 = connection.prepareStatement(query2);
           ps2.setInt(1, kunde_id);
           ResultSet rs2 = ps2.executeQuery();
           
           columns = rs2.getMetaData().getColumnCount();
           
           while(rs2.next()) {
           	for (int i = 1; i<= columns; i++)
           		strasse_hausnummer += (rs2.getString(i))+ " ";
           }
           anschrift.add(strasse_hausnummer);
           ps2.close();
           rs2.close();
           
           //Jetzt Postleitzahl und Stadt
           
         //Update 06.06.2021: Distinct hinzugef�gt
           String query3 = "SELECT DISTINCT adresse.plz, adresse.stadt from kunde join rechnung join adresse on kunde.kunde_id = rechnung.kunde_id AND rechnung.adresse_id = adresse.adresse_id WHERE kunde.kunde_id = ? ";
		   PreparedStatement ps3 = connection.prepareStatement(query3);
           ps3.setInt(1, kunde_id);
           ResultSet rs3 = ps3.executeQuery();
           
           columns = rs3.getMetaData().getColumnCount();
           
           while(rs3.next()) {
           	for (int i = 1; i<= columns; i++)
           		plz_stadt += (rs3.getString(i))+ " ";
           }
           anschrift.add(plz_stadt);
           ps3.close();
           rs3.close();
           
           
           return anschrift;         
   	
   		}catch (SQLException ex) {
		    
   			System.err.println(ex.getMessage());
   			System.err.println("SQL Fehler - " + ex.getMessage());
   			ex.printStackTrace();
   			return anschrift;
   	} 
   }
	   
	
    
    public static int convertStringToInteger(String input) {
    	String value = input;
    	String intValue = value.replaceAll("[^0-9]", "");
    	int convertedString = Integer.valueOf(intValue);
    	return convertedString;
    }
    
    //UPDATE David am 01.06 um 16:21
    public static int getAdresseIdByKundeId(int kunde_id) {
    	
    	int id;
    	int test = -1;
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    		
    		ResultSet rs = null;
    		String query = "SELECT adresse.adresse_id FROM adresse JOIN kunde ON adresse.adresse_id = kunde.adresse_id WHERE kunde.kunde_id = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, kunde_id);
            
            rs = ps.executeQuery();
            if (rs.next()) {
            	id = rs.getInt("adresse_id");
            	ps.close();
            	rs.close();
            	return id;
            	
            }
          
            ps.close();
            rs.close();
            
    	}
    	catch (SQLException ex) {  
    		
        	System.err.println(ex.getMessage());
        	System.err.println("SQL Fehler - " + ex.getMessage());
            ex.printStackTrace();
        }	
    	return test;
    	
    }
    
    //Update 02.06.2021: Methode um Datum f�r MySQL zu konvertieren. 
    public static String dateConverter4MySQL(int jahr, int monat, int tag) {
    	
    	String datum="";
        datum = datum+"'"+jahr; 
        if (monat < 10) {
      	  datum = datum +"-"+ 0 + monat;	// 0 Wird eingef�gt wenn der Monat nicht zweistellig it
        }
        else {
      	  datum = datum +"-"+ monat;		//Ansonsten wird der Monat �bernommen
        }
        
        if (tag < 10) {
        	  datum = datum +"-"+ 0 + tag;	// 0 wird eingef�gt wenn der Tag einstellig
          }
          else {
        	  datum = datum + "-"+ tag;		// Ansonsten wird Tag �bernommen
          }
        datum += "'";
        
        
        
        return datum;
    	
    }
    
    
  //Update 02.06.2021
    public static void addInvoiceForCustomer(int kunde_id, int monat, int jahr) {
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
    		String dateConverted = Database_Helper.dateConverter4MySQL(jahr, monat, 1);
    		
    		String checkSQL = "SELECT * FROM rechnung WHERE rechnung.kunde_id = " + kunde_id + " AND MONTH(rechnung.datum)="+monat+ " AND YEAR(rechnung.datum)= "+jahr+";";
    		Statement stmt= connection.createStatement();
    		ResultSet rs = stmt.executeQuery(checkSQL);
    		
            if (!rs.next()) {
        
    		String query1 = "INSERT INTO rechnung (adresse_id, kunde_id, datum) VALUES (?, ?,?)";
    		PreparedStatement ps = connection.prepareStatement(query1);
    		
   		   
              int adresse = Database_Helper.getAdresseIdByKundeId(kunde_id);
              String datum = "";
              datum = datum+jahr;
              if (monat < 10) {
            	  datum = datum + 0 + monat;
              }
              else {
            	  datum = datum + monat;
              }
              datum = datum + "01";
              int datumInt = Integer.parseInt(datum);
              
              ps.setInt(1, adresse);
              ps.setInt(2, kunde_id);
              ps.setInt(3, datumInt);
              
             // rs.close();
              ps.execute();
              ps.close();
            }
            else {
            	System.out.println("Rechnung existiert bereits");
            	
            }
            
            
      	
      		}catch (SQLException ex) {
   		    
      			System.err.println(ex.getMessage());
      			System.err.println("SQL Fehler - " + ex.getMessage());
      			ex.printStackTrace();
      			
      		}	
    }
    
    //Update 02.06.2021
    public static ArrayList<Integer> generateListOfYears() {
    	
    	 ArrayList<Integer> years = new ArrayList<Integer>();
    	 
    	 try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
  		  
  		   String query1 = "SELECT DISTINCT YEAR(datum) FROM unterrichtseinheit";
  		   PreparedStatement ps1 = connection.prepareStatement(query1);
             ResultSet rs1 = ps1.executeQuery();
             
             int columns = rs1.getMetaData().getColumnCount();
             
             while(rs1.next()) {
             	for (int i = 1; i<= columns; i++)
             		years.add(rs1.getInt(i));
             }
             
             ps1.close();
             rs1.close();
            
             
             return years;         
     	
     		}catch (SQLException ex) {
  		    
     			System.err.println(ex.getMessage());
     			System.err.println("SQL Fehler - " + ex.getMessage());
     			ex.printStackTrace();
     			return years;
     		}
    		
    }
    //Update: 02.06.2021
    public static ArrayList<String> getTutorForInvoice(int kunde_id, int monat, int jahr) {
    	
    	ArrayList<String> tutoren = new ArrayList<String>();
    	String vorname_nachname ="";
    	if (monat == 1) {
    		monat = 12;					// Dezember des Vorjahres, wenn eine Januar-Rechung kommen soll
    		jahr = jahr-1;
    	}
    	else {
    		monat = monat-1;
    	}
    	
 	    try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
 	    	String query1 = "SELECT DISTINCT tutor.vorname,tutor.nachname FROM unterrichtseinheit JOIN tutor on unterrichtseinheit.tutor_id = tutor.tutor_id WHERE unterrichtseinheit.kunde_id = " + kunde_id + " AND MONTH(unterrichtseinheit.datum)="+monat+ " AND YEAR(unterrichtseinheit.datum)= "+jahr+";";
 		    PreparedStatement ps1 = connection.prepareStatement(query1);
            //ps1.setInt(1, kunde_id);
            ResultSet rs1 = ps1.executeQuery();
            
            int columns = rs1.getMetaData().getColumnCount();
            
            while(rs1.next()) {
            	for (int i = 1; i<= columns; i++)
            		vorname_nachname += (rs1.getString(i))+ " ";
            }
            tutoren.add(vorname_nachname);
            ps1.close();
            rs1.close();
            return tutoren;         
    	
    		}catch (SQLException ex) {
 		    
    			System.err.println(ex.getMessage());
    			System.err.println("SQL Fehler - " + ex.getMessage());
    			ex.printStackTrace();
    			return tutoren;
    	} 
    
    	
    }
    //Update 02.06.2021
    public static String getKundeNameByID(int kunde_id) {
		
		
    	String vorname_nachname ="";
    	
    	
 	    try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
 	    	String query1 = "SELECT kunde.vorname,kunde.nachname FROM kunde where kunde_id= ?;";
 		    PreparedStatement ps1 = connection.prepareStatement(query1);
            ps1.setInt(1, kunde_id);
            ResultSet rs1 = ps1.executeQuery();
            
            int columns = rs1.getMetaData().getColumnCount();
            
            while(rs1.next()) {
            	for (int i = 1; i<= columns; i++)
            		vorname_nachname += (rs1.getString(i))+ " ";
            }
           
            ps1.close();
            rs1.close();
            return vorname_nachname;         
    	
    		}catch (SQLException ex) {
 		    
    			System.err.println(ex.getMessage());
    			System.err.println("SQL Fehler - " + ex.getMessage());
    			ex.printStackTrace();
    			return vorname_nachname;
    	} 
		
	}
    
    
    //Update Jenny 01.06.2021
    public static double getIncome(int selectionMonth) {

		double totalIncome = 0;
		double unitprice = 25.00;
		
		//Update 08.06.21: Connection bei try
		try(Connection connection = DriverManager.getConnection(url, user, password)) {
			String SQL = "SELECT * FROM `unterrichtseinheit`";
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(SQL);

			while (rs.next()) {
				Date date = rs.getDate("datum");
				if (date.getMonth() == selectionMonth) {

					double duration = rs.getDouble("dauer");
					double income = duration * unitprice;
					totalIncome += income;
					// Komplette Einnahmen ohne selektiertung nach Datum
				}
				//Update 08.06: keine Ausgabe in Konsole
				//System.out.println(totalIncome);

			}
		} catch (SQLException var6) {
			System.err.println("SQL Fehler - " + var6.getLocalizedMessage());
			var6.printStackTrace();
		}

		return totalIncome;
	}

 //Update 03.06.2021: Fachnamen kriegen
    
    public static ArrayList<String> generateListOfFachName()
    {
    	ArrayList<String> result = new ArrayList<String>();
   	 
   	 try(Connection connection = DriverManager.getConnection(url, user, password)) {
	    	
 		  
 		   String query1 = "SELECT DISTINCT fach_name FROM unterrichtsfach";
 		   PreparedStatement ps1 = connection.prepareStatement(query1);
            ResultSet rs1 = ps1.executeQuery();
            
            int columns = rs1.getMetaData().getColumnCount();
            
            while(rs1.next()) {
            	for (int i = 1; i<= columns; i++)
            		result.add(rs1.getString(i));
            }
            
            ps1.close();
            rs1.close();
           
            
            return result;         
    	
    		}catch (SQLException ex) {
 		    
    			System.err.println(ex.getMessage());
    			System.err.println("SQL Fehler - " + ex.getMessage());
    			ex.printStackTrace();
    			return result;
    		}
    }
  //Update 03.06.2021: Niveaus der F�cher kriegen
    public static ArrayList<String> generateListOfNiveau()
    {
    	ArrayList<String> result = new ArrayList<String>();
   	 
   	 try(Connection connection = DriverManager.getConnection(url, user, password)) {
	    	
 		  
 		   String query1 = "SELECT DISTINCT fach_niveau FROM unterrichtsfach";
 		   PreparedStatement ps1 = connection.prepareStatement(query1);
            ResultSet rs1 = ps1.executeQuery();
            
            int columns = rs1.getMetaData().getColumnCount();
            
            while(rs1.next()) {
            	for (int i = 1; i<= columns; i++)
            		result.add(rs1.getString(i));
            }
            
            ps1.close();
            rs1.close();
           
            
            return result;         
    	
    		}catch (SQLException ex) {
 		    
    			System.err.println(ex.getMessage());
    			System.err.println("SQL Fehler - " + ex.getMessage());
    			ex.printStackTrace();
    			return result;
    		}
    }
    //Update 03.06.2021: Neues Unterrichtsfach anlegen in der DB
    public static void addNeuesFach(String fach_name, String fach_niveau, double preis) {
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
    		
    		
    		String checkSQL = "SELECT * FROM unterrichtsfach WHERE unterrichtsfach.fach_name = '" + fach_name + "' AND unterrichtsfach.fach_niveau='"+ fach_niveau+ "';";
    		Statement stmt= connection.createStatement();
    		ResultSet rs = stmt.executeQuery(checkSQL);
    		
            if (!rs.next()) {
        
    		String query1 = "INSERT INTO unterrichtsfach (fach_name, fach_niveau, fach_preis) VALUES (?, ?,?)";
    		PreparedStatement ps = connection.prepareStatement(query1);
    		
   		   
              ps.setString(1, fach_name);
              ps.setString(2, fach_niveau);
              ps.setDouble(3, preis);
              
             // rs.close();
              ps.execute();
              ps.close();
            }
            else {
            	System.out.println("Fach existiert bereits");
            	
            }
            

      		}catch (SQLException ex) {
   		    
      			System.err.println(ex.getMessage());
      			System.err.println("SQL Fehler - " + ex.getMessage());
      			ex.printStackTrace();
      			
      		}	
    }
    //Update 05.06.21: Erg�nzungen f�r Honorarbescheinigung
    /*
     *  gib mir eine ArrayList<String> f�r Honorar�bersischt_Monat
     */
    
    public static List returnFeeData(int tutor_id,  /*int month_min,*/ int month, int year) {
    	
    	List<String> data = new ArrayList<>();
    	
   

    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    	
    		String query = "SELECT (@row_number:=@row_number + 1) AS Pos, kunde.vorname, kunde.nachname, unterrichtsfach.fach_name AS Fach, "
    				+ "unterrichtsfach.fach_niveau AS Niveau, TRUNCATE(SUM(unterrichtseinheit.dauer),2) as GesamtgeleisteteStunde, "
    				+ "unterrichtsfach.fach_preis as Einzelpreis,TRUNCATE((SUM(unterrichtseinheit.dauer)*unterrichtsfach.fach_preis),2) as Verg�tung "
    				+ "FROM (SELECT @row_number:=0) AS temp, kunde JOIN unterrichtseinheit JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id "
    				+ "WHERE unterrichtseinheit.tutor_id = ? AND MONTH(unterrichtseinheit.datum)= ? /*AND MONTH(unterrichtseinheit.datum)< ?*/ AND YEAR(unterrichtseinheit.datum)= ? "
    				+ "AND kunde.kunde_id=unterrichtseinheit.kunde_id GROUP BY unterrichtseinheit.kunde_id, unterrichtseinheit.fach_id";
            PreparedStatement ps = connection.prepareStatement(query);
           
            ps.setInt(1, tutor_id); 
            //ps.setInt(2, month_min);
            ps.setInt(2, month);
            ps.setInt(3, year);
            ResultSet rs = ps.executeQuery();
            
            int columns = rs.getMetaData().getColumnCount();
            
            while(rs.next()) {
            	for (int i = 1; i<= columns; i++)
            		data.add(rs.getString(i));
            }
            
            ps.close();
            rs.close();
            return data;         
    	
    	}catch (SQLException ex) {
 		    
    	System.err.println(ex.getMessage());
    	System.err.println("SQL Fehler - " + ex.getMessage());
        ex.printStackTrace();
        return data;
    	} 
    }
    
    /*
     * Gesamtdauer eines unterrichteten Fach
     */
     
    public static double returnTotalHour( int tutor_id, int month, int year)
 	{
 		double total_hour = 0;
 		
 		try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
 			String query = "SELECT TRUNCATE(SUM(unterrichtseinheit.dauer),2) as TotalHour FROM unterrichtseinheit JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id WHERE unterrichtseinheit.tutor_id = ?  AND MONTH(unterrichtseinheit.datum)=? AND YEAR(unterrichtseinheit.datum)=? GROUP BY unterrichtseinheit.kunde_id, unterrichtseinheit.fach_id"; 
           PreparedStatement ps = connection.prepareStatement(query);
          
           ps.setInt(1, tutor_id);
           ps.setInt(2, month);
           ps.setInt(3, year);
           ResultSet rs = ps.executeQuery();
           
           
           while(rs.next()) {
         	  total_hour+= rs.getDouble("TotalHour");
           }
           
           ps.close();
           rs.close();
           return total_hour;         
   	
   	}catch (SQLException ex) {
 		    
   	System.err.println(ex.getMessage());
   	System.err.println("SQL Fehler - " + ex.getMessage());
       ex.printStackTrace(); 
       return total_hour;
   	}
 	
 }

    /*
     * Verg�tung eines unterrichteten Fach je gesamte geleisteste stunde
     */
     
    public static double returnTotalSalary( int tutor_id, int month, int year)
 	{
 		double salary = 0;
 		
 		try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
 			String query = "SELECT TRUNCATE((SUM(unterrichtseinheit.dauer)*unterrichtsfach.fach_preis),2) as Verg�tung "
 					+ " FROM unterrichtseinheit JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id /*unterrichtseinheit.kunde_id = ?*/ "
 					+ "WHERE unterrichtseinheit.tutor_id = ? /*AND unterrichtseinheit.fach_id =?*/ AND MONTH(unterrichtseinheit.datum)=? /*AND MONTH(unterrichtseinheit.datum)<?*/ "
 					+ " AND YEAR(unterrichtseinheit.datum)=? GROUP BY unterrichtseinheit.kunde_id, unterrichtseinheit.fach_id"; 
            PreparedStatement ps = connection.prepareStatement(query);
           
            ps.setInt(1, tutor_id);
            
          
            ps.setInt(2, month);
            ps.setInt(3, year);
            ResultSet rs = ps.executeQuery();
            
            
            while(rs.next()) {
            	salary += rs.getDouble("Verg�tung");
            }
            
            ps.close();
            rs.close();
            return salary;         
    	
    	}catch (SQLException ex) {
 		    
    	System.err.println(ex.getMessage());
    	System.err.println("SQL Fehler - " + ex.getMessage());
        ex.printStackTrace(); 
        return salary;
    	}
 	
 }

    
    
    /*
     *  Methode zur Abgabe Vorname/Nachname Tutor als String 
     *  
     */
    public static String GetTutor(int tutor_id) 
    {
      String result = null ;

        try(Connection connection = DriverManager.getConnection(url, user, password))
        {
            String SQL = "SELECT tutor.vorname, tutor.nachname FROM tutor WHERE tutor_id= ?";
            PreparedStatement ps = connection.prepareStatement(SQL);
            ps.setInt(1, tutor_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                String vorname = rs.getString("vorname");
                String nachname = rs.getString("nachname");

                String fullname = vorname + " " + nachname;

               // if (result.contains(fullname) == false) 
                    result= fullname;
                   
                return result;
            
            }
        }
        catch (SQLException var6) {
            System.err.println("SQL Fehler - " + var6.getLocalizedMessage());
            var6.printStackTrace();
        }

        return result;
    }

    // Update methode gibt die Monate an denen ein Tutor gearbeitet hat
    public static ArrayList<Integer>  returnMonth( int tutor_id,  int year)
   	{
   ArrayList<Integer> jahr_honorar= new ArrayList<>();
   int month=0;
  
   try(Connection connection = DriverManager.getConnection(url, user, password))
   {
   String SQL = "SELECT DISTINCT MONTH(unterrichtseinheit.datum) as Month FROM unterrichtseinheit JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id "
   		+ " WHERE unterrichtseinheit.tutor_id =? AND YEAR(unterrichtseinheit.datum)=? ORDER BY MONTH(unterrichtseinheit.datum) ASC ";
   
   PreparedStatement ps = connection.prepareStatement(SQL);
   
   ps.setInt(1, tutor_id);
   ps.setInt(2, year);
   ResultSet rs = ps.executeQuery();
   
   
   while(rs.next()) {
   	month= rs.getInt("Month");
   	
   	
   	jahr_honorar.add(month);
   }
   
   ps.close();
   rs.close();
   
   return jahr_honorar;
   

  	}catch (SQLException ex) {
 		    
  	System.err.println(ex.getMessage());
  	System.err.println("SQL Fehler - " + ex.getMessage());
      ex.printStackTrace(); 
      
  	}
   
   return jahr_honorar;
   	}

    /*
     * j�hrliche Verg�tung eines Tutor
     */
    public static double returnYearSalary( int tutor_id,  int year)
 	{
 		double salary = 0;
 		double year_salary=0;

 	    Database_Helper db = new Database_Helper();
 		
 		ArrayList<Integer> month = db.returnMonth( tutor_id,  year);// um Monate zu bekommen
 		
 		
 		try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	int i=0;
 			while( i<= month.size()-1) {
 	    		
 				int monat= month.get(i);// gibt Monatzahl zur�ck
 				
 			String query = "SELECT TRUNCATE((SUM(unterrichtseinheit.dauer)*unterrichtsfach.fach_preis),2) as Verg�tung FROM unterrichtseinheit JOIN unterrichtsfach ON unterrichtseinheit.fach_id = unterrichtsfach.fach_id WHERE unterrichtseinheit.tutor_id = ? AND MONTH(unterrichtseinheit.datum)=? AND YEAR(unterrichtseinheit.datum)=? GROUP BY unterrichtseinheit.kunde_id, unterrichtseinheit.fach_id"; 
           PreparedStatement ps = connection.prepareStatement(query);
          
           ps.setInt(1, tutor_id);
           
         
           ps.setInt(2, monat);
           ps.setInt(3, year);
           ResultSet rs = ps.executeQuery();
           
           
           while(rs.next()) {
           	salary = rs.getDouble("Verg�tung");
           //Update 08.06.21: keine Konsolenausgabe
           	//	System.out.println(salary);
           	year_salary+= salary;
           	
           }
 	    i++;
           ps.close();
           rs.close();
 	    	}
           return year_salary;         
   	
   	}catch (SQLException ex) {
 		    
   	System.err.println(ex.getMessage());
   	System.err.println("SQL Fehler - " + ex.getMessage());
       ex.printStackTrace(); 
       return year_salary;
   	}
 	
 }
    
    
    //Update 03.06.2021: Methode um Preise der F�cher zu aktualisieren
    public static void updateFachPreis(String fach_name, String fach_niveau, double preis) {
    	
    		try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
    		
    		String query1 = "UPDATE unterrichtsfach SET fach_preis =" +preis+"WHERE fach_name = '"+fach_name +"' AND fach_niveau='"+fach_niveau+"' ;";
    		PreparedStatement ps = connection.prepareStatement(query1);

             // rs.close();
              ps.execute();
              ps.close();
            
            

      		}catch (SQLException ex) {
   		    
      			System.err.println(ex.getMessage());
      			System.err.println("SQL Fehler - " + ex.getMessage());
      			ex.printStackTrace();
      			
      		}	
    	
    }
    
    //Update 05.06 : Aktualisiere die Stunden, die ein Kunde aktuell hat
    
    public static void updateHours(int kunde_id) {
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
 	    	
    		
	String query1 ="UPDATE kunde SET kunde.stunden_aktuell = (SELECT SUM(unterrichtseinheit.dauer) FROM unterrichtseinheit where unterrichtseinheit.kunde_id = "+kunde_id+") WHERE kunde.kunde_id = "+kunde_id ;
    		PreparedStatement ps = connection.prepareStatement(query1);

             // rs.close();
              ps.execute();
              ps.close();
            
            

      		}catch (SQLException ex) {
   		    
      			System.err.println(ex.getMessage());
      			System.err.println("SQL Fehler - " + ex.getMessage());
      			ex.printStackTrace();
      			
      		}	
    	
    	
    }
    //Update 05.06 : stunden_limit als double holen
    public static double getLimitFromKunde(int kunde_id) {
    	double limit;
    	double test = -1;
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    		
    		ResultSet rs = null;
    		String query = "SELECT stunden_limit FROM kunde WHERE kunde_id = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, kunde_id);
            
            rs = ps.executeQuery();
            if (rs.next()) {
            	limit = rs.getDouble("stunden_limit");
            	ps.close();
            	rs.close();
            	return limit;
            	
            }
          
            ps.close();
            rs.close();
            
    	}
    	catch (SQLException ex) {  
    		
        	System.err.println(ex.getMessage());
        	System.err.println("SQL Fehler - " + ex.getMessage());
            ex.printStackTrace();
        }	
    	return test;
    }
    
  //Update 05.06 : stunden_aktuell als double holen
    public static double getStundenAktuellFromKunde(int kunde_id) {
    	double limit;
    	double test = -1;
    	
    	try(Connection connection = DriverManager.getConnection(url, user, password)) {
    		
    		ResultSet rs = null;
    		String query = "SELECT stunden_aktuell FROM kunde WHERE kunde_id = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, kunde_id);
            
            rs = ps.executeQuery();
            if (rs.next()) {
            	limit = rs.getDouble("stunden_aktuell");
            	ps.close();
            	rs.close();
            	return limit;
            	
            }
          
            ps.close();
            rs.close();
            
    	}
    	catch (SQLException ex) {  
    		
        	System.err.println(ex.getMessage());
        	System.err.println("SQL Fehler - " + ex.getMessage());
            ex.printStackTrace();
        }	
    	return test;
    }

    public static class tableViewHelperClass
    {
        public ArrayList<TableColumn> columnList = null;
        public ObservableList<ObservableList> data = null;
    }

    public static tableViewHelperClass buildDataFromTable(String tableName)
    {
        tableViewHelperClass result = new tableViewHelperClass();
        result.columnList = new ArrayList<TableColumn>();

        Connection c;
        data = FXCollections.observableArrayList();
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
            c = connection;
            //SQL FOR SELECTING ALL OF CUSTOMER
            String SQL = "SELECT * FROM " + tableName;
            //ResultSet
            ResultSet rs = c.createStatement().executeQuery(SQL);

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                //We are using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });
                result.columnList.add(col);

                System.out.println("Column [" + i + "] ");
            }


            while (rs.next())
            {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            result.data = data;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }

        return result;
    }
    
    //Update 04.06.2021: optimierte �bersicht aller Kunden
    public static tableViewHelperClass buildDataFromTableForKunden()
    {
        tableViewHelperClass result = new tableViewHelperClass();
        result.columnList = new ArrayList<TableColumn>();

        Connection c;
        data = FXCollections.observableArrayList();
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
            c = connection;
            //SQL FOR SELECTING ALL OF CUSTOMER
            String SQL = "SELECT kunde.kunde_id AS 'ID', kunde.vorname AS 'Vorname', kunde.nachname AS Nachname, kunde.email,kunde.tel, kunde.bankverbindung, adresse.plz, adresse.stadt,adresse.strasse, kunde.stunden_aktuell, kunde.stunden_limit FROM kunde JOIN adresse ON kunde.adresse_id =adresse.adresse_id ";
            //ResultSet
            ResultSet rs = c.createStatement().executeQuery(SQL);

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) { //hier -1
                //We are using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });
                result.columnList.add(col);

                System.out.println("Column [" + i + "] ");
            }


            while (rs.next())
            {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            result.data = data;
          //TEST TEST TEST
            rs.close();
            //connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }

        return result;
    }
    
  //Update 04.06.2021: optimierte �bersicht aller Tutoren
  // !! Nachtrag: hier muss irgendwo ein Fehler sein bei dieser Methode  
    public static tableViewHelperClass buildDataFromTableForTutoren()
    {
        tableViewHelperClass result = new tableViewHelperClass();
        result.columnList = new ArrayList<TableColumn>();

        Connection c;
        data = FXCollections.observableArrayList();
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
            c = connection;
            //SQL FOR SELECTING ALL OF CUSTOMER
            String SQL = "SELECT tutor.tutor_id, tutor.vorname, tutor.nachname, tutor.stundenlohn, tutor.email, tutor.tel, tutor.bankverbindung, adresse.plz, adresse.stadt, adresse.strasse, adresse.hausnr FROM tutor JOIN adresse ON tutor.adresse_id = adresse.adresse_id ";
            //ResultSet
            ResultSet rs = c.createStatement().executeQuery(SQL);

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                //We are using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });
                result.columnList.add(col);

                System.out.println("Column [" + i + "] ");
            }


            while (rs.next())
            {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            result.data = data;
            
            
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }

        return result;
    }
    
    //Update 04.06.2021: �bersicht aller Kunden F�R TUTOREN
    public static tableViewHelperClass buildDataFromTableInTutor(int tutor_id)
    {
        tableViewHelperClass result = new tableViewHelperClass();
        result.columnList = new ArrayList<TableColumn>();

        Connection c;
        data = FXCollections.observableArrayList();
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
            c = connection;
            //SQL FOR SELECTING ALL OF CUSTOMER
            String SQL = "SELECT DISTINCT kunde.kunde_id, kunde.vorname, kunde.nachname, kunde.email, kunde.tel, adresse.plz, adresse.stadt, adresse.strasse, adresse.hausnr, kunde.stunden_aktuell, kunde.stunden_limit FROM (kunde JOIN unterrichtseinheit ON kunde.kunde_id = unterrichtseinheit.kunde_id) JOIN adresse on kunde.adresse_id = adresse.adresse_id WHERE unterrichtseinheit.tutor_id = "+ tutor_id ;
            //ResultSet
            ResultSet rs = c.createStatement().executeQuery(SQL);

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                //We are using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });
                result.columnList.add(col);

                System.out.println("Column [" + i + "] ");
            }


            while (rs.next())
            {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            result.data = data;
          
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }

        return result;
    }
    
    //Update 06.06.2021: �bersicht aller F�cher
    public static tableViewHelperClass buildDataFromTableForUnterrichtsfach()
    {
        tableViewHelperClass result = new tableViewHelperClass();
        result.columnList = new ArrayList<TableColumn>();

        Connection c;
        data = FXCollections.observableArrayList();
        try(Connection connection = DriverManager.getConnection(url, user, password)) {
            c = connection;
            //SQL FOR SELECTING ALL OF CUSTOMER
            String SQL = "SELECT fach_name, fach_niveau, fach_preis FROM unterrichtsfach";
            //ResultSet
            ResultSet rs = c.createStatement().executeQuery(SQL);

            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) { 
                //We are using non property style for making dynamic table
                final int j = i;
                TableColumn col = new TableColumn(rs.getMetaData().getColumnName(i + 1));
                col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
                    public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });
                result.columnList.add(col);

                System.out.println("Column [" + i + "] ");
            }


            while (rs.next())
            {
                //Iterate Row
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    //Iterate Column
                    row.add(rs.getString(i));
                }
                System.out.println("Row [1] added " + row);
                data.add(row);

            }

            result.data = data;
          
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Data");
        }

        return result;
    }
    
    
    
 // Update 03.06.2021 Jenny
 // Update 08.06.2021: Verbindung zum MySQL-Server   
  	public static double getFee(int selectionMonth){

  		double totalFee = 0;
  	
  		try (Connection connection = DriverManager.getConnection(url, user, password)){
  			String SQL = "SELECT * FROM `QAnQ6VscJZ`.`unterrichtsfach` JOIN unterrichtseinheit ON unterrichtsfach.fach_id = unterrichtseinheit.fach_id";
  			Statement stmt = connection.createStatement();
  			ResultSet rs = stmt.executeQuery(SQL);

  			while (rs.next()) {
  				Date date = rs.getDate("datum");
  				if (date.getMonth() == selectionMonth) {

  					double duration = rs.getDouble("dauer");
  					double unitSalary = rs.getDouble("fach_preis");
  					double fee = duration * unitSalary;
  					totalFee += fee;
  					// Komplette Ausgaben ohne selektiertung nach Datum
  				}

  		//		System.out.println(totalFee);

  			}
  		} catch (SQLException var6) {
  			System.err.println("SQL Fehler - " + var6.getLocalizedMessage());
  			var6.printStackTrace();
  		}

  		return totalFee;
  	}
  	//Update 08.06.21: Versuch einer Korrektur
  	public static double getProfit(int selectionMonth)  {
  		
  		double profit = 0;
		try(Connection connection = DriverManager.getConnection(url, user, password)) {
			
	  		double income = getIncome(selectionMonth);
	  		double fee;
			fee = getFee(selectionMonth);
			profit = income - fee;
			return profit;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return profit;
		}
  		
  		
  		
  		
  	}
  	
    
    public static void main(String[] args) {
    	
		//int i ;
		Database_Helper db = new Database_Helper();
		
    	//System.out.println(returnTotalPrice(1579, 5, 2021));
    	
    	//List test = db.returnInvoiceData(1579, 5, 2021);
    	
    	//System.out.println(test.size());
    	//System.out.println((convertStringToInteger("G1234xC")));
    	/*ArrayList<String> test = db.generateListOfNiveau();
    	
    	for (int i = 0;i<test.size();i++){
		    System.out.println((test.get(i).toString()));
		    }*/
    	//db.updateFachPreis("Biologie", "Primarbereich", 25.00);
    	System.out.println(db.doesAdressExist(57072, "dfh", "Holzweg", "1"));
		}
		
	
    	
    	
    	
	}
    



