package sample;

public class helperFunctions
{
    public static boolean StringIsEmpty(String textToCheck)
    {
        return textToCheck == null || textToCheck.trim().length() == 0;
    }
}
