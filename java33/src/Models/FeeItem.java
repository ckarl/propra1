package Models;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import sample.Database_Helper;


public class FeeItem {
	
	Database_Helper db;
	
	private int fee_PositionNr;
	private Customer customer;
	private double hour_Tutor;
	private double hourly_Wage;
	private double total_Wage;
	private double salary;
	
	
	public FeeItem(int fee_PositionNr, Customer customer, double hour_Tutor, double hourly_Wage, double total_Wage, double salary) {
		
		this.fee_PositionNr = fee_PositionNr;
		this.customer = customer;
		this.hour_Tutor = hour_Tutor;
		this.hourly_Wage = hourly_Wage;
		this.total_Wage = total_Wage;
		this.salary = salary;
	}
	
	
	public FeeItem() {
		
	}
	
	public int getFee_PositionNr() {
		return fee_PositionNr;
	}
	
	public void setFee_PositionNr(int fee_PositionNr) {
		this.fee_PositionNr = fee_PositionNr;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public double getHour_Tutor() {
		return hour_Tutor;
	}
	
	public void setHour_Tutor(double hour_Tutor) {
		this.hour_Tutor = hour_Tutor;
	}
	
	public double getHourly_Wage() {
		return hourly_Wage;
	}
	
	public void setHourly_Wage(double hourly_Wage) {
		this.hourly_Wage = hourly_Wage;
	}
	
	public double getTotal_Wage() {
		return total_Wage;
	}
	
	public void setTotal_Wage(double total_Wage) {
		this.total_Wage = total_Wage;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	
	
	public String toString() {
		return " " + fee_PositionNr + " " + customer + " " + hour_Tutor + " " + hourly_Wage + " " + total_Wage + " " + salary;
		}

	
	
	// Erstelle eine monatliche �bersicht der Honorarbescheinigung f�r Tutor
	
	public void createPDFforFeeItem( int tutor_id, int monat_now, int jahr) {
		
		String monatstring="";	//wenn Monat kleiner als 10 ist, dann soll der Monat mit einer 0 davor ausgegeben werden. Also anstatt "9" dann "09"
		if (monat_now < 10) {
			 monatstring = "0"+monat_now;
		}
		
		try {
			
			
			// Name von Tutor
			String name_tutor= db.GetTutor(tutor_id);
			
			
			Font headline_font = new Font(Font.FontFamily.TIMES_ROMAN, 18,
		            Font.BOLD);
			Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
		            Font.BOLD);
			Font supersmallBold = new Font(Font.FontFamily.TIMES_ROMAN, 8,
		            Font.BOLDITALIC);
			
			//Create File Name -> ANMERKUNG: Pfad anpassen
			//String file_name = "C:\\Users\\David\\Desktop\\honorare\\" + name_tutor +"_honorarbescheinigung_"+monatstring+ jahr +".pdf";
			String homeDir = System.getProperty("user.home");
			System.out.println("homeDir                = " + homeDir);
			File file_name = new File( homeDir+"\\Downloads\\"+ name_tutor +"_honorarbescheinigung_"+monatstring+""+jahr+".pdf");
		   //�berpr�fung, ob der Dateiname schon existiert, wenn ja wird eine Zahl hinter dran geh�ngt
			int fileNo=0;
		    if (file_name.exists() && !file_name.isDirectory()) {
			while(file_name.exists()){
				fileNo++;
				file_name = new File(homeDir+"\\Downloads\\"+ name_tutor +"_honorarbescheinigung_"+monatstring+""+jahr+ "("+  fileNo +")"+ ".pdf" );
				
				}

		    }else if (!file_name.exists()) {
		    	file_name.createNewFile();
		    }
			
			//Create Document object
			Document document = new Document();
			
			//Get pdfwriter instance
			PdfWriter.getInstance(document, new FileOutputStream(file_name));
			
			//Open the document
			document.open();
			
			//Add image in pdf -> ANMERKUNG: Pfad anpassen
			document.add(Image.getInstance("C:\\Users\\David\\Desktop\\hiertesteichpdf\\Braingain_Bild.PNG"));
			
			// Anschrift
						/*document.add(new Paragraph(" "));
						List anschrift = db.getAnschriftByKunde_ID(kunde_id);
						for (int i = 0;i<anschrift.size();i++){
							Paragraph zeile = new Paragraph();
							zeile.add(new Paragraph(anschrift.get(i).toString())); //Methode einf�gen getKundeByID
							document.add(zeile);
						}*/

			// Add a content
			document.add(new Paragraph(" "));
			Paragraph headline = new Paragraph();
	        headline.add(new Paragraph("Honorarbescheinigung", headline_font));
			document.add(headline);
			document.add(new Paragraph(" "));
			
			Paragraph nametutor = new Paragraph();
			nametutor.add(new Paragraph("Vor/Nachname: " + name_tutor, smallBold));
			document.add(nametutor);
			
			//Hier kommen das Datum
			
			Paragraph datum = new Paragraph();
			datum.add(new Paragraph("F�r den Monat: " + monatstring + "."+jahr)); //Methode einf�gen Monat und Jahr
			document.add(datum);
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			
				
			    PdfPTable table = new PdfPTable(8); //zwischen den Klammern: Anzahl von Spalten
			    table.setWidthPercentage(95);
			    table.setWidths(new int[] {1,2,2,2,2,1,1,1}); // passt die Gr��e jeweiliger Spalte an
			    
			
			//Create Cell object
			PdfPCell c1 = new PdfPCell(new Phrase("Pos", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Sch�ler(in) Vorname", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Sch�ler(in) Nachname" , FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			
			c1 = new PdfPCell(new Phrase("Fach", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Niveau", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Anzahl Stunde", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Honorar pro h ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Verg�tung ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			table.setHeaderRows(1);
			
			
			//Einf�gen aus DB
			
			List test = db.returnFeeData(tutor_id, monat_now, jahr);
			
			for (int i = 0;i<test.size();i++){
			    table.addCell(new Phrase(test.get(i).toString(),FontFactory.getFont(FontFactory.HELVETICA, 9) ));
			}

			document.add(table);
			
			
			//Gesamtbetrag
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			Paragraph gesamtpreis = new Paragraph();
			gesamtpreis.add(new Paragraph("Gesamtbetrag: "+db.returnTotalSalary( tutor_id, monat_now, jahr) +"�"  , smallBold));
		
			gesamtpreis.setAlignment(Element.ALIGN_LEFT);
			
			document.add(gesamtpreis);
			
			
			//Update 07.06.2021: Kontaktdaten und Bankdaten auf den Honorarbescheid hinzuf�gen
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			
			
			
			 document.add(Chunk.NEWLINE);
			 LineSeparator ls = new LineSeparator();
			 document.add(new Chunk(ls));
			 //Create the table which will be 2 Columns wide and make it 100% of the page
			 PdfPTable myTable = new PdfPTable(2);
			 myTable.setWidthPercentage(100.0f);
			 
			//create a 2 cells and add them to the table
			 PdfPCell cellOne = new PdfPCell(new Paragraph("Institut f�r Lernf�rderung\nKatharina Becker\nHillerbacherweg 16\n57319 Bad Berleburg\n0178 1403710\nkatharina.becker.94@outlook.de", supersmallBold));
			 PdfPCell cellTwo = new PdfPCell(new Paragraph("Volksbank S�dwestfallen eG\nBIC: GENODEM1NRD\nIBAN: DE28447615345703564200\n Steuernummer: 342/50162169\nFinanzamt Siegen", supersmallBold));
			 
			 cellOne.setBorder(Rectangle.NO_BORDER);
			 cellOne.setHorizontalAlignment(Element.ALIGN_LEFT);
			 cellTwo.setBorder(Rectangle.LEFT);
			 cellTwo.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 
			 //Add the 2 cells to the table
			 myTable.addCell(cellOne);
			 myTable.addCell(cellTwo);
			 
			 document.add(myTable);
			
			
			//Close the document
			document.close();
			
			
			System.out.println("finished");
			
		    }  catch (Exception e) {
		    	System.err.println(e);
			
		    }
		}
		
		
	public void createPDFforYearFeeItem( int tutor_id, int jahr) {

		/*
		 * Name_Monate
		 */
		ArrayList<String> month_name= new ArrayList<String>();
					month_name.add("Januar");
					month_name.add("Februar");
					month_name.add("M�rz");
					month_name.add("April");
					month_name.add("Mai");
					month_name.add("Juni");
					month_name.add("Juli");
					month_name.add("August");
					month_name.add("September");
					month_name.add("Oktober");
					month_name.add("November");
					month_name.add("Dezember");
					
		try {
			
			
			// Name von Tutor
			String name_tutor= db.GetTutor(tutor_id);
			
			
			Font headline_font = new Font(Font.FontFamily.TIMES_ROMAN, 18,
		            Font.BOLD);
			Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
		            Font.BOLD);
			Font supersmallBold = new Font(Font.FontFamily.TIMES_ROMAN, 8,
		            Font.BOLDITALIC);
			
			//Create File Name -> ANMERKUNG: Pfad anpassen
			//String file_name = "C:\\Users\\David\\Desktop\\honorare\\" + name_tutor +"_honorarbescheid_"+jahr +".pdf";
			String homeDir = System.getProperty("user.home");
			System.out.println("homeDir                = " + homeDir);
			File file_name = new File( homeDir+"\\Downloads\\"+ name_tutor +"_honorarbescheinigung_"+jahr+".pdf");
		   //�berpr�fung, ob der Dateiname schon existiert, wenn ja wird eine Zahl hinter dran geh�ngt
			int fileNo=0;
		    if (file_name.exists() && !file_name.isDirectory()) {
			while(file_name.exists()){
				fileNo++;
				file_name = new File(homeDir+"\\Downloads\\"+ name_tutor +"_honorarbescheinigung_"+jahr+"rechnung"+ "("+  fileNo +")"+ ".pdf" );
				
				}

		    }else if (!file_name.exists()) {
		    	file_name.createNewFile();
		    }
			//Create Document object
			Document document = new Document();
			
			//Get pdfwriter instance
			PdfWriter.getInstance(document, new FileOutputStream(file_name));
			
			//Open the document
			document.open();
			
			//Add image in pdf -> ANMERKUNG: Pfad anpassen
			document.add(Image.getInstance("C:\\Users\\David\\Desktop\\hiertesteichpdf\\Braingain_Bild.PNG"));
			
			// Anschrift
						/*document.add(new Paragraph(" "));
						List anschrift = db.getAnschriftByKunde_ID(kunde_id);
						for (int i = 0;i<anschrift.size();i++){
							Paragraph zeile = new Paragraph();
							zeile.add(new Paragraph(anschrift.get(i).toString())); //Methode einf�gen getKundeByID
							document.add(zeile);
						}*/

			// Add a content
			document.add(new Paragraph(" "));
			Paragraph headline = new Paragraph();
	        headline.add(new Paragraph("J�hrliche Honorarbescheinigung", headline_font));
			document.add(headline);
			document.add(new Paragraph(" "));
			
			Paragraph nametutor = new Paragraph();
			nametutor.add(new Paragraph("Vor/Nachname: " + name_tutor, smallBold));
			document.add(nametutor);
			
			//Hier kommen das Datum
			
			Paragraph datum = new Paragraph();
			datum.add(new Paragraph("F�r das Jahr: " + +jahr)); //Methode einf�gen Monat und Jahr
			document.add(datum);
			
			
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			//Create Cell object
			PdfPTable tableau = new PdfPTable(3); //zwischen den Klammern: Anzahl von Spalten
		    tableau.setWidthPercentage(95);
			
				// Stunde Tabelle
			 PdfPTable table = new PdfPTable(1); //zwischen den Klammern: Anzahl von Spalten
			 table.setWidthPercentage(95);
			 
			 PdfPCell c =new PdfPCell(new Phrase("Anzahl Stunde", FontFactory.getFont(FontFactory.HELVETICA, 9)));
		     c.setVerticalAlignment(Element.ALIGN_CENTER);
			 table.addCell(c)
				.setBackgroundColor(BaseColor.GREEN);;
			   
			  
				// Einf�gen der geleisteten Stunden pro Monat von Tutor in der Tabelle Stunde
				ArrayList<Integer> test = db.returnMonth( tutor_id,   jahr);
				double hour;
				for (int i = 0;i<test.size();i++){
					int monat= test.get(i);
					System.out.println(monat);
					 hour= db.returnTotalHour( tutor_id, monat, jahr);
				    table.addCell(new Phrase(""+hour, FontFactory.getFont(FontFactory.HELVETICA, 9) ));
				}
				tableau.addCell(table);
				
				
				//Monat Tabelle
				PdfPTable table3 = new PdfPTable(1); //zwischen den Klammern: Anzahl von Spalten
			    table3.setWidthPercentage(95);
				PdfPCell c2 = new PdfPCell(new Phrase("Monat ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
				c2.setVerticalAlignment(Element.ALIGN_CENTER);
				table3.addCell(c2)
				.setBackgroundColor(BaseColor.GREEN);;
				  
				//Einf�gen von Monate, an denen der Tutor gearbeitet hat in der Tabelle Monat
				ArrayList<Integer> test1 = db.returnMonth( tutor_id,jahr);
				for (int i = 0;i<test1.size();i++){
					for(int j = 0;j<month_name.size();j++) { 
						
						int monat= test.get(i);
						String name= month_name.get(j);
						
					if(monat==(j+1)){
					table3.addCell(new Phrase(""+name, FontFactory.getFont(FontFactory.HELVETICA, 9) ));
					} else {
						System.out.println("fehler");
					}
					}
				}
				tableau.addCell(table3); // einf�gen von Tab3 in einer gr��eren tab
				
				
				// Verg�tung Tabelle
				PdfPTable table2 = new PdfPTable(1); //zwischen den Klammern: Anzahl von Spalten
			    table2.setWidthPercentage(95);
			    PdfPCell c1 = new PdfPCell(new Phrase("Lohn ", FontFactory.getFont(FontFactory.HELVETICA, 9)));
				c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table2.addCell(c1)
				.setBackgroundColor(BaseColor.GREEN);;
				table2.setHeaderRows(1);
				
				  //Einf�gen von monatliche L�hne von Tutor in der Tab Verg�tung
				ArrayList<Integer> test2 = db.returnMonth( tutor_id, jahr);
				double salary=0;
				for (int i = 0;i<test2.size();i++){
					int monat= test2.get(i);
					salary= db.returnTotalSalary( tutor_id, monat, jahr);
				    table2.addCell(new Phrase(""+salary, FontFactory.getFont(FontFactory.HELVETICA, 9) ));
				    
				}
				tableau.addCell(table2);
				document.add(tableau);
			
		
			
			//Gesamtbetrag
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			Paragraph gesamtpreis = new Paragraph();
			gesamtpreis.add(new Paragraph("Jahresgehalt: "+db.returnYearSalary( tutor_id, jahr) +"�"  , smallBold));
		
			gesamtpreis.setAlignment(Element.ALIGN_LEFT);
			
			document.add(gesamtpreis);
			
			
			//Update 07.06.2021: Kontaktdaten und Bankdaten auf den Honorarbescheid hinzuf�gen
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			
			
			
			 document.add(Chunk.NEWLINE);
			 LineSeparator ls = new LineSeparator();
			 document.add(new Chunk(ls));
			 //Create the table which will be 2 Columns wide and make it 100% of the page
			 PdfPTable myTable = new PdfPTable(2);
			 myTable.setWidthPercentage(100.0f);
			 
			//create a 2 cells and add them to the table
			 PdfPCell cellOne = new PdfPCell(new Paragraph("Institut f�r Lernf�rderung\nKatharina Becker\nHillerbacherweg 16\n57319 Bad Berleburg\n0178 1403710\nkatharina.becker.94@outlook.de", supersmallBold));
			 PdfPCell cellTwo = new PdfPCell(new Paragraph("Volksbank S�dwestfallen eG\nBIC: GENODEM1NRD\nIBAN: DE28447615345703564200\n Steuernummer: 342/50162169\nFinanzamt Siegen", supersmallBold));
			 
			 cellOne.setBorder(Rectangle.NO_BORDER);
			 cellOne.setHorizontalAlignment(Element.ALIGN_LEFT);
			 cellTwo.setBorder(Rectangle.LEFT);
			 cellTwo.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 
			 //Add the 2 cells to the table
			 myTable.addCell(cellOne);
			 myTable.addCell(cellTwo);
			 
			 document.add(myTable);
			
			
			//Close the document
			document.close();
			
			
			System.out.println("finished");
			
			
			
		    }  catch (Exception e) {
		    	System.err.println(e);
			
		    }
		
		
		}
		
	public static void main(String[] args) {
		FeeItem fee = new FeeItem();
		
		fee.createPDFforFeeItem( 19,  7, 2021);
		
		//fee.createPDFforYearFeeItem(5, 2021);
	}

}

