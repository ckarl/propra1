package Models;

import java.util.Date;

public class InvoiceLine {
	
	private int invoice_PositionNr;
	private String service;
	private Date invoice_Time;
	private double unitPrice;
	private double hour_Customer;
	private double totalPrice;
	
	
	public InvoiceLine(int invoice_PositionNr, String service, Date invoice_Time, double unitPrice, double hour_Customer, double totalPrice) {
		
		this.invoice_PositionNr = invoice_PositionNr;
		this.service = service;
		this.invoice_Time = invoice_Time;
		this.unitPrice = unitPrice;
		this.hour_Customer = hour_Customer;
		this.totalPrice = totalPrice;
	}
	
	public int getInvoice_positionNr() {
		return invoice_PositionNr;
	}
	
	public void setInvoice_PositionNr(int invoice_PositionNr) {
		this.invoice_PositionNr = invoice_PositionNr;
	}
	
	public String getService() {
		return service;
	}
	
	public void setService(String service) {
		this.service = service;
	}

	public Date getInvoice_Time() {
		return invoice_Time;
	}
	
	public void setInvoice_Time(Date invoice_Time) {
		this.invoice_Time = invoice_Time;
	}
	
	public double getUnitPrice() {
		return unitPrice;
	}
	
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public double getHour_Customer() {
		return hour_Customer;
	}
	
	public void setHour_Customer(double hour_Customer) {
		this.hour_Customer = hour_Customer;
	}
	
	public double getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
	public String toString() {
		return " " + invoice_PositionNr + " " + service + " " + invoice_Time + " " + unitPrice + " " + hour_Customer + " " + totalPrice;
	}
	

}
