package Models;

public class Tutor extends Person{
	private String bankData;
	private String email;

	
	public Tutor(int id, String lastname, String firstname, int phoneNumber, Address address, String bankData, String email) {
		super(id, lastname, firstname, phoneNumber, address);
		
		this.bankData = bankData;
		this.email = email;
	}
	
	public String getBankData() {
		return bankData;
	}
	
	public void setBankData(String bankData) {
		this.bankData = bankData;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	@Override
	public String toString() {
		
		return super.toString() + " " + bankData + " " + email;
	}

}
