package Models;

import java.util.Calendar;


import java.util.Date;
import java.util.TimeZone;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import sample.Database_Helper;

import java.sql.*;

public class Invoice {
	
	// NEU
	Database_Helper db;
	
	//
	
	private int invoiceNr;
	private InvoiceLine invoiceLine;
	private Customer customer;
	
	
	public Invoice(int invoiceNr, InvoiceLine invoiceLine, Customer customer) {
		this.invoiceNr = invoiceNr;
		this.invoiceLine = invoiceLine;
		this.customer = customer;
	}
	
	public Invoice() {
		
	}
	
	public int getInvoiceNr() {
		return invoiceNr;
	}
	
	public void setInvoiceNr(int invoiceNr) {
		this.invoiceNr = invoiceNr;
	}
	
	public InvoiceLine getInvoiceLine() {
		return invoiceLine;
	}
	
	public void setInvoiceLine(InvoiceLine invoiceLine) {
		this.invoiceLine = invoiceLine;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	public String toString() {
		return " " + invoiceNr + " " + invoiceLine + " " + customer;
	}
	
	// AB HIER NEU
	public void createPDF(int kunde_id, int monat, int jahr) {
		
		String monatstring="";	//wenn Monat kleiner als 10 ist, dann soll der Monat mit einer 0 davor ausgegeben werden. Also anstatt "9" dann "09"
		if (monat < 10) {
			 monatstring = "0"+monat;
		} 
		
		try {
			
			
			String rechnungsnr = kunde_id + "-" + jahr +"-" + monatstring;
			
			Font headline_font = new Font(Font.FontFamily.TIMES_ROMAN, 18,
		            Font.BOLD);
			Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
		            Font.BOLD);
			Font supersmallBold = new Font(Font.FontFamily.TIMES_ROMAN, 8,
		            Font.BOLDITALIC);
		
			
			//Create File Name -> ANMERKUNG: Pfad anpassen
			//Update damit man nicht den Pfad immer anpassen muss
			String homeDir = System.getProperty("user.home");
			System.out.println("homeDir                = " + homeDir);
			File file_name = new File( homeDir+"\\Downloads\\"+ kunde_id +""+monat+""+jahr+"rechnung.pdf");
		   //�berpr�fung, ob der Dateiname schon existiert, wenn ja wird eine Zahl hinter dran geh�ngt
			int fileNo=0;
		    if (file_name.exists() && !file_name.isDirectory()) {
			while(file_name.exists()){
				fileNo++;
				file_name = new File(homeDir+"\\Downloads\\"+ kunde_id +""+monat+""+jahr+"rechnung"+ "("+  fileNo +")"+ ".pdf" );
				
				}

		    }else if (!file_name.exists()) {
		    	file_name.createNewFile();
		    }
			
			//Create Document object
			Document document = new Document();
			//Get pdfwriter instance
			PdfWriter.getInstance(document, new FileOutputStream(file_name));
			
			//Open the document
			document.open();
			
			
			
			//Add image in pdf -> ANMERKUNG: Pfad anpassen
			document.add(Image.getInstance("C:\\Users\\David\\Desktop\\hiertesteichpdf\\Braingain_Bild.PNG"));
			
			// Anschrift
						document.add(new Paragraph(" "));
						List anschrift = db.getAnschriftByKunde_ID(kunde_id);
						for (int i = 0;i<anschrift.size();i++){
							Paragraph zeile = new Paragraph();
							zeile.add(new Paragraph(anschrift.get(i).toString())); //Methode einf�gen getKundeByID
							document.add(zeile);
						}
	
			// Add a content
			document.add(new Paragraph(" "));
			Paragraph headline = new Paragraph();
	        headline.add(new Paragraph("Rechnung", headline_font));
			document.add(headline);
			document.add(new Paragraph(" "));
			
			Paragraph rechnungnr = new Paragraph();
			rechnungnr.add(new Paragraph("Rechnung Nr: " + rechnungsnr, smallBold));
			document.add(rechnungnr);
			
			Paragraph info = new Paragraph();
			info.add(new Paragraph("Bitte bei Zahlungen den Schriftverkehr angeben", supersmallBold));
			document.add(info);
			
			document.add(new Paragraph(" "));
			//Hier kommen Empf�nger, Tutor usw. hin
			
			Paragraph datum = new Paragraph();
			datum.add(new Paragraph("Datum: 01." + monatstring + "."+jahr)); //Methode einf�gen getKundeByID
			document.add(datum);
			//Update 02.06.2021: Name des Kunden in Rechnung
			Paragraph kunde = new Paragraph();
			kunde.add(new Paragraph("Empf�nger: " + Database_Helper.getKundeNameByID(kunde_id) +"(ID: "+kunde_id+")")); //Methode einf�gen getKundeByID
			document.add(kunde);
			
			//Update 02.06.2021: Tutor in die Rechnung einf�gen
			Paragraph tutor = new Paragraph();
			String tutorAusgabe = "Tutor: ";
			List alleTutoren = Database_Helper.getTutorForInvoice(kunde_id, monat, jahr);
			for (int i = 0;i<alleTutoren.size();i++){
				tutorAusgabe += " " + alleTutoren.get(i).toString();
			}
			
			tutor.add(new Paragraph(tutorAusgabe)); //Methode einf�gen getKundeByID
			document.add(tutor);
			
			
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			
		
			//Add Pdf table objects
			//private static void createTable(Section subCatPart)
			        //throws BadElementException {
				
			    PdfPTable table = new PdfPTable(7); //zwischen den Klammern: Anzahl von Spalten
			    table.setWidthPercentage(100);
			    
			
			//Create Cell object
			PdfPCell c1 = new PdfPCell(new Phrase("Pos", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Fach", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1).setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Niveau", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Datum", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Einzelpreis", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Anzahl Stunde", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Gesamtpreis", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			table.setHeaderRows(1);
			
			
			//Einf�gen aus DB
			
			Database_Helper db = new Database_Helper();
			
			List test = db.returnInvoiceData(kunde_id, monat, jahr);
			
			for (int i = 0;i<test.size();i++){
			    table.addCell(new Phrase(test.get(i).toString(),FontFactory.getFont(FontFactory.HELVETICA, 9) ));
			}

			document.add(table);
			
			//subCatPart.add(table);
			//}
		
			
			//Gesamtbetrag
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			Paragraph gesamtpreis = new Paragraph();
			gesamtpreis.add(new Paragraph("Gesamtbetrag:  "+db.returnTotalPrice(kunde_id, monat, jahr) +" EUR"  , smallBold));
		
			gesamtpreis.setAlignment(Element.ALIGN_LEFT);
			
			document.add(gesamtpreis);
			
			//Update 07.06.2021: Kontaktdaten und Bankdaten auf der Rechnung hinzuf�gen
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			
			
			
			 document.add(Chunk.NEWLINE);
			 LineSeparator ls = new LineSeparator();
			 document.add(new Chunk(ls));
			 //Create the table which will be 2 Columns wide and make it 100% of the page
			 PdfPTable myTable = new PdfPTable(2);
			 myTable.setWidthPercentage(100.0f);
			 
			//create a 2 cells and add them to the table
			 PdfPCell cellOne = new PdfPCell(new Paragraph("Institut f�r Lernf�rderung\nKatharina Becker\nHillerbacherweg 16\n57319 Bad Berleburg\n0178 1403710\nkatharina.becker.94@outlook.de", supersmallBold));
			 PdfPCell cellTwo = new PdfPCell(new Paragraph("Volksbank S�dwestfallen eG\nBIC: GENODEM1NRD\nIBAN: DE28447615345703564200\n Steuernummer: 342/50162169\nFinanzamt Siegen", supersmallBold));
			 
			 cellOne.setBorder(Rectangle.NO_BORDER);
			 cellOne.setHorizontalAlignment(Element.ALIGN_LEFT);
			 cellTwo.setBorder(Rectangle.LEFT);
			 cellTwo.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 
			 //Add the 2 cells to the table
			 myTable.addCell(cellOne);
			 myTable.addCell(cellTwo);
			 
			 document.add(myTable);
			 document.bottom();
			 
			 
			//Close the document
			document.close();
			
			
			System.out.println("finished");
			
		    }  catch (Exception e) {
		    	System.err.println(e);
			
		    }
		}
	
	public void createPDFforStaatlich(String adressat, 
			String strasseundhausnr, String plzundstadt, 
			String freitext,int kunde_id, int monat, int jahr) {
		
		String monatstring="";	//wenn Monat kleiner als 10 ist, dann soll der Monat mit einer 0 davor ausgegeben werden. Also anstatt "9" dann "09"
		if (monat < 10) {
			 monatstring = "0"+monat;
		} 
		
		try {
			
			
			String rechnungsnr = kunde_id + "-" + jahr +"-" + monatstring;
			
			Font headline_font = new Font(Font.FontFamily.TIMES_ROMAN, 18,
		            Font.BOLD);
			Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
		            Font.BOLD);
			Font supersmallBold = new Font(Font.FontFamily.TIMES_ROMAN, 8,
		            Font.BOLDITALIC);
		
			
			//Create File Name -> ANMERKUNG: Pfad anpassen
			//Update damit man nicht den Pfad immer anpassen muss
			String homeDir = System.getProperty("user.home");
			System.out.println("homeDir                = " + homeDir);
			File file_name = new File( homeDir+"\\Downloads\\"+ kunde_id +""+monat+""+jahr+"rechnung.pdf");
		   //�berpr�fung, ob der Dateiname schon existiert, wenn ja wird eine Zahl hinter dran geh�ngt
			int fileNo=0;
		    if (file_name.exists() && !file_name.isDirectory()) {
			while(file_name.exists()){
				fileNo++;
				file_name = new File(homeDir+"\\Downloads\\"+ kunde_id +""+monat+""+jahr+"rechnung"+ "("+  fileNo +")"+ ".pdf" );
				
				}

		    }else if (!file_name.exists()) {
		    	file_name.createNewFile();
		    }
			
			//Create Document object
			Document document = new Document();
			//Get pdfwriter instance
			PdfWriter.getInstance(document, new FileOutputStream(file_name));
			
			//Open the document
			document.open();
			
			//08.06.21: Neue Seite eingef�gt
			//Add image in pdf -> ANMERKUNG: Pfad anpassen
			document.add(Image.getInstance("C:\\Users\\David\\Desktop\\hiertesteichpdf\\Braingain_Bild.PNG"));
			
			// Anschrift vom Amt
			document.add(new Paragraph(" "));
			List anschriftAmt = new ArrayList<String>();
			anschriftAmt.add(adressat);
			anschriftAmt.add(strasseundhausnr);
			anschriftAmt.add(plzundstadt);
			for (int i = 0;i<anschriftAmt.size();i++){
				Paragraph zeile = new Paragraph();
				zeile.add(new Paragraph(anschriftAmt.get(i).toString())); //Methode einf�gen getKundeByID
				document.add(zeile);
			}
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			// Add a content
						document.add(new Paragraph(" "));
						Paragraph headlineSeite1 = new Paragraph();
				        headlineSeite1.add(new Paragraph("Rechnung f�r den Nachhilfeunterricht eines Sch�lers", smallBold));
						document.add(headlineSeite1);
						document.add(new Paragraph(" "));
						
						Paragraph rechnungnrSeite1 = new Paragraph();
						rechnungnrSeite1.add(new Paragraph("Rechnung Nr: " + rechnungsnr, smallBold));
						document.add(rechnungnrSeite1);
						
						Paragraph infoSeite1 = new Paragraph();
						infoSeite1.add(new Paragraph("Bitte bei Zahlungen den Schriftverkehr angeben", supersmallBold));
						document.add(infoSeite1);
					
						document.add(new Paragraph(" "));
						//Informationen wie Datum usw.
						Paragraph datumSeite1 = new Paragraph();
						datumSeite1.add(new Paragraph("Datum: 01." + monatstring + "."+jahr)); //Methode einf�gen getKundeByID
						document.add(datumSeite1);
						//Update 02.06.2021: Name des Kunden in Rechnung
						Paragraph kundeSeite1 = new Paragraph();
						kundeSeite1.add(new Paragraph("Sch�ler: " + Database_Helper.getKundeNameByID(kunde_id) +"(ID: "+kunde_id+")")); //Methode einf�gen getKundeByID
						document.add(kundeSeite1);
						
						document.add(new Paragraph(" "));
						document.add(new Paragraph(" "));
						document.add(new Paragraph(" "));
						document.add(new Paragraph(" "));
						Paragraph freitextSeite1 = new Paragraph();
						freitextSeite1.add(new Paragraph(freitext));
						document.add(freitextSeite1);
			
						//Update 07.06.2021: Kontaktdaten und Bankdaten auf der Rechnung hinzuf�gen
						document.add(new Paragraph(" "));
						document.add(new Paragraph(" "));
						document.add(new Paragraph(" "));
						
						
						
						 document.add(Chunk.NEWLINE);
						 LineSeparator lsSeite1 = new LineSeparator();
						 document.add(new Chunk(lsSeite1));
						 //Create the table which will be 2 Columns wide and make it 100% of the page
						 PdfPTable myTableSeite1 = new PdfPTable(2);
						 myTableSeite1.setWidthPercentage(100.0f);
						 
						//create a 2 cells and add them to the table
						 PdfPCell cellOneSeite1 = new PdfPCell(new Paragraph("Institut f�r Lernf�rderung\nKatharina Becker\nHillerbacherweg 16\n57319 Bad Berleburg\n0178 1403710\nkatharina.becker.94@outlook.de", supersmallBold));
						 PdfPCell cellTwoSeite1 = new PdfPCell(new Paragraph("Volksbank S�dwestfallen eG\nBIC: GENODEM1NRD\nIBAN: DE28447615345703564200\n Steuernummer: 342/50162169\nFinanzamt Siegen", supersmallBold));
						 
						 cellOneSeite1.setBorder(Rectangle.NO_BORDER);
						 cellOneSeite1.setHorizontalAlignment(Element.ALIGN_LEFT);
						 cellTwoSeite1.setBorder(Rectangle.LEFT);
						 cellTwoSeite1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						 
						 //Add the 2 cells to the table
						 myTableSeite1.addCell(cellOneSeite1);
						 myTableSeite1.addCell(cellTwoSeite1);
						 
						 document.add(myTableSeite1);
						 document.bottom();
			
			
			// *********************08.06.21: neue Seite vorbei:***************************
			document.newPage();
			
			//Add image in pdf -> ANMERKUNG: Pfad anpassen
			document.add(Image.getInstance("C:\\Users\\David\\Desktop\\hiertesteichpdf\\Braingain_Bild.PNG"));
			
			// Anschrift
						document.add(new Paragraph(" "));
						List anschrift = db.getAnschriftByKunde_ID(kunde_id);
						for (int i = 0;i<anschrift.size();i++){
							Paragraph zeile = new Paragraph();
							zeile.add(new Paragraph(anschrift.get(i).toString())); //Methode einf�gen getKundeByID
							document.add(zeile);
						}
	
			// Add a content
			document.add(new Paragraph(" "));
			Paragraph headline = new Paragraph();
	        headline.add(new Paragraph("Rechnung", headline_font));
			document.add(headline);
			document.add(new Paragraph(" "));
			
			Paragraph rechnungnr = new Paragraph();
			rechnungnr.add(new Paragraph("Rechnung Nr: " + rechnungsnr, smallBold));
			document.add(rechnungnr);
			
			Paragraph info = new Paragraph();
			info.add(new Paragraph("Bitte bei Zahlungen den Schriftverkehr angeben", supersmallBold));
			document.add(info);
			
			document.add(new Paragraph(" "));
			//Hier kommen Empf�nger, Tutor usw. hin
			
			Paragraph datum = new Paragraph();
			datum.add(new Paragraph("Datum: 01." + monatstring + "."+jahr)); //Methode einf�gen getKundeByID
			document.add(datum);
			//Update 02.06.2021: Name des Kunden in Rechnung
			Paragraph kunde = new Paragraph();
			kunde.add(new Paragraph("Empf�nger: " + Database_Helper.getKundeNameByID(kunde_id) +"(ID: "+kunde_id+")")); //Methode einf�gen getKundeByID
			document.add(kunde);
			
			//Update 02.06.2021: Tutor in die Rechnung einf�gen
			Paragraph tutor = new Paragraph();
			String tutorAusgabe = "Tutor: ";
			List alleTutoren = Database_Helper.getTutorForInvoice(kunde_id, monat, jahr);
			for (int i = 0;i<alleTutoren.size();i++){
				tutorAusgabe += " " + alleTutoren.get(i).toString();
			}
			
			tutor.add(new Paragraph(tutorAusgabe)); //Methode einf�gen getKundeByID
			document.add(tutor);
			
			
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			
		
			//Add Pdf table objects
			//private static void createTable(Section subCatPart)
			        //throws BadElementException {
				
			    PdfPTable table = new PdfPTable(7); //zwischen den Klammern: Anzahl von Spalten
			    table.setWidthPercentage(100);
			    
			
			//Create Cell object
			PdfPCell c1 = new PdfPCell(new Phrase("Pos", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Fach", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1).setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Niveau", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Datum", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Einzelpreis", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Anzahl Stunde", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			
			c1 = new PdfPCell(new Phrase("Gesamtpreis", FontFactory.getFont(FontFactory.HELVETICA, 9)));
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1)
			.setBackgroundColor(BaseColor.GREEN);;
			table.setHeaderRows(1);
			
			
			//Einf�gen aus DB
			
			Database_Helper db = new Database_Helper();
			
			List test = db.returnInvoiceData(kunde_id, monat, jahr);
			
			for (int i = 0;i<test.size();i++){
			    table.addCell(new Phrase(test.get(i).toString(),FontFactory.getFont(FontFactory.HELVETICA, 9) ));
			}

			document.add(table);
			
			//subCatPart.add(table);
			//}
		
			
			//Gesamtbetrag
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			Paragraph gesamtpreis = new Paragraph();
			gesamtpreis.add(new Paragraph("Gesamtbetrag:  "+db.returnTotalPrice(kunde_id, monat, jahr) +" EUR"  , smallBold));
		
			gesamtpreis.setAlignment(Element.ALIGN_LEFT);
			
			document.add(gesamtpreis);
			
			//Update 07.06.2021: Kontaktdaten und Bankdaten auf der Rechnung hinzuf�gen
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			document.add(new Paragraph(" "));
			
			
			
			
			 document.add(Chunk.NEWLINE);
			 LineSeparator ls = new LineSeparator();
			 document.add(new Chunk(ls));
			 //Create the table which will be 2 Columns wide and make it 100% of the page
			 PdfPTable myTable = new PdfPTable(2);
			 myTable.setWidthPercentage(100.0f);
			 
			//create a 2 cells and add them to the table
			 PdfPCell cellOne = new PdfPCell(new Paragraph("Institut f�r Lernf�rderung\nKatharina Becker\nHillerbacherweg 16\n57319 Bad Berleburg\n0178 1403710\nkatharina.becker.94@outlook.de", supersmallBold));
			 PdfPCell cellTwo = new PdfPCell(new Paragraph("Volksbank S�dwestfallen eG\nBIC: GENODEM1NRD\nIBAN: DE28447615345703564200\n Steuernummer: 342/50162169\nFinanzamt Siegen", supersmallBold));
			 
			 cellOne.setBorder(Rectangle.NO_BORDER);
			 cellOne.setHorizontalAlignment(Element.ALIGN_LEFT);
			 cellTwo.setBorder(Rectangle.LEFT);
			 cellTwo.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 
			 //Add the 2 cells to the table
			 myTable.addCell(cellOne);
			 myTable.addCell(cellTwo);
			 
			 document.add(myTable);
			 document.bottom();
			 
			 
			//Close the document
			document.close();
			
			
			System.out.println("finished");
			
		    }  catch (Exception e) {
		    	System.err.println(e);
			
		    }
		}
		
		
		
	
	


	public static void main(String[] args) {
		Invoice invoice = new Invoice();
		invoice.createPDF(1579, 5, 2021);
		
	}



	
	

}
