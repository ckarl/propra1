package Models;

public class Institute extends Address{
	
	private String institute_Name;
	private String recipient;
	
	
	public Institute(String street, String houseNr, int zip, String city, String institute_Name, String recipient) {
		
		super(street, houseNr, zip, city);
		this.institute_Name = institute_Name;
		this.recipient = recipient;
		
	}
	
	public String getInstitute_Name() {
		return institute_Name;
	}
	
	public void setInstitute_Name(String institute_Name) {
		this.institute_Name = institute_Name;
	}
	
	
	@Override
	public String toString() {
		
		return super.toString() + " " + institute_Name + " " + recipient;
	}

	

}
