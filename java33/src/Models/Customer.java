package Models;

public class Customer extends Person
{
	
	private Tutor tutor;
	
	public Customer(int id, String lastname, String firstname, int phoneNumber, Address address, Tutor tutor) {
		super(id, lastname, firstname, phoneNumber, address);
		this.tutor = tutor;
		}
	
	public Tutor getTutor() {
		return tutor;
	}
	
	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}
	
	
	@Override
	public String toString() {
		
		return super.toString() + " " + tutor;
	}

}
