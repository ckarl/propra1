package Models;

public class StateCustomer extends Customer {
	
	private String approvedSubject;
	private double maxHour;
	private double currentHour;
	
	
	public StateCustomer(int id, String lastname, String firstname, int phoneNumber, Address address, Tutor tutor, String approvedSubject, double maxHour, double currentHour) {
		
		super(id, lastname, firstname, phoneNumber, address, tutor);
		this.approvedSubject = approvedSubject;
		this.maxHour = maxHour;
		this.currentHour = currentHour;
	}
	
	
	public String getApprovedSubject() {
		return approvedSubject;
	}
	
	public void setApprovedSubject(String approvedSubject) {
		this.approvedSubject = approvedSubject;
	}
	
	public double getMaxHour() {
		return maxHour;
	}
	
	public void setMaxHour(double maxHour) {
		this.maxHour = maxHour;
	}
	
	public double getCurrentHour() {
		return currentHour;
	}
	
	public void setCurrentHour(double currentHour) {
		this.currentHour = currentHour;
	}

	
	@Override
	public String toString() {
		
		return super.toString() + " " + approvedSubject + " " + maxHour + " " + currentHour;
	}

}
