package Models;

public abstract class Person {
	
	private static int id;
	private String lastname;
	private String firstname;
	private int phoneNumber;
	private Address address;
	
	
	
	public Person(int id, String lastname, String firstname, int phoneNumber, Address address) {
		
		Person.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}
	
	public static int getID() {
		return id;
	}
	
	public static void setID(int newId) {
		Person.id = newId;
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getFirstname() {
		return firstname;
	}
	
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public int getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setTelefonnummer(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	public String toString() {
		return " " + id + " " + lastname + " " + firstname + " " + phoneNumber + " " + address;
	}
	
	
	
	
	
	

	/*public static void main(String[] args) {
		// TODO Auto-generated method stub

	}*/

}
