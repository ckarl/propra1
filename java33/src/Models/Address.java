package Models;

public class Address {
	
	private String street;
	private String houseNr;
	private int zip;
	private String city;
	
	
	public Address(String street, String houseNr, int zip, String city) {
		
		this.street = street;
		this.houseNr = houseNr;
		this.zip = zip;
		this.city = city;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getHouseNr() {
		return houseNr;
	}
	
	public void setHouseNr(String houseNr) {
		this.houseNr = houseNr;
	}
	
	public int getZip() {
		return zip;
	}
	
	public void setZip(int zip) {
		this.zip = zip;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
	
	public String toString() {
		return " " + street + " " + houseNr + " " + zip + " " + city;
		}

}
