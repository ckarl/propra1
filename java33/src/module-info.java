module java33
{
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;
	requires itextpdf;

    opens sample;
}