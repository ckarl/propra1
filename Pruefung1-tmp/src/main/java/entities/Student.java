package main.java.entities;

public class Student {

	private String firstName;
	private String lastName;
	private String numberString;
	private String birthDate;
	
	public Student(String firstName2, String lastName2, String number, String birthDate2) {
		this.firstName = firstName2;
		this.lastName = lastName2;
		this.numberString = number;
		this.birthDate = birthDate2;	
	}
	
	public Student() {
		
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNumber(String numberString) {
		this.numberString = numberString;
	}
	
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}
	
	public String getNumber() {
		return this.numberString;
	}
	
	public String getBirthDate() {
		return this.birthDate;
	}
	
	public String printStudent() {
		return this.firstName + ", " + this.lastName + ", " + this.numberString + ", " + this.birthDate;
	}

}

