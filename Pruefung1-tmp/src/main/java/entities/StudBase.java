package main.java.entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StudBase {

	private int size;
	private List<Student> students;

	public StudBase(int size) {
		this.size = size;
		this.students = new ArrayList<Student>(size);
	}

	public void printStudent() {
		for(Student student : students) {
			System.out.println(student.printStudent());
		}
	}

	public boolean addStudent(Student student) {
		int actualSize = students.size();
		if(actualSize < this.size) {
			return this.students.add(student);			
		} else {
			System.err.println("Cannot add student. Maximum size reached");
			return false;
		}
	}

	public boolean deleteStudent(String number) {
		boolean isDeleted = false;
		Student deleteStudent = null;
		for(Student student : students) {
			if (student.getNumber().equals(number)) {
				deleteStudent = student;
				break;
			}
		}
		if(deleteStudent != null) {
			isDeleted = students.remove(deleteStudent);
		}
		return isDeleted;
	}

	public int amountOfStudents() {
		return students.size();
	}

	public void printAverageAge() {
		int amountStudents = this.students.size();
		for(Student student : students) {
			String birthDate = student.getBirthDate();
			String split[] = birthDate.split("-");
			Integer year = Integer.getInteger(split[0]);
			Integer month = Integer.getInteger(split[1]);
			Integer day = Integer.getInteger(split[2]);
		}
		System.out.print("failure");
	}

	public void search(String string) {
		List<Student> found = new ArrayList<Student>();
		for(Student student : students) {
			if(student.getBirthDate().contains(string)||
			student.getFirstName().contains(string)||
			student.getLastName().contains(string)||
			student.getNumber().contains(string)) {
				found.add(student);
			}
		}
		for(Student student: found) {
			student.printStudent();
		}
	}

}
