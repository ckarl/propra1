package main.java.main;

import java.util.ArrayList;
import java.util.List;

import main.java.entities.StudBase;
import main.java.entities.Student;

public class Main {
	
	public static void main(String[] args) {
		Main.executeTask1();
		Main.executeTask2();
		Main.executeTask3();
		Main.executeTask4();
		Main.executeTask5();
		Main.executeTask6();
		Main.executeTask7();
		Main.executeTask8();
		Main.executeTask9();
	}

	private static void executeTask9() {
		StudBase studBase = new StudBase(2);
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		Student student = new Student(firstName, lastName, number, "2001-05-03");
		studBase.search("Koch");
		
		
	}

	private static void executeTask8() {
		StudBase studBase = new StudBase(3);
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		Student student = new Student(firstName, lastName, number, "2001-05-03");
		studBase.addStudent(student);
		student = new Student(firstName, lastName, number, "2011-05-03");
		studBase.addStudent(student);
		studBase.printAverageAge();
	}

	private static void executeTask7() {
		StudBase studBase = new StudBase(5);
		System.out.println(studBase.amountOfStudents());
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		String birthDate = "1983-05-23";
		Student student = new Student(firstName, lastName, number, birthDate);
		studBase.addStudent(student);
		System.out.println(studBase.amountOfStudents());
	}

	private static void executeTask6() {
		int size = 1;
		StudBase studBase = new StudBase(size);
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		String birthDate = "1983-05-23";
		Student student = new Student(firstName, lastName, number, birthDate);
		studBase.addStudent(student);
		boolean isDeleted = studBase.deleteStudent("23");
		if(isDeleted) {
			System.out.println("Deletion successful. All good.");
		} else {
			System.err.println("Deletion failed. All bad.");
		}
		studBase.printStudent();
	}

	private static void executeTask5() {
		int size = 1;
		StudBase studBase = new StudBase(size);
		studBase.printStudent();
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		String birthDate = "1983-05-23";
		Student student = new Student(firstName, lastName, number, birthDate);
		boolean isAdded = studBase.addStudent(student);
		if (!isAdded) {
			System.err.println("Student was not added!");
		} else {
			System.out.println("All good.");
		}
		studBase.printStudent();
		isAdded = studBase.addStudent(student);
		if(isAdded) {
			System.err.println("Maximum sized should have been reached.");
		} else {
			System.out.println("Previous error expected. All good.");
		}		
	}

	private static void executeTask4() {
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		String birthDate = "1983-05-23";
		List<Student> students = new ArrayList<Student>();
		for (int i = 1; i < 101; i++) {
			number = i + "";
			Student student = new Student(firstName, lastName, number, birthDate);
			students.add(student);
		}
		for (Student student : students) {
			System.out.println(student.printStudent());
		}
	}

	private static void executeTask3() {
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		String birthDate = "1983-05-23";
		Student student = new Student(firstName, lastName, number, birthDate);
		String printed = student.printStudent();
		System.out.println(printed);
		String expected = "Karl, Koch, 23, 1983-05-23";
		if (printed.equals(expected)) {
			System.out.println("Success.");
		} else {
			System.err.println("Failure.");
		}
	}

	private static void executeTask2() {
		String firstName = "Karl";
		String lastName = "Koch";
		String number = "23";
		String birthDate = "1983-05-23";
		Student student = new Student(firstName, lastName, number, birthDate);

		System.out.println("Vorname: " + student.getFirstName());
		System.out.println("Nachname: " + student.getLastName());
		System.out.println("Matrikelnummer: " + student.getNumber());
		System.out.println("Geburtstagsdatum: " + student.getBirthDate());
	}

	private static void executeTask1() {
		Student student = new Student();
		student.setFirstName("Karl");
		student.setLastName("Koch");
		student.setNumber("23");
		student.setBirthDate("1983-05-23");
		
		System.out.println("Vorname: " + student.getFirstName());
		System.out.println("Nachname: " + student.getLastName());
		System.out.println("Matrikelnummer: " + student.getNumber());
		System.out.println("Geburtstagsdatum: " + student.getBirthDate());
	}

}
